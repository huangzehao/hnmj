#include "StdAfx.h"
#include "TableFrameSink.h"
#include "FvMask.h"

bool CTableFrameSink::hasRule(BYTE cbRule)
{
    return FvMask::HasAny(m_dwGameRuleIdex, _MASK_(cbRule));
}
bool CTableFrameSink::isHZGuiZe()
{
    return hasRule(GAME_TYPE_ZZ_HONGZHONG_GZ);
}
//DWORD CTableFrameSink::AnalyseChiHuCardZZ(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, BYTE cbCurrentCard, CChiHuRight &ChiHuRight, bool bSelfSendCard)
//{
//    //变量定义
//    DWORD cbChiHuKind = WIK_NULL;
//    CAnalyseItemArray AnalyseItemArray;
//
//    //抢杠胡
//    bool isQiangGangHu = false;
//    if (m_wCurrentUser == INVALID_CHAIR && m_bGangStatus)
//    {
//        if (hasRule(GAME_TYPE_ZZ_QIANGGANGHU) && !hasRule(GAME_TYPE_ZZ_ZIMOHU)) //自摸胡不可抢杠
//        {
//            ChiHuRight |= CHR_QIANG_GANG_HU;
//            isQiangGangHu = true;
//        }
//        else
//        {
//            ChiHuRight.SetEmpty();
//        }
//
//    }
//    if (hasRule(GAME_TYPE_ZZ_HONGZHONG) && isHZGuiZe())
//    {
//        // 起手4个红中直接胡
//        if (m_GameLogic.has4MagicCard(cbCardIndex) && m_cbSendCardCount <= 0)
//        {
//            cbChiHuKind = WIK_CHI_HU;
//            ChiHuRight.SetEmpty();
//            ChiHuRight |= CHR_TIAN_HU;
//            return cbChiHuKind;
//        }
//    }
//
//    if (hasRule(GAME_TYPE_ZZ_ZIMOHU)
//        && !bSelfSendCard
//        && !isQiangGangHu
//        )
//    {
//        return WIK_NULL;
//    }
//
//    if (isHZGuiZe()
//        && !bSelfSendCard
//        && !isQiangGangHu
//        )
//    {
//        return WIK_NULL;
//    }
//
//
//    if (cbCurrentCard == 0)
//    {
//        return WIK_NULL;
//    }
//    //设置变量
//    AnalyseItemArray.RemoveAll();
//    ChiHuRight.SetEmpty();
//
//    if (bSelfSendCard)
//    {
//        ChiHuRight |= CHR_ZI_MO;
//    }
//
//    //构造扑克
//    BYTE cbCardIndexTemp[MAX_INDEX];
//    CopyMemory(cbCardIndexTemp, cbCardIndex, sizeof(cbCardIndexTemp));
//
//    if (hasRule(GAME_TYPE_ZZ_QIDUI))
//    {
//        int iTemp = 0;
//        if (m_GameLogic.IsQiXiaoDui(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard, iTemp))
//        {
//            cbChiHuKind = WIK_CHI_HU;
//            ChiHuRight |= CHR_QI_XIAO_DUI;
//            return cbChiHuKind;
//        }
//    }
//
//    //插入扑克
//    if (cbCurrentCard != 0)
//        cbCardIndexTemp[m_GameLogic.SwitchToCardIndex(cbCurrentCard)]++;
//
//    if (hasRule(GAME_TYPE_ZZ_HONGZHONG))
//    {
//        if (m_GameLogic.has4MagicCard(cbCardIndexTemp) && m_cbSendCardCount <= 0)
//        {
//            cbChiHuKind = WIK_CHI_HU;
//            return cbChiHuKind;
//        }
//        if (m_GameLogic.getMagicCardCount(cbCardIndexTemp) == 0 && isHZGuiZe())
//        {
//            ChiHuRight |= CHR_HONGZHONG_WU;
//        }
//    }
//
//    //分析扑克
//    bool bValue = m_GameLogic.AnalyseCard(cbCardIndexTemp, WeaveItem, cbWeaveCount, AnalyseItemArray);
//    if (!bValue)
//    {
//        ChiHuRight.SetEmpty();
//        return WIK_NULL;
//    }
//
//    //258做将
//    if (hasRule(GAME_TYPE_ZZ_258))
//    {
//        bool b258Vaild = false;
//        for (INT_PTR i = 0; i < AnalyseItemArray.GetCount(); i++)
//        {
//            //变量定义
//            tagAnalyseItem * pAnalyseItem = &AnalyseItemArray[i];
//            BYTE cbCardValue = pAnalyseItem->cbCardEye&MASK_VALUE;
//            if (cbCardValue != 2 && cbCardValue != 5 && cbCardValue != 8)
//            {
//            }
//            else
//            {
//                b258Vaild = true;
//                break;
//            }
//        }
//        if (!b258Vaild)
//        {
//            ChiHuRight.SetEmpty();
//            cbChiHuKind = WIK_NULL;
//            return cbChiHuKind;
//        }
//    }
//    cbChiHuKind = WIK_CHI_HU;
//
//    ChiHuRight |= CHR_SHU_FAN;
//
//    return cbChiHuKind;
//}

//DWORD CTableFrameSink::AnalyseChiHuCardCS(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, BYTE cbCurrentCard, CChiHuRight &ChiHuRight, bool bSelfSendCard)
//{
//    //变量定义
//    DWORD cbChiHuKind = WIK_NULL;
//    CAnalyseItemArray AnalyseItemArray;
//
//    //设置变量
//    AnalyseItemArray.RemoveAll();
//    //	ChiHuRight.SetEmpty();
//
//        //构造扑克
//    BYTE cbCardIndexTemp[MAX_INDEX];
//    CopyMemory(cbCardIndexTemp, cbCardIndex, sizeof(cbCardIndexTemp));
//
//    //cbCurrentCard一定不为0			!!!!!!!!!
//    ASSERT(cbCurrentCard != 0);
//    if (cbCurrentCard == 0) return WIK_NULL;
//
//    /*
//    //	特殊番型
//    */
//    //七小对牌
//    int nGenCount = 0;
//    bool isHu = false;
//    if (m_GameLogic.IsQiXiaoDui(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard, nGenCount))
//    {
//        if (nGenCount == 1)
//        {
//            ChiHuRight |= CHR_HAOHUA_QI_XIAO_DUI;
//        }
//        else if (nGenCount == 2)
//        {
//            ChiHuRight |= CHR_SHUANG_HAOHUA_QI_XIAO_DUI;
//        }
//        else if (nGenCount == 3)
//        {
//            ChiHuRight |= CHR_SAN_HAOHUA_QI_XIAO_DUI;
//        }
//        else
//        {
//            ChiHuRight |= CHR_QI_XIAO_DUI;
//        }
//        isHu = true;
//    }
//    if (m_GameLogic.IsJiangJiangHu(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard))
//    {
//        ChiHuRight |= CHR_JIANGJIANG_HU;
//
//        isHu = true;
//    }
//    if (cbWeaveCount == 4 && m_GameLogic.IsDanDiao(cbCardIndex, cbCurrentCard))
//    {
//        ChiHuRight |= CHR_QUAN_QIU_REN;
//        isHu = true;
//    }
//
//    if (!ChiHuRight.IsEmpty())
//        cbChiHuKind = WIK_CHI_HU;
//
//    //插入扑克
//    if (cbCurrentCard != 0)
//        cbCardIndexTemp[m_GameLogic.SwitchToCardIndex(cbCurrentCard)]++;
//
//    //分析扑克
//    bool bValue = m_GameLogic.AnalyseCard(cbCardIndexTemp, WeaveItem, cbWeaveCount, AnalyseItemArray);
//
//    //胡牌分析
//    if (!bValue && !isHu)
//    {
//        ChiHuRight.SetEmpty();
//        return WIK_NULL;
//    }
//
//    cbChiHuKind = WIK_CHI_HU;
//
//    //牌型分析
//    for (INT_PTR i = 0; i < AnalyseItemArray.GetCount(); i++)
//    {
//        //变量定义
//        tagAnalyseItem * pAnalyseItem = &AnalyseItemArray[i];
//
//        //碰碰和
//        if (m_GameLogic.IsPengPeng(pAnalyseItem))
//            ChiHuRight |= CHR_PENGPENG_HU;
//    }
//
//    //清一色牌
//    if (m_GameLogic.IsQingYiSe(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard))
//        ChiHuRight |= CHR_QING_YI_SE;
//
//    //素番
//    if (ChiHuRight.IsEmpty())
//        ChiHuRight |= CHR_SHU_FAN;
//
//    if (cbChiHuKind == WIK_CHI_HU && !(ChiHuRight&CHR_SHU_FAN).IsEmpty())
//    {
//        //长沙麻将小胡必须258做将
//        bool b258Vaild = false;
//        for (INT_PTR i = 0; i < AnalyseItemArray.GetCount(); i++)
//        {
//            //变量定义
//            tagAnalyseItem * pAnalyseItem = &AnalyseItemArray[i];
//            BYTE cbCardValue = pAnalyseItem->cbCardEye&MASK_VALUE;
//            if (cbCardValue != 2 && cbCardValue != 5 && cbCardValue != 8)
//            {
//            }
//            else
//            {
//                b258Vaild = true;
//                break;
//            }
//        }
//        if (!b258Vaild)
//        {
//            ChiHuRight.SetEmpty();
//            cbChiHuKind = WIK_NULL;
//            return cbChiHuKind;
//        }
//    }
//    if (bSelfSendCard)
//    {
//        ChiHuRight |= CHR_ZI_MO;
//    }
//    return cbChiHuKind;
//}
DWORD CTableFrameSink::AnalyseChiHuRightCardHSHH(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, const tagWeaveItem PiziItem[], BYTE cbPiziCount, BYTE cbCurrentCard, CChiHuRight & ChiHuRight, bool bSelfSendCard)
{
    //变量定义
    DWORD cbChiHuKind = WIK_NULL;
    CAnalyseItemArray AnalyseItemArray;

    if (cbCurrentCard == 0)
    {
        return WIK_NULL;
    }

    if (m_wCurrentUser == INVALID_CHAIR && m_bGangStatus)
    {//明杠不可以抢杠胡
        return WIK_NULL;
    }

    bool bigHu = false;  //大胡
    bool suFanHu = true; //屁胡


    //构造扑克
    BYTE cbCardIndexTemp[MAX_INDEX];
    CopyMemory(cbCardIndexTemp, cbCardIndex, sizeof(cbCardIndexTemp));
    //插入扑克
    if (cbCurrentCard != 0)
    {
        cbCardIndexTemp[m_GameLogic.SwitchToCardIndex(cbCurrentCard)]++;
    }


    if (m_GameLogic.IsHavePiZiInHand(cbCardIndexTemp))
    {//手里还有红中、发财  就不可以胡牌 
        return WIK_NULL;
    }


    //分析扑克
    bool bValue = m_GameLogic.AnalyseCardHSHH(cbCardIndexTemp, WeaveItem, cbWeaveCount, AnalyseItemArray);
    if (bValue==false)
    {
        ChiHuRight.SetEmpty();
        return WIK_NULL;
    }

    if (bSelfSendCard)
    {//自摸
        ChiHuRight |= CHR_ZI_MO;
    }

    {//七小对
        int nGenCount = 0;
        if (m_GameLogic.IsQiXiaoDui(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard, nGenCount))
        {
            if (nGenCount == 1)
            {
                ChiHuRight |= CHR_HAOHUA_QI_XIAO_DUI;
            }
            else if (nGenCount == 2)
            {
                ChiHuRight |= CHR_SHUANG_HAOHUA_QI_XIAO_DUI;
            }
            else if (nGenCount == 3)
            {
                ChiHuRight |= CHR_SAN_HAOHUA_QI_XIAO_DUI;
            }
            else
            {
                ChiHuRight |= CHR_QI_XIAO_DUI;
            }
            bigHu = true;
            suFanHu = false;
        }
    }


    {//碰碰胡
        //牌型分析
        if (m_GameLogic.IsPengPengHSHH(cbCardIndexTemp, WeaveItem, cbWeaveCount))
        {
            ChiHuRight |= CHR_PENGPENG_HU;
            bigHu = true;
            suFanHu = false;
        }
    }

    {
        //清一色牌
        if (m_GameLogic.IsQingYiSeHSHH(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard))
        {
            ChiHuRight |= CHR_QING_YI_SE;
            bigHu = true;
            suFanHu = false;
        }
    }


    //将一色
    {
        if (m_GameLogic.IsJiangJiangHuHSHH(cbCardIndexTemp, WeaveItem, cbWeaveCount))
        {
            ChiHuRight |= CHR_JIANGJIANG_HU;
            bigHu = true;
            suFanHu = false;
        }
    }

    //无赖子胡牌CHR_CHIHU_NULL_MAGIC
    {
        if (m_GameLogic.IsYingHuHSHH(cbCardIndexTemp, WeaveItem, cbWeaveCount))
        {
            ChiHuRight |= CHR_CHIHU_NULL_MAGIC;
        }
    }


    {
        if (bSelfSendCard && bigHu == false)
        {//门清
            if (cbWeaveCount == 0)
            {
                ChiHuRight |= CHR_MEN_QING;
                suFanHu = false;
            }
            else
            {
                bool result = true;
                for (int i = 0; i < cbWeaveCount; i++)
                {
                    if (WeaveItem[i].cbWeaveKind == WIK_PIZI_ZFB_GANG ||
                        WeaveItem[i].cbPublicCard == FALSE)
                    {
                        continue;
                    }
                    result = false;
                    break;
                }
                if (result)
                {
                    suFanHu = false;
                    ChiHuRight |= CHR_MEN_QING;
                }
            }

        }
    }

    {
        bool isJianPaiHu = m_GameLogic.IsJianPaiHu(cbCardIndexTemp, WeaveItem, cbWeaveCount, cbCurrentCard);

        if (cbCardIndex[m_GameLogic.SwitchToCardIndex(m_GameLogic.MagicCardData())] >= 2 ||
            true == isJianPaiHu)
        {//2个癞子或者癞子做将，只可以自摸或者大胡放炮
            if (bSelfSendCard == false && bigHu == false)
            {
                ChiHuRight.SetEmpty();
                return WIK_NULL;
            }
        }
    }

    if (suFanHu)
    {
        ChiHuRight |= CHR_SHU_FAN;
    }
    cbChiHuKind = WIK_CHI_HU;

    return cbChiHuKind;
}
DWORD CTableFrameSink::AnalyseChiHuCardCS_XIAOHU(const BYTE cbCardIndex[MAX_INDEX], CChiHuRight &ChiHuRight)
{
    BYTE cbReplaceCount = 0;
    DWORD cbChiHuKind = WIK_NULL;

    //临时数据
    BYTE cbCardIndexTemp[MAX_INDEX];
    CopyMemory(cbCardIndexTemp, cbCardIndex, sizeof(cbCardIndexTemp));

    bool bDaSiXi = false;//大四喜
    bool bBanBanHu = true;//板板胡
    BYTE cbQueYiMenColor[3] = { 1,1,1 };//缺一色
    BYTE cbLiuLiuShun = 0;//六六顺

    //计算单牌
    for (BYTE i = 0; i < MAX_INDEX; i++)
    {
        BYTE cbCardCount = cbCardIndexTemp[i];

        if (cbCardCount == 0)
        {
            continue;
        }

        if (cbCardCount == 4)
        {
            bDaSiXi = true;
        }

        if (cbCardCount == 3)
        {
            cbLiuLiuShun++;
        }

        BYTE cbValue = m_GameLogic.SwitchToCardData(i) & MASK_VALUE;
        if (cbValue == 2 || cbValue == 5 || cbValue == 8)
        {
            bBanBanHu = false;
        }

        BYTE cbCardColor = m_GameLogic.SwitchToCardData(i)&MASK_COLOR;
        cbQueYiMenColor[cbCardColor >> 4] = 0;
    }
    if (bDaSiXi)
    {
        ChiHuRight |= CHR_XIAO_DA_SI_XI;
        cbChiHuKind = WIK_XIAO_HU;
    }
    if (bBanBanHu)
    {
        ChiHuRight |= CHR_XIAO_BAN_BAN_HU;
        cbChiHuKind = WIK_XIAO_HU;
    }
    if (cbQueYiMenColor[0] || cbQueYiMenColor[1] || cbQueYiMenColor[2])
    {
        ChiHuRight |= CHR_XIAO_QUE_YI_SE;
        cbChiHuKind = WIK_XIAO_HU;
    }
    if (cbLiuLiuShun >= 2)
    {
        ChiHuRight |= CHR_XIAO_LIU_LIU_SHUN;
        cbChiHuKind = WIK_XIAO_HU;
    }
    return cbChiHuKind;
}

DWORD CTableFrameSink::AnalyseChiHuCardHSHH(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, const tagWeaveItem PiziItem[], BYTE cbPiziCount, BYTE cbCurrentCard, CChiHuRight &ChiHuRight, WORD wChairId)
{
    DWORD chihuKind = this->AnalyseChiHuRightCardHSHH(cbCardIndex, WeaveItem, cbWeaveCount, PiziItem, cbPiziCount, cbCurrentCard, ChiHuRight, m_wProvideUser == m_wCurrentUser);
    if (chihuKind != WIK_NULL && this->IsFanShuCanChiHu(wChairId, ChiHuRight))
    {
        return chihuKind;
    }
    else
    {
        ChiHuRight.SetEmpty();
        return WIK_NULL;
    }
}

bool CTableFrameSink::IsFanShuCanChiHu(WORD wChairId, CChiHuRight &ChiHuRight)
{
    LONGLONG totalScore = this->CalculateChiHuGetScore(wChairId, ChiHuRight);
    int cellScore = hasRule(GAME_TYPE_HSHH_DIFEN16) ? 16 : 20;
    if (totalScore >= cellScore)
    {//判断是16分还是20分
        return true;
    }
#if _DEBUG
    return true;
#endif
    return false;
}

LONGLONG CTableFrameSink::CalculateChiHuGetScore(WORD wHuChairId, CChiHuRight & ChiHuRight)
{
    int fanshu = 0;
    int huFanShu = m_wFanShu[wHuChairId] + this->GetChiHuFanShu(ChiHuRight) + this->GetKaiKouFanShu(wHuChairId);
    LONGLONG cellScore = this->GetChiHuCellScore(wHuChairId, ChiHuRight);
    LONGLONG totalScore = 0;
    DWORD huType;
    ChiHuRight.GetRightData(&huType, MAX_RIGHT_COUNT);

    for (int i = 0; i < GAME_PLAYER; i++)
    {
        if (i == wHuChairId)
        {
            continue;
        }
        LONGLONG score = this->CalculatePlayerScore(i, ChiHuRight, huFanShu, cellScore);
        totalScore += score;
        m_lHuScore[i] = score * -1;
        //fanshu += m_wFanShu[i] + huFanShu + this->GetKaiKouFanShu(i);
    }
    m_lHuScore[wHuChairId] = totalScore;
    //if ((huType & CHR_ZI_MO) == 0)
    //{
    //    fanshu++;
    //}
    //LONGLONG totalScore = this->GetChiHuFanShuScore(this->GetChiHuCellScore(wHuChairId, ChiHuRight), fanshu);
    return totalScore;
}

LONGLONG CTableFrameSink::CalculatePlayerScore(WORD wChairId, CChiHuRight & ChiHuRight, WORD huFanShu, LONGLONG cellScore)
{
    //DWORD huType;
    //ChiHuRight.GetRightData(&huType, MAX_RIGHT_COUNT);
    ASSERT(m_wHuUser >= 0);
    ASSERT(wChairId != m_wHuUser);
    //int fanshu = m_wFanShu[wChairId]+ m_wFanShu[m_wHuUser] + this->GetChiHuFanShu(ChiHuRight) + this->GetKaiKouFanShu(wChairId) + this->GetKaiKouFanShu(m_wHuUser);
    int fanshu = m_wFanShu[wChairId] + this->GetKaiKouFanShu(wChairId) + huFanShu;
    DWORD huType;
    ChiHuRight.GetRightData(&huType, MAX_RIGHT_COUNT);
    if (((huType & CHR_ZI_MO) == 0) && (wChairId== m_wProvideUser))
    {
        fanshu++;
    }
    LONGLONG score = this->GetChiHuFanShuScore(cellScore, fanshu);
    return score;
}

LONGLONG CTableFrameSink::GetChiHuCellScore(WORD wChairId, CChiHuRight &ChiHuRight)
{// 判断牌型设置返回底分
    DWORD huType;
    ChiHuRight.GetRightData(&huType, MAX_RIGHT_COUNT);
    LONGLONG result = 0;

    if (huType & CHR_ZI_MO)
    {//0.0 最喜欢这种穷举不用动脑的
        if (huType & CHR_PENGPENG_HU)
        {
            if (huType & CHR_QING_YI_SE)
            {
                return 20;
            }
            if (huType & CHR_JIANGJIANG_HU)
            {
                return 20;
            }
            return 10;
        }
        if (huType & CHR_QING_YI_SE)
        {
            if (huType & CHR_QI_XIAO_DUI)
            {
                return 20;
            }
            if (huType & CHR_HAOHUA_QI_XIAO_DUI)
            {
                return 40;
            }
            if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
            {
                return 80;
            }
            if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
            {
                return 160;
            }
            return 10;
        }
        if (huType & CHR_JIANGJIANG_HU)
        {
            if (huType & CHR_QI_XIAO_DUI)
            {
                return 20;
            }
            if (huType & CHR_HAOHUA_QI_XIAO_DUI)
            {
                return 40;
            }
            if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
            {
                return 80;
            }
            if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
            {
                return 160;
            }
            return 10;
        }
        if (huType & CHR_MEN_QING)
        {
            return 5;
        }
        if (huType & CHR_QI_XIAO_DUI)
        {
            return 10;
        }
        if (huType & CHR_HAOHUA_QI_XIAO_DUI)
        {
            return 20;
        }
        if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
        {
            return 40;
        }
        if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
        {
            return 80;
        }
        if (huType & CHR_SHU_FAN)
        {
            return 3;
        }
    }
    if ((huType & CHR_ZI_MO)==0)
    {//0.0 最喜欢这种穷举不用动脑的
        if (huType & CHR_PENGPENG_HU)
        {
            if (huType & CHR_QING_YI_SE)
            {
                return 10;
            }
            if (huType & CHR_JIANGJIANG_HU)
            {
                return 10;
            }
            return 5;
        }
        if (huType & CHR_QING_YI_SE)
        {
            if (huType & CHR_QI_XIAO_DUI)
            {
                return 10;
            }
            if (huType & CHR_HAOHUA_QI_XIAO_DUI)
            {
                return 20;
            }
            if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
            {
                return 40;
            }
            if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
            {
                return 80;
            }
            return 5;
        }
        if (huType & CHR_JIANGJIANG_HU)
        {
            if (huType & CHR_QI_XIAO_DUI)
            {
                return 10;
            }
            if (huType & CHR_HAOHUA_QI_XIAO_DUI)
            {
                return 20;
            }
            if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
            {
                return 40;
            }
            if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
            {
                return 80;
            }
            return 5;
        }
        if (huType & CHR_MEN_QING)
        {
            ASSERT(false);
        }
        if (huType & CHR_QI_XIAO_DUI)
        {
            return 5;
        }
        if (huType & CHR_HAOHUA_QI_XIAO_DUI)
        {
            return 10;
        }
        if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
        {
            return 20;
        }
        if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
        {
            return 40;
        }
        if (huType & CHR_SHU_FAN)
        {
            return 1;
        }
    }
    ASSERT(false);
    return 1;
}

int CTableFrameSink::GetChiHuFanShu(CChiHuRight &ChiHuRight)
{// 判断牌型返回番数
    DWORD huType;
    ChiHuRight.GetRightData(&huType, MAX_RIGHT_COUNT);
    int result = 0;
    //if (huType & CHR_HAOHUA_QI_XIAO_DUI)
    //{
    //    result += 1;
    //}
    //if (huType & CHR_SHUANG_HAOHUA_QI_XIAO_DUI)
    //{
    //    result += 2;
    //}
    //if (huType & CHR_SAN_HAOHUA_QI_XIAO_DUI)
    //{
    //    result += 3;
    //}
    //if ((huType & CHR_PENGPENG_HU) || (huType & CHR_QING_YI_SE) || (huType & CHR_JIANGJIANG_HU))
    //{
    //    result += 1;
    //}
    if (huType & CHR_CHIHU_NULL_MAGIC)
    {
        result++;
    }
    return result;
}

int CTableFrameSink::GetKaiKouFanShu(WORD wChairId)
{
    int num = 0;
    for (int i = 0; i < m_cbWeaveItemCount[wChairId]; i++)
    {
        if ((m_WeaveItemArray[wChairId][i].cbWeaveKind == WIK_GANG &&
            m_WeaveItemArray[wChairId][i].cbPublicCard == TRUE) ||
            m_WeaveItemArray[wChairId][i].cbWeaveKind == WIK_PENG ||
            m_WeaveItemArray[wChairId][i].cbWeaveKind == WIK_LEFT ||
            m_WeaveItemArray[wChairId][i].cbWeaveKind == WIK_CENTER ||
            m_WeaveItemArray[wChairId][i].cbWeaveKind == WIK_RIGHT
            )
        {
            num++;
        }
    }
    if (num >= 3)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int CTableFrameSink::GetZFBGangFanShu(BYTE chairID)
{
    int num = 0;
    for (int i = 0; i < m_cbWeaveItemCount[chairID]; i++)
    {
        if (m_WeaveItemArray[chairID][i].cbWeaveKind == WIK_PIZI_ZFB_GANG)
        {
            num++;
        }
    }
    if (m_GameLogic.IsMagicCard(0x37))
    {
        return num * 5;
    }
    return num * 3;
}

LONGLONG CTableFrameSink::GetChiHuFanShuScore(LONGLONG cellScore, int fanshu)
{
    return cellScore* pow(2, fanshu);
}
//
//DWORD CTableFrameSink::AnalyseChiHuCard(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, BYTE cbCurrentCard, CChiHuRight &ChiHuRight)
//{
//    if (GAME_TYPE_ZZ == m_cbGameTypeIdex)
//    {
//        return AnalyseChiHuCardZZ(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard, ChiHuRight, m_wProvideUser == m_wCurrentUser);
//    }
//    if (GAME_TYPE_CS == m_cbGameTypeIdex)
//    {
//        return AnalyseChiHuCardCS(cbCardIndex, WeaveItem, cbWeaveCount, cbCurrentCard, ChiHuRight, m_wProvideUser == m_wCurrentUser);
//    }
//    ASSERT(false);
//    return 0;
//}
//
void CTableFrameSink::FiltrateRight(WORD wChairId, CChiHuRight &chr)
{
    //黄石晃晃不能有什么杠上开花之类的牌型杠上开花 就是自摸而已

    //权位增加
    //抢杠
    //if( m_wCurrentUser == INVALID_CHAIR && m_bGangStatus )
    //{
    //	chr |= CHR_QIANG_GANG_HU;
    //}
    //if (m_cbLeftCardCount==0)
    //{
    //	chr |= CHR_HAI_DI_LAO;
    //}
    ////附加权位
    ////杠上花
    //if( m_wCurrentUser==wChairId && m_bGangStatus )
    //{
    //	chr |= CHR_GANG_KAI;
    //	if (m_bCSGangStatusHuCout == 2)
    //	{
    //		chr |= CHR_GANG_SHUANG_KAI;
    //	}
    //}
    ////杠上炮
    //if( m_bGangOutStatus && !m_bGangStatus )
    //{
    //	chr |= CHR_GANG_SHANG_PAO;
    //	if (m_bCSGangStatusHuCout == 2)
    //	{
    //		chr |= CHR_GANG_SHUANG_KAI;
    //	}
    //}
}
