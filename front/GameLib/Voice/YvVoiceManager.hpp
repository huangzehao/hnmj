//
//  YvVoiceManager.hpp
//  GameBase
//
//  Created by zjm on 16/9/1.
//
//

#ifndef YvVoiceManager_hpp
#define YvVoiceManager_hpp

#include <stdio.h>
#include "cocos2d.h"

#include "GCloudVoice.h"
#include "DispatchMsgNode.hpp"

typedef void(*VoidFunc)();

class YvVoiceManager : public gcloud_voice::IGCloudVoiceNotify
{
public:
    static YvVoiceManager* GetInstance();
    void Init();
    void Cleanup();
    void setUploadSuccessCallBack(const std::function<void(const char *fileID)>& callback);
    void setPlayFinishCallBack(const std::function<void()>& callback);

    void OnUploadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) override;
    void OnDownloadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) override;
    void OnPlayRecordedFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath) override;
    void OnApplyMessageKey(gcloud_voice::GCloudVoiceCompleteCode code) override;

    
    void CpLogin(const std::string& nickname, const std::string& openid);
    void StartRecord();
    void StopRecord();
    void BtnRecord();
    void PlayRecord(std::string fileID);
    void UploadFile(); 
private:

    std::string _wpath;
    std::string _dpath;
    std::function<void(const char *fileID)> _uploadSuccessCallBack;
    std::function<void()> _playFinishCallBack;
    DispatchMsgNode * m_dispathMsgNode;
    bool _isPlayRecorded;
};

#endif /* YvVoiceManager_hpp */


