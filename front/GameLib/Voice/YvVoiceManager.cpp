//
//  YvVoiceManager.cpp
//  GameBase
//
//  Created by zjm on 16/9/1.
//
//
#include "cocos2d.h"
#include "GameLib/Game/Script/utility.h"


#include "YvVoiceManager.hpp"

YvVoiceManager* YvVoiceManager::GetInstance()
{
    static YvVoiceManager* pManager = NULL;
    if(pManager == NULL)
        pManager = new YvVoiceManager();
    return pManager;
}

void YvVoiceManager::Init(){

    _uploadSuccessCallBack = nullptr;
    _playFinishCallBack = nullptr;
    m_dispathMsgNode = nullptr;
    _isPlayRecorded = false;
    _dpath = _wpath = cocos2d::FileUtils::getInstance()->getWritablePath();
    if (_dpath.back() == '/')
    {
        _wpath += "audio.dat";
        _dpath += "downlad.dat";
    }
    else
    {
        _wpath += "/audio.dat";
        _dpath += "/downlad.dat";
    }
    CCLOG("wPath is %s", _wpath.c_str());

}


void YvVoiceManager::Cleanup(){
     cocos2d::log("YvVoiceManager::Cleanup");
}

void YvVoiceManager::CpLogin(const std::string& nickname, const std::string& openid){
    cocos2d::log("YvVoiceManager::CpLogin");
 
    gcloud_voice::GCloudVoiceErrno rst;

    std::stringstream openIDS;
    openIDS << nickname;
    openIDS << openid;
    rst = gcloud_voice::GetVoiceEngine()->SetAppInfo("1474387292", "987542871ff2d8bb46f6b41c15eb2397", openIDS.str().c_str());
    if (rst != gcloud_voice::GCLOUD_VOICE_SUCC)
    {
        CCLOG("SetAppInfo Error %d", rst);
    }

    rst = gcloud_voice::GetVoiceEngine()->Init();
    if (rst != gcloud_voice::GCLOUD_VOICE_SUCC)
    {
        CCLOG("Init Error %d", rst);
    }

    gcloud_voice::GetVoiceEngine()->SetMode(gcloud_voice::IGCloudVoiceEngine::Messages);

    gcloud_voice::GetVoiceEngine()->SetNotify(this);


    rst = gcloud_voice::GetVoiceEngine()->ApplyMessageKey();
    if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
        CCLOG("ApplyMessageKey Error %d", rst);
        return;
    }

    if (m_dispathMsgNode == NULL)
    {
        m_dispathMsgNode = DispatchMsgNode::create();
        m_dispathMsgNode->retain();
        m_dispathMsgNode->startDispatch();
    }
}

void YvVoiceManager::OnUploadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{
    cocos2d::log("YvVoiceManager::OnUploadFile");
    if (code == gcloud_voice::GV_ON_UPLOAD_RECORD_DONE) {
        if (this->_uploadSuccessCallBack != nullptr)
        {
            this->_uploadSuccessCallBack(fileID);
        }
    }
}


void YvVoiceManager::OnDownloadFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{
    cocos2d::log("YvVoiceManager::OnDownloadFile");
    if (code == gcloud_voice::GV_ON_DOWNLOAD_RECORD_DONE) {
        gcloud_voice::GCloudVoiceErrno rst;
        if (!_isPlayRecorded) {
            rst = gcloud_voice::GetVoiceEngine()->PlayRecordedFile(_dpath.c_str());
            if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
                CCLOG("PlayRecordedFile Error %d", rst);
                return;
            }
            _isPlayRecorded = true;
        }
        else {
            rst = gcloud_voice::GetVoiceEngine()->StopPlayFile();
            if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
                CCLOG("StopPlayFile Error %d", rst);
                return;
            }
            _isPlayRecorded = false;
        }
    }
}

void YvVoiceManager::OnPlayRecordedFile(gcloud_voice::GCloudVoiceCompleteCode code, const char *filePath)
{
    cocos2d::log("YvVoiceManager::OnPlayRecordedFile");
    if (_playFinishCallBack != nullptr)
    {
        _playFinishCallBack();
    }
    _isPlayRecorded = false;
}

void YvVoiceManager::OnApplyMessageKey(gcloud_voice::GCloudVoiceCompleteCode code)
{
    std::string msg;
    if (code == gcloud_voice::GV_ON_MESSAGE_KEY_APPLIED_SUCC) {
        msg = "OnApplyMessageKey success";
    }
    else {
        msg = "OnApplyMessageKey error " + code;
    }
}
void YvVoiceManager::StartRecord(){   
    cocos2d::log("YvVoiceManager::StartRecord");
    this->BtnRecord();
}

void YvVoiceManager::StopRecord(){
    cocos2d::log("YvVoiceManager::StopRecord()");

    this->BtnRecord();
}
void YvVoiceManager::BtnRecord()
{
    gcloud_voice::GCloudVoiceErrno rst;
    static bool once = false;
    if (!once) {
        rst = gcloud_voice::GetVoiceEngine()->StartRecording(_wpath.c_str());
        if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
            CCLOG("StartRecording Error %d", rst);
            return;
        }
        once = true;
    }
    else {
        rst = gcloud_voice::GetVoiceEngine()->StopRecording();
        if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
            CCLOG("StopPlayFile Error %d", rst);   
            return;
        }
        once = false;
        this->UploadFile();
    }
}

void YvVoiceManager::PlayRecord(std::string fileID){
     cocos2d::log("YvVoiceManager::PlayRecord");
     gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->DownloadRecordedFile(fileID.c_str(), _dpath.c_str());
     if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
         CCLOG("DownloadRecordedFile Error %d", rst);
     }

}

void YvVoiceManager::UploadFile(){
    cocos2d::log("YvVoiceManager::UploadFile");
    gcloud_voice::GCloudVoiceErrno rst = gcloud_voice::GetVoiceEngine()->UploadRecordedFile(_wpath.c_str());
    if (rst != gcloud_voice::GCLOUD_VOICE_SUCC) {
        CCLOG("UploadRecordedFile Error %d", rst);
    }
}

void YvVoiceManager::setUploadSuccessCallBack(const std::function<void(const char*fileID)>& callback)
{
    _uploadSuccessCallBack = callback;
}

void YvVoiceManager::setPlayFinishCallBack(const std::function<void()>& callback)
{
    _playFinishCallBack = callback;
}