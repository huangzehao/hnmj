#include "GameBase.h"

#include "Game/GameLib.h"
#include "Game/Sound/ArmFun.h"

#include "ScenceManagerBase.h"
#include "GameManagerBase.h"
#include "Voice/YvVoiceManager.hpp"
#include "JniCross/JniFun.h"

GameBase::GameBase(unsigned int iType,unsigned int iOption)
	:m_pSelfPlayer(NULL)
	,m_kReqPlayerInfo(ScriptData<std::string>("address").Value().c_str(),ScriptData<int>("Port").Value())
	,m_iGameType(iType)
	,m_iGameOption(iOption)
{
	m_kReqPlayerInfo.setMissionSink(this);
}
GameBase::~GameBase()
{

}
bool GameBase::init()
{
	if (!Node::init())
	{
		return false;
	};
	return true;
}
void GameBase::setOptionRule(unsigned int iRule)
{
	m_iGameOption = iRule;
}
bool GameBase::HaveOptionRule(int iRule)
{
	return FvMask::HasAny(m_iGameOption,_MASK_(iRule));
}
void GameBase::Button_ExitGameBase(cocos2d::Ref*,WidgetUserInfo*)
{
	if (IServerItem::get())
	{
		IServerItem::get()->PerformStandUpAction();
	}
	else
	{
		ExitGame();
	}
}

void GameBase::Button_KickUser(dword dwTargetUserID)
{
    if (IServerItem::get())
    {
        IServerItem::get()->PerformOwnerKickUser(dwTargetUserID);
    }
}

void GameBase::sendSelfLocation()
{
    if (IServerItem::get())
    {
        double latiude = JniFun::getLatiude();
        double longitude = JniFun::getLongitude();
        IServerItem::get()->SendUserLocation(longitude, latiude);
    }
}

void GameBase::clearInfo()
{
	while (m_kPlayers.size())
	{
		GamePlayer* pTempPlayer = m_kPlayers[0];
		removeGamePlayerToList(pTempPlayer);
		DeletePlayer(pTempPlayer);
	}
}
void GameBase::ExitGame()
{
	clearInfo();
	ScenceManagerBase::InstanceBase().GameBackScence();
	UserInfo::Instance().reqAccountInfo();
	GameManagerBase::InstanceBase().disconnectServer();
}

void GameBase::OnSocketSubPrivateRoomInfo(CMD_GF_Private_Room_Info* pNetInfo)
{

}
void GameBase::OnSocketSubPrivateEnd(CMD_GF_Private_End_Info* pNetInfo)
{

}
void GameBase::OnSocketSubPrivateDismissInfo(CMD_GF_Private_Dismiss_Info* pNetInfo)
{

}
bool GameBase::BackKey()
{
	if (!IsInGame())
	{
		return false;
	}
	PopScence::Instance().showAccessPlane(utility::getScriptString("GameExitKeyBack"),this,
		button_selector(GameBase::Button_CB_OnExit),NULL);
	return true;
}
void GameBase::Button_CB_OnExit(cocos2d::Ref*,WidgetUserInfo*)
{
	ExitGame();
}

void GameBase::sendTalkFile(int iChair,std::string kFile)
{
	ssize_t iSize = 0;
	std::string kDestFile = kFile;
//#if CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID
//	kDestFile = cocos2d::CCFileUtils::getInstance()->getWritablePath()+utility::toString(time(NULL),".arm");
//	ArmFun::WavToArm(kFile.c_str(),kDestFile.c_str());
//#endif
	CMD_GR_C_TableTalk kNetInfo;
	kNetInfo.cbChairID = iChair;
	kNetInfo.cbType = CMD_GR_C_TableTalk::TYPE_FILE;
	//unsigned char* pData = cocos2d::CCFileUtils::sharedFileUtils()->getFileData(kDestFile,"rb",&iSize);
//	if (!pData)
//	{
//		NoticeMsg::Instance().ShowTopMsg("Record Error");
//		return;
//	}
	kNetInfo.cbChairID = iChair;
    kNetInfo.kDataStream.pushValue((char*)kFile.c_str(), kFile.size());
//	kNetInfo.kDataStream.pushValue((char*)pData, iSize);
//	free(pData);
	datastream kDataStream;
	kNetInfo.StreamValue(kDataStream,true);
    CCLOG("----->before send talkFile data %lu, %d", kDataStream.size(), iSize);
	IServerItem::get()->SendSocketData(MDM_GF_FRAME,SUB_GR_TABLE_TALK,&kDataStream[0],kDataStream.size());
    CCLOG("----->after send talkFile data %lu", kDataStream.size());
}
void GameBase::sendTalkString(int iChair,std::string strString)
{
	CMD_GR_C_TableTalk kNetInfo;
	kNetInfo.cbChairID = iChair;
    kNetInfo.cbType = CMD_GR_C_TableTalk::TYPE_WORD;
    kNetInfo.strString.pushValue(strString);
	datastream kDataStream;
	kNetInfo.StreamValue(kDataStream,true);
	IServerItem::get()->SendSocketData(MDM_GF_FRAME,SUB_GR_TABLE_TALK,&kDataStream[0],kDataStream.size());
   
    
}
void GameBase::sendTalkDefine(int iChair,std::string strSoundPath)
{
    CMD_GR_C_TableTalk kNetInfo;
    kNetInfo.strString.pushValue(strSoundPath);
	kNetInfo.cbChairID = iChair;
	kNetInfo.cbType = CMD_GR_C_TableTalk::TYPE_DEFINE;
	datastream kDataStream;
	kNetInfo.StreamValue(kDataStream,true);
	IServerItem::get()->SendSocketData(MDM_GF_FRAME,SUB_GR_TABLE_TALK,&kDataStream[0],kDataStream.size());
}
void GameBase::sendTalkBiaoQing(int iChair,std::string strFilePath)
{
	CMD_GR_C_TableTalk kNetInfo;
	kNetInfo.strString.pushValue(strFilePath);
	kNetInfo.cbChairID = iChair;
	kNetInfo.cbType = CMD_GR_C_TableTalk::TYPE_BIAOQING;
	datastream kDataStream;
	kNetInfo.StreamValue(kDataStream,true);
	IServerItem::get()->SendSocketData(MDM_GF_FRAME,SUB_GR_TABLE_TALK,&kDataStream[0],kDataStream.size());
}
bool GameBase::RevTalk(void* data,int dataSize)
{
	datastream kDataStream(data,dataSize);
	CMD_GR_C_TableTalk kTalkNet;
	kTalkNet.StreamValue(kDataStream,false);
	if (kTalkNet.cbType == CMD_GR_C_TableTalk::TYPE_FILE)
	{
		RevTalk_File(&kTalkNet);
	}
	if (kTalkNet.cbType == CMD_GR_C_TableTalk::TYPE_WORD)
	{
		RevTalk_String(&kTalkNet);
	}
	if (kTalkNet.cbType == CMD_GR_C_TableTalk::TYPE_DEFINE)
	{
		RevTalk_TalkIdex(&kTalkNet);
	}
	if (kTalkNet.cbType == CMD_GR_C_TableTalk::TYPE_BIAOQING)
	{
		RevTalk_BiaoQing(&kTalkNet);
	}
	return true;
}

void onPlayFinish()
{
   SoundFun::Instance().ResumeBackMusic(0.50f);

}
static int s_lastPlayTime = 0;
bool GameBase::RevTalk_File(CMD_GR_C_TableTalk* pNetInfo)
{
	if (pNetInfo->kDataStream.size() == 0 ){
		return true;
	}
    //if(YvVoiceManager::GetInstance()->isPlaying){
    //    if((int)time(NULL) - s_lastPlayTime > 30 ){
    //        YvVoiceManager::GetInstance()->isPlaying = false;
    //    }
    //    else{
    //        return true;
    //    }
    //    
    //}
   
    /*
	static int iIdex = 0;
	iIdex ++;
	std::string kFile = utility::toString(cocos2d::CCFileUtils::sharedFileUtils()->getWritablePath(),"TableTalk",iIdex,".arm");
	FILE *fp = fopen(kFile.c_str(), "wb");
    printf("GameBase::RevTalk_File recv talk file, size %lu ", pNetInfo->kDataStream.size());
	fseek(fp,0,SEEK_END);
	fseek(fp,0,SEEK_SET);
	fwrite(&pNetInfo->kDataStream[0],sizeof(unsigned char), pNetInfo->kDataStream.size(),fp);
	fclose(fp);
     

	std::string kDestFile = kFile;
	utility::StringReplace(kDestFile,"arm","wav");
	ArmFun::ArmToWav(kFile.c_str(),kDestFile.c_str());
     */
	SoundFun::Instance().PaseBackMusic();
	
	SoundFun::Instance().PaseEffectMusic();
	SoundFun::Instance().ResumeEffectMusic(0.5f);
    
    std::string url(&pNetInfo->kDataStream[0]);
    s_lastPlayTime = (int)time(NULL);
    YvVoiceManager::GetInstance()->PlayRecord(url);
    YvVoiceManager::GetInstance()->setPlayFinishCallBack([=]() {
        onPlayFinish();
    });

	//SoundFun::Instance().playEffectDirect(kDestFile);

	GamePlayer* pPlayer = getPlayerByChairID(pNetInfo->cbChairID);
	if (pPlayer)
	{
		pPlayer->showTalkState(pNetInfo);
	}
	return true;
}
bool GameBase::RevTalk_String(CMD_GR_C_TableTalk* pNetInfo)
{
	GamePlayer* pPlayer = getPlayerByChairID(pNetInfo->cbChairID);
	if (pPlayer)
	{
		pPlayer->showTalkState(pNetInfo);
	}
	return true;
}
bool GameBase::RevTalk_TalkIdex(CMD_GR_C_TableTalk* pNetInfo)
{
	GamePlayer* pPlayer = getPlayerByChairID(pNetInfo->cbChairID);
	SoundFun::Instance().PaseBackMusic();
	SoundFun::Instance().ResumeBackMusic(5.0f);
	if (pPlayer)
	{
		pPlayer->showTalkState(pNetInfo);
	}
	return true;
}

bool GameBase::RevTalk_BiaoQing(CMD_GR_C_TableTalk* pNetInfo)
{
	GamePlayer* pPlayer = getPlayerByChairID(pNetInfo->cbChairID);
	if (pPlayer)
	{
		pPlayer->showTalkState(pNetInfo);
	}
	return true;
}