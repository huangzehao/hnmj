#include "PopScence.h"
#include "ClientHN_THJ/Game/HNMJ/HNMJGameScence.h"
#include "JniCross/JniFun.h"

FV_SINGLETON_STORAGE(PopScence);

PopScence::PopScence()
	:m_pAccessPlanePoint(NULL)
	,m_pAccessPlaneAccessHander(NULL)
	,m_pAccessPlaneCancelHander(NULL)
    , user_id_(0)
    , kick_button_(NULL)
{
	init();
}
PopScence::~PopScence()
{

}
bool PopScence::init()
{
	if (!cocos2d::Node::init())
	{
		return false;
	};
	WidgetScenceXMLparse kScence1("Script/PopScence.xml",this);
	WidgetManager::addButtonCB("Button_PopCancel",this,button_selector(PopScence::Button_PopCancel));
	WidgetManager::addButtonCB("Button_PopAccess",this,button_selector(PopScence::Button_PopAccess));

    kick_button_ = cocos2d::ui::Button::create("GameHNMJ/btn_tiren_0.png", "GameHNMJ/btn_tiren_1.png");
    WidgetFun::getChildWidget(this, "PlayerInfoPlane")->addChild(kick_button_);
    kick_button_->setPosition(cocos2d::Vec2(0.0f, -150.0f));

    kick_button_->addClickEventListener([=](cocos2d::Ref*) {
        assert(user_id_ != 0);
        HNMJGameScence::Instance().Button_KickUser(user_id_);
        HideAll();
    });
	return true;
}
void PopScence::HideAll()
{
	WidgetFun::setVisible(this,"AccessPlane",false);
	WidgetFun::setVisible(this,"PlayerInfoPlane",false);
    user_id_ = 0;
}
void PopScence::setKickButtonVisible(bool visible)
{
    kick_button_->setVisible(visible);
}
void PopScence::showAccessPlane(std::string kTitle,cocos2d::Ref* pPoint,CB_BUTTON_Handler pAccessHander)
{
	HideAll();
	showAccessPlane(kTitle,pPoint,pAccessHander,NULL);
	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"AccessPlane");
	WidgetFun::setVisible(pRootNode,"ACPlane1",true);
	WidgetFun::setVisible(pRootNode,"ACPlane2",false);
}
void PopScence::showAccessPlane(std::string kTitle,cocos2d::Ref* pPoint,CB_BUTTON_Handler pAccessHander,CB_BUTTON_Handler pCancelHander)
{
	HideAll();
	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"AccessPlane");
	m_pAccessPlanePoint = pPoint;
	m_pAccessPlaneAccessHander = pAccessHander;
	m_pAccessPlaneCancelHander = pCancelHander;
	WidgetFun::setText(pRootNode,"AccessTxt",kTitle);
	WidgetFun::ActionStart(pRootNode);
	WidgetFun::setVisible(this,"ACPlane1",false);
	WidgetFun::setVisible(this,"ACPlane2",true);
}
void PopScence::Button_PopCancel(cocos2d::Ref*,WidgetUserInfo*)
{
	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"AccessPlane");
	WidgetFun::ActionEnd(pRootNode);
	if (m_pAccessPlaneCancelHander && m_pAccessPlanePoint)
	{
		(m_pAccessPlanePoint->*m_pAccessPlaneCancelHander)(NULL,NULL);
	}
}
void PopScence::Button_PopAccess(cocos2d::Ref*,WidgetUserInfo*)
{
	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"AccessPlane");
	WidgetFun::ActionEnd(pRootNode);
	if (m_pAccessPlaneAccessHander && m_pAccessPlanePoint)
	{
		(m_pAccessPlanePoint->*m_pAccessPlaneAccessHander)(NULL,NULL);
	}
}

void PopScence::showIPInfo(std::string strNickName,dword userID,std::string strIP,std::string kImagicHttp, double distanceToSelf)
{//distanceToSelf传进来单位:米
	HideAll();
	WidgetFun::setVisible(this,"PlayerInfoPlane",true);
	WidgetFun::setText(this,"PopName",strNickName);
	WidgetFun::setText(this,"PopID", cocos2d::StringUtils::toString(userID));
	WidgetFun::setText(this,"PopIP",strIP);

    user_id_ = userID;


    CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
    tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

    WidgetFun::setVisible(this, "PopLocation", user_id_ == pGlobalUserData->dwUserID);
    WidgetFun::setVisible(this, "PopLocationT", user_id_ == pGlobalUserData->dwUserID);

    WidgetFun::setVisible(this, "PopDistance", distanceToSelf>0 && user_id_ != pGlobalUserData->dwUserID);
    WidgetFun::setVisible(this, "PopDistanceT", distanceToSelf>0 && user_id_ != pGlobalUserData->dwUserID);


    double latiude = JniFun::getLatiude();
    double longitude = JniFun::getLongitude();
    WidgetFun::setText(this, "PopLocation", cocos2d::StringUtils::format("%.2f %.2f", longitude, latiude));


    WidgetFun::setText(this, "PopDistance", cocos2d::StringUtils::format("%.2f km", distanceToSelf / 1000.f));

	ImagicDownManager::Instance().addDown(WidgetFun::getChildWidget(this,"HeadImagic"),kImagicHttp,utility::parseInt(strIP));
}