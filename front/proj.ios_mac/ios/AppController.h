#import <UIKit/UIKit.h>
#import "WXApi.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate, WXApiDelegate,BMKGeneralDelegate,BMKLocationServiceDelegate> {
    UIWindow *window;
    BMKLocationService* _locService;
}

@property(nonatomic, readonly) RootViewController* viewController;

@end

