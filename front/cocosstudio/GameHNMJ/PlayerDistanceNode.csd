<GameFile>
  <PropertyGroup Name="PlayerDistanceNode" Type="Node" ID="b8185f23-136b-4725-ae94-9eee0bd4a2fd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="8" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1365398130" Tag="23" RotationSkewX="45.0000" RotationSkewY="44.9986" IconVisible="False" LeftMargin="-158.1470" RightMargin="-162.8530" TopMargin="-158.6903" BottomMargin="-159.3097" Scale9Enable="True" LeftEage="47" RightEage="47" TopEage="46" BottomEage="46" Scale9OriginX="47" Scale9OriginY="46" Scale9Width="49" Scale9Height="50" ctype="ImageViewObjectData">
            <Size X="321.0000" Y="318.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.3530" Y="-0.3097" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="GameHNMJ/DistanceBack.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_0" ActionTag="474547380" Tag="16" RotationSkewX="45.0000" RotationSkewY="45.0000" IconVisible="False" LeftMargin="5.3663" RightMargin="-185.3663" TopMargin="-112.4292" BottomMargin="76.4292" FontSize="36" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="180.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.3663" Y="94.4292" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_1" ActionTag="494091718" Tag="21" RotationSkewY="-0.0001" IconVisible="False" LeftMargin="-59.7583" RightMargin="-60.2417" TopMargin="3.7704" BottomMargin="-27.7704" FontSize="24" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.2417" Y="-15.7704" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_2" ActionTag="-306786894" Tag="22" RotationSkewX="90.0000" RotationSkewY="90.0004" IconVisible="False" LeftMargin="-50.8936" RightMargin="-57.1064" TopMargin="-69.5717" BottomMargin="45.5717" FontSize="24" LabelText="11.123 km" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="3.1064" Y="57.5717" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_3" ActionTag="1710227505" Tag="23" RotationSkewX="45.0000" RotationSkewY="45.0000" IconVisible="False" LeftMargin="-185.9527" RightMargin="5.9527" TopMargin="74.4564" BottomMargin="-110.4564" FontSize="36" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="180.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-95.9527" Y="-92.4564" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_4" ActionTag="1377000063" Tag="24" RotationSkewX="-45.0000" RotationSkewY="-45.0000" IconVisible="False" LeftMargin="8.8147" RightMargin="-188.8147" TopMargin="75.1624" BottomMargin="-111.1624" FontSize="36" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="180.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="98.8147" Y="-93.1624" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_dis_5" ActionTag="1559571509" Tag="25" RotationSkewX="-45.0000" RotationSkewY="-45.0009" IconVisible="False" LeftMargin="-179.2574" RightMargin="-0.7426" TopMargin="-115.7300" BottomMargin="79.7300" FontSize="36" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="180.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-89.2574" Y="97.7300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>