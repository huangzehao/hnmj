package org.cocos2dx.cpp;


import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.hbgame.hshh.wxapi.WXEntryActivity;



import android.content.Intent;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;


public class Native extends Cocos2dxHelper
{
	public static native void WxLoginGetAccessToken(String kUrl);
	public static native void WxLoginGetFailToken(String Error);
	
	
	public static native void PayJFTResult(int payStatus);
	
//	public static void DoJFTPay(String userid, int payNumble) {
//		AppActivity.getInstance().doJFTpay(userid, payNumble);	
//	}	
	
	static double latitude_ = 0;
	static double longitude_ = 0;
	public static void GetLocationSuccess(double latitude, double longitude)
	{
		latitude_ = latitude;
		longitude_ = longitude;
		
//	    LatLng p1 = new LatLng(latitude_,longitude_);
//	    LatLng p2 = new LatLng(23,111);
//		double dis = DistanceUtil.getDistance(p1, p2);
//		dis = DistanceUtil.getDistance(p1, p2);
	}
	
	public static double GetDistanceToSelf(double latitude_0, double longitude_0, double latitude_1, double longitude_1)
	{
		if (latitude_0 == 0 && longitude_0 ==0) {
			return 0;
		}
		if (latitude_1 == 0 && longitude_1 ==0) {
			return 0;
		}
	    LatLng p1 = new LatLng(latitude_0,longitude_0);
	    LatLng p2 = new LatLng(latitude_1,longitude_1);
		double dis = DistanceUtil.getDistance(p1, p2);
		return dis;
	}
	
	 public static double GetLatitude()
     {
		 return latitude_;
     } 
	 public static double GetLongitude()
     {
		 return longitude_;
     } 
	
	 public static void LoginWX(String APP_ID,String AppSecret)
	  {
		 Log.i("WXEntryActivity", "开始调用");
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		intent.putExtra(WXEntryActivity.ReqWxLogin,"wxlogin");
		 Cocos2dxActivity.getContext().startActivity(intent);
	  }
	 public static void ShareImageWX(String ImgPath,int nType)
     {
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		 intent.putExtra(WXEntryActivity.ReqWxShareImg,"ReqWxShareImg");
		 intent.putExtra("ImgPath",ImgPath);
		 intent.putExtra("ShareType",nType);
		 Cocos2dxActivity.getContext().startActivity(intent);
     } 
	 public static void ShareTextWX(String text,int nType)
     {
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		 intent.putExtra(WXEntryActivity.ReqWxShareTxt,"ReqWxShareTxt");
		 intent.putExtra("ShareText",text);
		 intent.putExtra("ShareType",nType);
		 Cocos2dxActivity.getContext().startActivity(intent);
     } 
	 
	 public static void ShareUrlWX(String url,String title,String desc,int nType)
     {
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		 intent.putExtra(WXEntryActivity.ReqWxShareUrl,"ReqWxShareUrl");
		 intent.putExtra("ShareUrl",url);
		 intent.putExtra("ShareTitle",title);
		 intent.putExtra("ShareDesc",desc);
		 intent.putExtra("ShareType",nType);
		 Cocos2dxActivity.getContext().startActivity(intent);
     } 
	 
	 public static void showWebView(String kUrl)
     {
		 Log.d("showWebView",kUrl);
		 Uri uri = Uri.parse(kUrl);
		 Intent it = new Intent(Intent.ACTION_VIEW, uri);    
		 Cocos2dxActivity.getContext().startActivity(it);
     }

	public static void versionUpdate(final String url, final String info,final int size, final int isUpdate) 
	{
		 Log.d("versionUpdate","1111");
		QYFun.getInstance().setContext(Cocos2dxActivity.getContext());

		 Log.d("versionUpdate","2222");
		new Thread() {
			@Override
			public void run() {

				 Log.d("versionUpdate","3333");
				QYFun.getInstance().showVersionUpdate(url,info,size,isUpdate);
			}
		}.start();
		
	}

	 public static void startSoundRecord()
	 {
		 String SoundFilePath= Environment.getExternalStorageDirectory().getAbsolutePath();  
		 String SoundFileName = "soundRecord.wav";

		 Log.d("startSoundRecord",SoundFilePath);
		 //ExtAudioRecorder recorder = ExtAudioRecorder.getInstanse(false);
		 ExtAudioRecorder.recordChat(SoundFilePath+"/",SoundFileName);
		 
	 }
	 
	 public static String stopSoundRecord()
	 {
		 return ExtAudioRecorder.stopRecord();
	 }
	 
	 public static void connectRongIM(String token) {
		 //AppActivity.getInstance().connect(token);
	 }
}
