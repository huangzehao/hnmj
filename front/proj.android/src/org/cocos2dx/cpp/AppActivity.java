/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.cpp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.mapapi.SDKInitializer;
import com.tencent.gcloud.voice.GCloudVoiceEngine;


import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.os.*;
import android.os.PowerManager.WakeLock;

import android.util.Log;
import android.widget.Toast;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;




public class AppActivity extends Cocos2dxActivity {
//	static{
//		System.loadLibrary("BaiduMapSDK");
//	}
	static AppActivity instance = null;
	ProgressDialog mProgressDlg = null;
	private int downLoadFileSize;
	public PowerManager powerManager=null;
	public WakeLock wakeLock=null;
	
	public LocationClient mLocationClient = null;
	public BDLocationListener myListener = new MyLocationListener();
	

	public static AppActivity getInstance() {  
	    return instance;  
	}  	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		powerManager = (PowerManager) this
				.getSystemService(this.POWER_SERVICE);
		wakeLock = this.powerManager.newWakeLock(
				PowerManager.FULL_WAKE_LOCK, "My Lock");
		SDKInitializer.initialize(getApplicationContext());
//		com.yunva.im.sdk.lib.YvLoginInit.initApplicationOnCreate(this.getApplication(), "1000517");
		GCloudVoiceEngine.getInstance().init(getApplicationContext(), this);
		
		//融云sdk
		RongIM.init(this);
		
	    mLocationClient = new LocationClient(getApplicationContext());     
	    //声明LocationClient类
	    mLocationClient.registerLocationListener( myListener );    
	    //注册监听函数
	    mLocationClient.start();
	    mLocationClient.requestLocation();
	    
	}
	@Override
	protected void onDestroy(){
		super.onDestroy();
//		com.yunva.im.sdk.lib.YvLoginInit.release();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	// 打开下载文件安装
	private void openFile(File file) {
		Intent intent = new Intent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),
				"application/vnd.android.package-archive");
		startActivity(intent);
		this.finish();
	}

	// 下载文件
	public void downFile(String url, String path) throws IOException {
		String filename = url.substring(url.lastIndexOf("/") + 1);
		// 获取文件名
		URL myURL = new URL(url);
		URLConnection conn = myURL.openConnection();
		conn.connect();
		InputStream inputstream = conn.getInputStream();
		final File file = new File(path + filename);
		if (inputstream == null)
			throw new RuntimeException("stream is null");
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		// 把数据存入路径+文件名
		byte buf[] = new byte[512];
		downLoadFileSize = 0;
		sendMsg(0);
		do {
			int numread = inputstream.read(buf);
			if (numread == -1) {
				break;
			}
			fileOutputStream.write(buf, 0, numread);
			downLoadFileSize += numread;
			sendMsg(1);// 更新进度条
		} while (true);
		sendMsg(2);// 通知下载完成
		try {
			fileOutputStream.close();
			inputstream.close();
		} catch (Exception ex) {
			Log.e("tag", "error: " + ex.getMessage(), ex);
		}
	}

	private void sendMsg(int flag, String url, int size, String info,
			int isUpdate) {
		Message msg = new Message();
		msg.what = flag;
		Bundle bundle = new Bundle();
		bundle.putInt("size", size);
		bundle.putString("url", url);
		bundle.putInt("isUpdate", isUpdate);
		bundle.putString("info", info);
		msg.setData(bundle);
		handler.sendMessage(msg);
	}

	private void sendMsg(int flag) {
		Message msg = new Message();
		msg.what = flag;
		handler.sendMessage(msg);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		wakeLock.acquire();
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		wakeLock.release();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (!Thread.currentThread().isInterrupted()) {
				switch (msg.what) {
				case 1:
					if (mProgressDlg != null) {
						mProgressDlg.setProgress(downLoadFileSize / 1024);
					}

					break;
				case 2:
					Toast.makeText(AppActivity.this, "文件下载完成", 1).show();
					String absPath = Environment.getExternalStorageDirectory()
							+ "/Android/data/"
							+ getApplicationInfo().packageName + "/files/";
					openFile(new File(absPath/* + filename */));
					break;
				case 3:
					Toast.makeText(AppActivity.this, "请检查wifi网络是否开启!", 1)
							.show();
					break;
				case -1:
					Toast.makeText(AppActivity.this, "网络异常，文件下载失败!", 1).show();
					// Cocos2dxHelper.ExitUpdate(0);
					break;
				case 4:
					break;
				}
			}

		}
	};
	
	/***
	 * 定位结果回调，在此方法中处理定位结果
	 */
	
	class MyLocationListener implements BDLocationListener{

		@Override
		public void onConnectHotSpotMessage(String s, int i) {
			// TODO Auto-generated method stub
//			Log.i("lxm", i+"  "+s);
//			String resText = "";
//			
//			if(i == 0){
//				resText = "不是wifi热点, wifi:"+s;
//			} else if(i == 1){
//				resText = "是wifi热点, wifi:"+s;
//			} else if (i == -1){
//				resText = "未连接wifi";
//			}
        	//res.setText(resText);
		}

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null) {
				return;
			}
		    double latitude =  location.getLatitude();    //获取纬度信息		      
		    double longitude = location.getLongitude();    //获取经度信息		      
		    //mLocationClient.stop();
		    Native.GetLocationSuccess(latitude, longitude);
		}
		
	}
	
	
//	@SuppressLint("DefaultLocale")
//	public void doJFTpay(String userid, int payNumble) {
//		String goodsname = String.format("%d元宝", payNumble);
//		float fPayNumble = payNumble;
//		String goodsprice = String.format("%.2f", fPayNumble);
//		Intent intent=new Intent();   
//        intent.putExtra("GoodsName", goodsname);
//        intent.putExtra("GoodsPrice", goodsprice);
//        intent.putExtra("CustomName", userid);
//        intent.putExtra("UserID", userid);
//		intent.setClass(AppActivity.this, PayActivity.class); 
//		startActivity(intent); 
//
//		
////		Intent intent=new Intent();   
////		intent.setClass(AppActivity.this, GoodsListActivity.class); 
////		startActivity(intent); 
//	}
    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
	/**
	 * <p>连接服务器，在整个应用程序全局，只需要调用一次，需在 {@link #init(Context)} 之后调用。</p>
	 * <p>如果调用此接口遇到连接失败，SDK 会自动启动重连机制进行最多10次重连，分别是1, 2, 4, 8, 16, 32, 64, 128, 256, 512秒后。
	 * 在这之后如果仍没有连接成功，还会在当检测到设备网络状态变化时再次进行重连。</p>
	 *
	 * @param token    从服务端获取的用户身份令牌（Token）。
	 * @param callback 连接回调。
	 * @return RongIM  客户端核心类的实例。
	 */
	public void connect(String token) {

	    if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext()))) {

	        RongIM.connect(token, new RongIMClient.ConnectCallback() {

	            /**
	             * Token 错误。可以从下面两点检查 1.  Token 是否过期，如果过期您需要向 App Server 重新请求一个新的 Token
	             *                  2.  token 对应的 appKey 和工程里设置的 appKey 是否一致
	             */
	            @Override
	            public void onTokenIncorrect() {

	            }

	            /**
	             * 连接融云成功
	             * @param userid 当前 token 对应的用户 id
	             */
	            @Override
	            public void onSuccess(String userid) {
	                Log.d("LoginActivity", "--onSuccess" + userid);
	                //startActivity(new Intent(LoginActivity.this, MainActivity.class));
	                //finish();
	            }

	            /**
	             * 连接融云失败
	             * @param errorCode 错误码，可到官网 查看错误码对应的注释
	             */
	            @Override
	            public void onError(RongIMClient.ErrorCode errorCode) {

	            }
	        });
	    }
	}
}
