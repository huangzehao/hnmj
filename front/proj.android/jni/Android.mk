LOCAL_PATH := $(call my-dir)
LOCAL_SHORT_COMMANDS := true

include $(CLEAR_VARS)
LOCAL_MODULE := libGCloudVoice
LOCAL_SRC_FILES := ../../GCloudVoice/libs/Android/$(TARGET_ARCH_ABI)/libGCloudVoice.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libBaiduMapSDK
LOCAL_SRC_FILES := ../../BaiduLBS_AndroidSDK_Lib/libs/$(TARGET_ARCH_ABI)/libBaiduMapSDK_base_v4_3_1.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := liblocSDK7a
LOCAL_SRC_FILES := ../../BaiduLBS_AndroidSDK_Lib/libs/$(TARGET_ARCH_ABI)/liblocSDK7a.so
include $(PREBUILT_SHARED_LIBRARY)

# include $(CLEAR_VARS)
# LOCAL_MODULE := libRongIMLib
# LOCAL_SRC_FILES := ../../Rong_Cloud_Android_IMKit_SDK_v2_8_16_Stable/IMLib/libs/$(TARGET_ARCH_ABI)/libRongIMLib.so
# include $(PREBUILT_SHARED_LIBRARY)

$(call import-add-path,$(LOCAL_PATH)/../..)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared



# LOCAL_SHARED_LIBRARIES += libYvImSdk

LOCAL_MODULE_FILENAME := libcocos2dcpp

FILE_LIST := hellocpp/main.cpp  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/kernel/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/kernel/game/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/kernel/server/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/kernel/user/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/network/*.cpp)
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/network/client_net/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Kernel/socket/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFDefine/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFDefine/data/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFDefine/df/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFDefine/msg/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFKernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/LoadScene/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/LoginScene/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/ModeScene/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/ServerListScene/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Platform/PFView/ServerScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/pthread/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Tools/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Tools/core/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Tools/Dialog/*.cpp)  
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Tools/Manager/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Tools/tools/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/FV/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Game/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Script/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Widget/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Sound/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Sound/amr_nb/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Game/Sound/amr_nb/*.c)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/GameNet/cocosHttp/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/GameNet/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/JniCross/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ClientHN_THJ/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ClientHN_THJ/Game/HNMJ/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ClientHN_THJ/Main/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ClientHN_THJ/Scene/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../GameLib/Voice/*.cpp)

LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)


LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes/ClientHN_THJ/Main
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../GameLib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../GCloudVoice/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES  += android_support
LOCAL_SHARED_LIBRARIES += libGCloudVoice
LOCAL_SHARED_LIBRARIES += libBaiduMapSDK
LOCAL_SHARED_LIBRARIES += liblocSDK7a
LOCAL_SHARED_LIBRARIES += libRongIMLib

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
# $(call import-module,IM_SDK)
# $(call import-module,GCloudVoice)

$(call import-module,android/support)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
