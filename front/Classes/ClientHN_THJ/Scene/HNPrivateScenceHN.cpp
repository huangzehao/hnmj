#include "HNPrivateScenceHN.h"
#include "HNScenceManager.h"
#include "Game/GameLib.h"
#include "kernel/kernel/server/CServerItem.h"
#include "ClientHN_THJ/Game/HNMJ/CMD_HNMJ.h"
#include "cocostudio/CocoStudio.h"
#include "Game/Game/MissionWeiXin.h"

FV_SINGLETON_STORAGE(HNPrivateScenceHN);

HNPrivateScenceHN::HNPrivateScenceHN()
	:m_iPlayCoutIdex(0)
	,m_dwPlayRule(0)
	,m_cbGameTypeIdex(GAME_TYPE_HH)
	//, m_iGameChiHuCellScore(0)
	, m_costType(Type_Private_QuanBao)
    , isBanDisReady_(true)
{
	init();
}
HNPrivateScenceHN::~HNPrivateScenceHN()
{

}

void HNPrivateScenceHN::OnSocketSubPrivateCreateSuceess(CMD_GR_Create_Private_Sucess * pNetInfo)
{
    m_roomID = pNetInfo->dwRoomNum;
    cocos2d::Node* enterLayer = cocos2d::CSLoader::createNode("EnterPrivateLayer.csb");
    this->addChild(enterLayer);

    dynamic_cast<cocos2d::ui::Text*>(enterLayer->getChildByName("Text_room"))->setString(cocos2d::StringUtils::toString(pNetInfo->dwRoomNum));

    strcpy(m_kJoinNumTxt, cocos2d::StringUtils::toString(pNetInfo->dwRoomNum).c_str());

    auto* enterBtn = dynamic_cast<cocos2d::ui::Button*>(enterLayer->getChildByName("Button_jiaruyouxi"));
    enterBtn->addClickEventListener([=](cocos2d::Ref*) {
        m_kActJoinNum = utility::parseInt(m_kJoinNumTxt);
        int iServerID = m_kActJoinNum / 10000 - 10;
        CGameServerItem* pGameServer = GameManagerBase::InstanceBase().SearchGameServer(iServerID);
        if (pGameServer && pGameServer->IsPrivateRoom())
        {
            GameManagerBase::InstanceBase().connectGameServerByServerID(iServerID);
            m_eLinkAction = Type_Link_Join;
            enterLayer->removeFromParent();
        }
        else
        {
            NoticeMsg::Instance().ShowTopMsgByScript("JoinRoomNumError");
        }
    });

    dynamic_cast<cocos2d::ui::Button*>(enterLayer->getChildByName("Button_close"))->addClickEventListener(
        [=](cocos2d::Ref*) {
        enterLayer->removeFromParent();
    });

    dynamic_cast<cocos2d::ui::Button*>(enterLayer->getChildByName("Button_yaoqing"))->addClickEventListener(
        [=](cocos2d::Ref*) {
        std::string kTitle = utility::getScriptReplaceValue("HNWeiXinSharTitle",
            cocos2d::StringUtils::toString(m_roomID), "");

        std::string kDes = this->getWeiXinShareDes(m_roomID);

        MissionWeiXin::Instance().shareUrlWeiXin(utility::getScriptString("HNWeiXinSharUrl"),
            kTitle,
            kDes,
            MissionWeiXin::SHARE_SESSION);
    });
}

bool HNPrivateScenceHN::init()
{
	if (!cocos2d::CCNode::init())
	{
		return false;
	};

	WidgetScenceXMLparse kScence1("Script/HNPrivateScenceHN.xml",this);

	WidgetManager::addButtonCB("Button_JoinRoom",this,button_selector(HNPrivateScenceHN::Button_JoinNumReset));
	WidgetManager::addButtonCB("Button_JoinNum",this,button_selector(HNPrivateScenceHN::Button_JoinNum));
	WidgetManager::addButtonCB("Button_JoinNumDel",this,button_selector(HNPrivateScenceHN::Button_JoinNumDel));
	WidgetManager::addButtonCB("Button_JoinNumReset",this,button_selector(HNPrivateScenceHN::Button_JoinNumReset));
	
	WidgetManager::addButtonCB("Button_Show_Create_Private",this,button_selector(HNPrivateScenceHN::Button_Show_Create_Private));
	WidgetManager::addButtonCB("Button_Show_Create_Public",this,button_selector(HNPrivateScenceHN::Button_Show_Create_Public));
	WidgetManager::addButtonCB("Button_Show_Join_Private",this,button_selector(HNPrivateScenceHN::Button_Show_Join_Private));
	WidgetManager::addButtonCB("Button_CreateRoom",this,button_selector(HNPrivateScenceHN::Button_CreateRoom));
	WidgetManager::addButtonCB("Button_JoinPublic",this,button_selector(HNPrivateScenceHN::Button_JoinPublic));
	WidgetManager::addButtonCB("Button_DismissPrivate",this,button_selector(HNPrivateScenceHN::Button_DismissPrivate));
	WidgetManager::addButtonCB("Button_DismissPrivateNot",this,button_selector(HNPrivateScenceHN::Button_DismissPrivateNot));
	WidgetManager::addButtonCB("Button_PrivateAgagin",this,button_selector(HNPrivateScenceHN::Button_PrivateAgagin));

	WidgetManager::addButtonCB("Button_GameCout0",this,button_selector(HNPrivateScenceHN::Button_PrivatePlayCout));
	WidgetManager::addButtonCB("Button_GameCout1",this,button_selector(HNPrivateScenceHN::Button_PrivatePlayCout));

	WidgetManager::addButtonCB("Button_GameRule0",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
	WidgetManager::addButtonCB("Button_GameRule1",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
	WidgetManager::addButtonCB("Button_GameRule2",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
	WidgetManager::addButtonCB("Button_GameRule3",this,button_selector(HNPrivateScenceHN::ButtonGameChiHuCellScore16));
	WidgetManager::addButtonCB("Button_GameRule4",this,button_selector(HNPrivateScenceHN::ButtonGameChiHuCellScore20));
    WidgetManager::addButtonCB("Button_GameRule5",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
    WidgetManager::addButtonCB("Button_GameRule6",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
    WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule6",true);
    WidgetManager::addButtonCB("Button_GameRule8",this,button_selector(HNPrivateScenceHN::ButtonGameRule));
	WidgetManager::addButtonCB("Button_GameType0",this,button_selector(HNPrivateScenceHN::Button_GameType));
	WidgetManager::addButtonCB("Button_GameType1",this,button_selector(HNPrivateScenceHN::Button_GameType));
    WidgetManager::addButtonCB("Button_GameDisReady", this, button_selector(HNPrivateScenceHN::Button_BanDisReady));

	WidgetFun::setVisible(this, "Button_GameType1", false);
	WidgetFun::setVisible(this, "Node_CSMJ", false);
	//WidgetFun::setVisible(this, "Label_KeXuan", false);
	//WidgetFun::setVisible(this, "Node_ZhaNiao_2", false);
	//WidgetFun::setVisible(this, "Node_ZhaNiao_4", false);
	WidgetFun::setVisible(this, "Node_ZhaNiao_6", false);
	WidgetFun::setVisible(this, "CheckBox_HZLZ", false);
	//WidgetFun::setChecked(this, "Button_GameRule2", true);
	FvMask::Add(m_dwPlayRule, _MASK_((dword)GAME_TYPE_ZZ_HONGZHONG));
	//FvMask::Add(m_dwPlayRule, _MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU));
	this->ButtonGameChiHuCellScore16(nullptr, nullptr);

	this->updateGameRule();

	setPlayerCoutIdex(0);
	return true;
}
void HNPrivateScenceHN::hideAll()
{
	WidgetFun::setVisible(this,"CreateRoomPlane",false);
	WidgetFun::setVisible(this,"JoinRoomPlane",false);
}
bool HNPrivateScenceHN::BackKey()
{
	if (WidgetFun::isWidgetVisble(this,"CreateRoomPlane"))
	{
		WidgetFun::setVisible(this,"CreateRoomPlane",false);
		return true;
	}
	if (WidgetFun::isWidgetVisble(this,"JoinRoomPlane"))
	{
		WidgetFun::setVisible(this,"JoinRoomPlane",false);
		return true;
	}
	return false;
}
void HNPrivateScenceHN::setPlayerCoutIdex(int iIdex)
{
	for (int i = 0;i<2;i++)
	{
		WidgetFun::setChecked(this,utility::toString("Button_GameCout",i),iIdex == i);
	}
	m_iPlayCoutIdex = iIdex;
}
void HNPrivateScenceHN::setGameChiHuCellScore(int score)
{
    m_cellScore = score;
	WidgetFun::setChecked(this, "Button_GameRule3", score == 16);
	WidgetFun::setChecked(this, "Button_GameRule4", score == 20);
}
void HNPrivateScenceHN::setGameRuleIdex(int iIdex)
{
	if (iIdex == GAME_TYPE_ZZ_QIANGGANGHU)
	{
		m_costType = Type_Private_QuanBao;
	}
	if (iIdex == GAME_TYPE_ZZ_ZIMOHU)
	{
		m_costType = Type_Private_Card_AA;
	}
	//if (iIdex == GAME_TYPE_ZZ_HONGZHONG)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_258));
	//	if (!FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU))
	//		&&!FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZIMOHU)))
	//	{
	//		FvMask::Add(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU));
	//	}
	//}
	//if (iIdex == GAME_TYPE_ZZ_258)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZIMOHU));
	//	//FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_258));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_HONGZHONG));
 //       //FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_CS_ZHUANGXIANFEN));
	//}
	//if (iIdex == GAME_TYPE_ZZ_QIANGGANGHU 
	//	||iIdex == GAME_TYPE_ZZ_ZIMOHU)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZIMOHU));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_258));
	//}
	//if (iIdex == GAME_TYPE_ZZ_ZHANIAO2)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO4));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO6));
	//}
	//if (iIdex == GAME_TYPE_ZZ_ZHANIAO4)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO2));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO6));
	//}
	//if (iIdex == GAME_TYPE_ZZ_ZHANIAO6)
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO4));
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO2));
	//}
 //   if(iIdex == GAME_TYPE_CS_ZHUANGXIANFEN){
 //       FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU));
 //       FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZIMOHU));
 //       //FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_258));
 //       FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_HONGZHONG));
 //       //FvMask::Del(m_dwPlayRule,_MASK_((dword)GAME_TYPE_CS_ZHUANGXIANFEN));
 //   }
	//if (FvMask::HasAny(m_dwPlayRule,_MASK_((dword)iIdex)))
	//{
	//	FvMask::Del(m_dwPlayRule,_MASK_((dword)iIdex));
	//}
	//else
	//{
	//	FvMask::Add(m_dwPlayRule,_MASK_((dword)iIdex));
	//}

	updateGameRule();
}

void HNPrivateScenceHN::updateGameRule()
{
	//WidgetFun::setChecked(this,"Button_GameRule0",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIANGGANGHU)));
	//WidgetFun::setChecked(this,"Button_GameRule1",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZIMOHU)));
	//WidgetFun::setChecked(this,"Button_GameRule2",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_HONGZHONG)));
	//WidgetFun::setChecked(this,"PlaneCreate_ZZ:Button_GameRule3",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO2)));
	//WidgetFun::setChecked(this,"PlaneCreate_ZZ:Button_GameRule4",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO4)));
	//WidgetFun::setChecked(this,"PlaneCreate_ZZ:Button_GameRule5",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO6)));
	//WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule3",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO2)));
 //   WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule4",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO4)));
 //   WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule5",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_ZHANIAO6)));
 //   WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule6",true);


	WidgetFun::setChecked(this, "Button_GameRule0", m_costType == Type_Private_QuanBao);
	WidgetFun::setChecked(this, "Button_GameRule1", m_costType == Type_Private_Card_AA);

    WidgetFun::setChecked(this, "Button_GameDisReady", isBanDisReady_ == true);

    //WidgetFun::setChecked(this,"PlaneCreate_CS:Button_GameRule8",FvMask::HasAny(m_dwPlayRule,_MASK_((dword)GAME_TYPE_CS_ZHUANGXIANFEN)));
    //WidgetFun::setChecked(this,"PlaneCreate_ZZ:Button_GameRule7",true);
    
}

void HNPrivateScenceHN::Button_Show_Create_Private(cocos2d::Ref*,WidgetUserInfo* pInfo)
{
	WidgetFun::setVisible(this,"CreateRoomPlane",true);
	UserInfo::Instance().checkInGameServer();
	WidgetFun::setVisible(this,"PlaneCreate",true);
	WidgetFun::setVisible(this,"PlanePublic",false);
}
void HNPrivateScenceHN::Button_Show_Create_Public(cocos2d::Ref*,WidgetUserInfo*)
{
	WidgetFun::setVisible(this,"CreateRoomPlane",true);
	UserInfo::Instance().checkInGameServer();
	WidgetFun::setVisible(this,"PlaneCreate",false);
	WidgetFun::setVisible(this,"PlanePublic",true);
}
void HNPrivateScenceHN::Button_Show_Join_Private(cocos2d::Ref*,WidgetUserInfo* pInfo)
{
	WidgetFun::setVisible(this,"JoinRoomPlane",true);
	Button_JoinNumReset(NULL,NULL);
	UserInfo::Instance().checkInGameServer();
}

void HNPrivateScenceHN::Button_PrivateAgagin(cocos2d::Ref*,WidgetUserInfo*)
{
	CMD_GR_Again_Private kNetInfo = CMD_GR_Again_Private();
	//CServerItem::get()->SendSocketData(MDM_GR_PRIVATE,SUB_GR_RIVATE_AGAIN,&kNetInfo,sizeof(kNetInfo));
}
void HNPrivateScenceHN::Button_CreateRoom(cocos2d::Ref*,WidgetUserInfo*)
{
    if(m_cbGameTypeIdex==(int)GAME_TYPE_ZZ){
        FvMask::Add(m_dwPlayRule,_MASK_((dword)GAME_TYPE_ZZ_QIDUI));
    }
    CMD_GR_Create_Private kSendNet;
    zeromemory(&kSendNet,sizeof(kSendNet));
    kSendNet.cbGameType = Type_Private;
    kSendNet.bGameTypeIdex = m_cbGameTypeIdex;
	kSendNet.bGameRuleIdex = m_dwPlayRule;
	kSendNet.bPlayCoutIdex = m_iPlayCoutIdex;
    kSendNet.bRoomCardCostType = m_costType;
    kSendNet.isBanDisReady = isBanDisReady_;
	ConnectAndCreatePrivateByKindID(310,kSendNet);
}
void HNPrivateScenceHN::Button_JoinPublic(cocos2d::Ref*,WidgetUserInfo*)
{
	CMD_GR_Create_Private kSendNet;
	zeromemory(&kSendNet,sizeof(kSendNet));
	kSendNet.cbGameType = Type_Public;
	kSendNet.bGameRuleIdex = m_dwPlayRule;
	kSendNet.bGameTypeIdex = m_cbGameTypeIdex;
	kSendNet.bPlayCoutIdex = m_iPlayCoutIdex;
	kSendNet.bRoomCardCostType = m_costType;
	ConnectAndCreatePrivateByKindID(310,kSendNet);
}
void HNPrivateScenceHN::Button_PrivatePlayCout(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	int iIdex = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Idex"));
	setPlayerCoutIdex(iIdex);

}
void HNPrivateScenceHN::ButtonGameRule(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	int iIdex = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Idex"));
	setGameRuleIdex(iIdex);
}

void HNPrivateScenceHN::ButtonGameChiHuCellScore16(cocos2d::Ref *, WidgetUserInfo *pUserInfo)
{
	FvMask::Del(m_dwPlayRule, _MASK_((dword)GAME_TYPE_HSHH_DIFEN20));
	FvMask::Add(m_dwPlayRule, _MASK_((dword)GAME_TYPE_HSHH_DIFEN16));
	this->setGameChiHuCellScore(16);
}

void HNPrivateScenceHN::ButtonGameChiHuCellScore20(cocos2d::Ref *, WidgetUserInfo *pUserInfo)
{
	FvMask::Del(m_dwPlayRule, _MASK_((dword)GAME_TYPE_HSHH_DIFEN16));
	FvMask::Add(m_dwPlayRule, _MASK_((dword)GAME_TYPE_HSHH_DIFEN20));
	this->setGameChiHuCellScore(20);
}

void HNPrivateScenceHN::Button_GameType(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	m_cbGameTypeIdex = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Idex"));
	if (m_cbGameTypeIdex == GAME_TYPE_ZZ)
	{
		m_dwPlayRule = 0;
		//setGameRuleIdex(GAME_TYPE_ZZ_QIANGGANGHU);
		WidgetFun::setChecked(this,"Button_GameType0",true);
		WidgetFun::setChecked(this,"Button_GameType1",false);
	}
	if (m_cbGameTypeIdex == GAME_TYPE_CS)
	{
		m_dwPlayRule = 0;
		setGameRuleIdex(GAME_TYPE_ZZ_258);
		updateGameRule();
		WidgetFun::setChecked(this,"Button_GameType0",false);
		WidgetFun::setChecked(this,"Button_GameType1",true);
	}
}

void HNPrivateScenceHN::Button_BanDisReady(cocos2d::Ref *, WidgetUserInfo *)
{
    isBanDisReady_ = !isBanDisReady_;
    this->updateGameRule();
}

std::string HNPrivateScenceHN::getWeiXinShareDes(int roomNum)
{
    std::string paytype;
    if (m_costType == Type_Private_QuanBao)
    {
        paytype = utility::getScriptString("MJRule3");
    }
    else
    {
        paytype = utility::getScriptString("MJRule1");
    }
    
    return cocos2d::StringUtils::format(utility::getScriptString("HNWeiXinSharDes1").c_str(),
        roomNum, m_iPlayCoutIdex == 0 ? 8 : 16, m_cellScore, paytype.c_str());
}
