#pragma once
#include "cocos2d.h"
#include "Game/FV/FvSingleton.h"
#include "Game/Widget/WidgetDefine.h"
#include "Game/Game/HNPrivateScenceBase.h"
#include "Kernel/kernel/server/IServerPrivateSink.h"

class HNPrivateScenceHN
	:public HNPrivateScenceBase
	,public FvSingleton<HNPrivateScenceHN> 
{
public:
	HNPrivateScenceHN();
	~HNPrivateScenceHN();
    void OnSocketSubPrivateCreateSuceess(CMD_GR_Create_Private_Sucess* pNetInfo) override;
public:
	void hideAll();
	void setPlayerCoutIdex(int iIdex);
	void setGameRuleIdex(int iIdex);
	void updateGameRule();
	//��Ϸ���Ƶ׷�
	void setGameChiHuCellScore(int score);
public:
	virtual bool init();
	bool BackKey();
public:

	void Button_Show_Create_Public(cocos2d::Ref*,WidgetUserInfo*);
	void Button_Show_Create_Private(cocos2d::Ref*,WidgetUserInfo*);
	void Button_Show_Join_Private(cocos2d::Ref*,WidgetUserInfo*);
	
	void Button_CreateRoom(cocos2d::Ref*,WidgetUserInfo*);
	void Button_JoinPublic(cocos2d::Ref*,WidgetUserInfo*);
	void Button_PrivateAgagin(cocos2d::Ref*,WidgetUserInfo*);
	
	void Button_PrivatePlayCout(cocos2d::Ref*,WidgetUserInfo*);
	void ButtonGameRule(cocos2d::Ref*, WidgetUserInfo*);
	void ButtonGameChiHuCellScore16(cocos2d::Ref*, WidgetUserInfo*);
	void ButtonGameChiHuCellScore20(cocos2d::Ref*,WidgetUserInfo*);
    void Button_GameType(cocos2d::Ref*, WidgetUserInfo*);
    void Button_BanDisReady(cocos2d::Ref*,WidgetUserInfo*);
	
    std::string getWeiXinShareDes(int roomNum);
private:
    int m_cellScore;
    int m_iPlayCoutIdex;
    RoomCardCostType m_costType;
	int m_cbGameTypeIdex;

	dword m_dwPlayRule;
    bool isBanDisReady_;
};