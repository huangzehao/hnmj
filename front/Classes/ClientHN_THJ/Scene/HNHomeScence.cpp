#include "HNHomeScence.h"
#include "HNScenceManager.h"
#include "Game/GameLib.h"
#include "JniCross/JniFun.h"
#include "ClientHN_THJ/Game/HNMJ/HNMJSoundFun.h"

FV_SINGLETON_STORAGE(HNHomeScence);

HNHomeScence::HNHomeScence()
{
	init();
	CServerListData::shared()->SetServerListDataSink(this);
	UserInfo::Instance().addUpPlayerInfoCB(this,QY_CALLFUNC_SELECTOR(HNHomeScence::upPlayerInfo));
	UserInfo::Instance().addLoginSucessCB(this,QY_CALLFUNC_SELECTOR(HNHomeScence::LogonSucess));
}
HNHomeScence::~HNHomeScence()
{

}
bool HNHomeScence::init()
{
	if (!cocos2d::CCNode::init())
	{
		return false;
	};
	WidgetScenceXMLparse kScence1("Script/HNHomeScence.xml",this);

	init_Button();

	return true;
}

void HNHomeScence::UpdatePaomadeng(std::string paomadeng){
    utility::runPaoMaDeng(this, "SysSpeakerTxt" ,"LayoutNode", paomadeng, NULL);
}

void HNHomeScence::UpdateChongKaNotice(std::string notice){
    
    std::string myKey = "ShopTxt";
    if (WidgetFun::getChildWidget(this,myKey) != NULL)
    {
        WidgetFun::setText(this,myKey,notice);
    }
}

void HNHomeScence::EnterScence()
{
	upPlayerInfo();
	float fEffect = cocos2d::UserDefault::getInstance()->getFloatForKey("Effect",1);
	SoundFun::Instance().SetSoundEffect(fEffect);
	float fSound = cocos2d::UserDefault::getInstance()->getFloatForKey("Sound",1);
	SoundFun::Instance().SetSoundBackground(fSound);
	
	SoundFun::Instance().playBackMusic("raw/backMusic.mp3");
    
    std::string url = "http://120.24.178.243:8080/Api/Notice";
    MCWebReq::instance().sendRequestStr(url, CC_CALLBACK_1(HNHomeScence::UpdatePaomadeng, this), "");
    
    
    std::string chongUrl = "http://120.24.178.243:8080/Api/ChongNotice";
    MCWebReq::instance().sendRequestStr(chongUrl, CC_CALLBACK_1(HNHomeScence::UpdateChongKaNotice, this), "");
    
    
    
}
void HNHomeScence::upPlayerInfo()
{
	utility::log("SCHomeScence::upPlayerInfo()");
	WidgetFun::setText(this,"HomeID",utility::paseInt(UserInfo::Instance().getUserID(),6));
	WidgetFun::setText(this,"HomeName",UserInfo::Instance().getUserNicName());
	WidgetFun::setText(this,"HomeGold",UserInfo::Instance().getUserScore());
	WidgetFun::setText(this,"HomeFangKa",UserInfo::Instance().getUserInsure());
	ImagicDownManager::Instance().addDown(WidgetFun::getChildWidget(this,"HeadImagic"),
		UserInfo::Instance().getHeadHttp(),UserInfo::Instance().getUserID());
}
void HNHomeScence::LogonSucess()
{
	UserInfo::Instance().reqAccountInfo();
}
void HNHomeScence::upGameSever()
{

}

