#include "HNLogonScence.h"
#include "HNScenceManager.h"
#include "Game/GameLib.h"
#include "Voice/YvVoiceManager.hpp"
#include "../SHA1.h"
FV_SINGLETON_STORAGE (HNLogonScence);

HNLogonScence::HNLogonScence() :
		m_kLoginMission(ScriptData<std::string>("address").Value().c_str(),
				ScriptData<int>("Port").Value()) {
	init();
	MissionWeiXin::Instance().setMissionSink(this);
	m_kLoginMission.setMissionSink(this);
}
HNLogonScence::~HNLogonScence() {

}
bool HNLogonScence::init() {
	if (!cocos2d::CCNode::init()) {
		return false;
	};
	WidgetScenceXMLparse kScence1("Script/HNLogonScence.xml", this);
	WidgetManager::addButtonCB("Button_WeiXinLogon", this,
			button_selector(HNLogonScence::Button_WeiXinLogon));
	WidgetManager::addButtonCB("Button_UserXieYiCheak", this,
			button_selector(HNLogonScence::Button_UserXieYiCheak));
    
    WidgetManager::addButtonCB("Button_GuestLogon", this,
                               button_selector(HNLogonScence::Button_GuestLogon));
    
    
	WidgetFun::setChecked(this, "Button_UserXieYiCheak", true);
	return true;
}
void HNLogonScence::EnterScence() {
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
	return;
#endif

	std::string kAccounts =
			cocos2d::UserDefault::getInstance()->getStringForKey("Accounts");
	std::string kPassword =
			cocos2d::UserDefault::getInstance()->getStringForKey("Password");
	if (kAccounts == "111111") {
		kAccounts = "222222";
	} else if (kAccounts == "222222") {
		kAccounts = "333333";
	} else if (kAccounts == "333333") {
		kAccounts = "444444";
	} else if (kAccounts == "444444") {
		kAccounts = "111111";
	} else {
		kAccounts = "111111";
	}
	kPassword = "111111";
    //kAccounts = "444444";

    YvVoiceManager::GetInstance()->CpLogin(kAccounts, "TestAccounts");

    if (kAccounts != "" && kPassword != "") {
		m_kPssword = kPassword;
		CMD_GP_LogonAccounts loginAccount;
		loginAccount.dwPlazaVersion = DF::shared()->GetPlazaVersion();
		loginAccount.cbValidateFlags = MB_VALIDATE_FLAGS
				| LOW_VER_VALIDATE_FLAGS;
		strcpy(loginAccount.szAccounts, kAccounts.c_str());
		strcpy(loginAccount.szPassword, kPassword.c_str());
		m_kLoginMission.loginAccount(loginAccount);
	} else {
		m_kPssword = kPassword;
		CMD_GP_RegisterAccounts kRegister;
		zeromemory(&kRegister, sizeof(kRegister));
		kRegister.dwPlazaVersion = DF::shared()->GetPlazaVersion();
		kRegister.cbValidateFlags = MB_VALIDATE_FLAGS | LOW_VER_VALIDATE_FLAGS;
		kRegister.cbGender = 0;
		kRegister.wFaceID = 0;
		strncpy(kRegister.szAccounts, kAccounts.c_str(), kAccounts.size());
		strncpy(kRegister.szLogonPass, m_kPssword.c_str(), m_kPssword.size());
		std::string kNickName = (m_kWeiXinUserInfo.nickname);
		strncpy(kRegister.szNickName, kNickName.c_str(), kNickName.size());
		m_kLoginMission.registerServer(kRegister);
	}
}
void HNLogonScence::RegisterAccount() {
	CCAssert(m_kWeiXinUserInfo.openid != "","");
	if (m_kWeiXinUserInfo.openid == "") {
		return;
	}
	std::string kAccounts = "WeiXin" + m_kWeiXinUserInfo.openid;
	m_kPssword = "WeiXinPassword";
	CMD_GP_RegisterAccounts kRegister;
	zeromemory(&kRegister, sizeof(kRegister));
	kRegister.dwPlazaVersion = DF::shared()->GetPlazaVersion();
	kRegister.cbValidateFlags = MB_VALIDATE_FLAGS | LOW_VER_VALIDATE_FLAGS;
	kRegister.cbGender = 0;
	kRegister.wFaceID = 0;
	strncpy(kRegister.szAccounts, kAccounts.c_str(), kAccounts.size());
	strncpy(kRegister.szLogonPass, m_kPssword.c_str(), m_kPssword.size());
	std::string kNickName = (m_kWeiXinUserInfo.nickname);
	kNickName = utility::subUTF8(kNickName, 0, 6);
	strncpy(kRegister.szNickName, kNickName.c_str(), kNickName.size());
	m_kLoginMission.registerServer(kRegister);
}

void HNLogonScence::reqRongToken()
{
    std::string kUrl = "http://api.cn.ronghub.com/user/getToken.json";

    std::string portrait = UserInfo::Instance().getHeadHttp();
    if (portrait == "")
    {
        portrait = "http://a2.qpic.cn/psb?/304204f2-d55b-462e-ad7f-b87d157e0dbf/njVpEjMPQGkoHkY0NljqHZ7cFumx5cHD1HVdWF4C8XA!/b/dEC5ReeHJgAA&bo=cgSAAgAF0AIFCAg!&rf=viewer_4";
    }
    std::string dataStr = cocos2d::StringUtils::format("userId=%d&name=%s&portraitUri=%s", UserInfo::Instance().getUserID(), UserInfo::Instance().getUserNicName().c_str(), portrait.c_str());

    std::vector<std::string>headers;
    std::string appSecret = "2STFg9oFKUDO";
    headers.push_back("App-Key: p5tvi9dspmty4");
    std::string randStr = cocos2d::StringUtils::toString(rand());
    headers.push_back("Nonce: " + randStr);
    std::string timesStr = cocos2d::StringUtils::toString(time(0));
    headers.push_back("Timestamp: " + timesStr);
    std::string signature = appSecret + randStr + timesStr;
    unsigned chSha1[5];
    SHA1 sha1;
    const char* str1 = signature.c_str();
    sha1.Reset();
    sha1 << str1;

    sha1.Result(chSha1);

    for (int i = 0; i < 5; i++)
    {
        CCLOG("%X ", chSha1[i]);
    }
    std::string signatureSHA1 = cocos2d::StringUtils::format("%X%X%X%X%X", chSha1[0], chSha1[1], chSha1[2], chSha1[3], chSha1[4]);
    headers.push_back("Signature: " + signatureSHA1);

    headers.push_back("Content-Type: application/x-www-form-urlencoded");

    MCWebReq::instance().sendRequestDocumentUrlWithHeaders(kUrl, CC_CALLBACK_1(HNLogonScence::rspRongToken, this), nullptr, headers, dataStr);
}

void HNLogonScence::rspRongToken(rapidjson::Document * pDoc)
{
    int code = MCWebReq::getDataValueInt(pDoc, "code");

    if (code != 200)
    {
        return;
    }
    std::string token = MCWebReq::getDataValueStr(pDoc, "token");
    std::string userId = MCWebReq::getDataValueStr(pDoc, "userId");
    CCLOG("HNLogonScence::rspRongToken");

}

void HNLogonScence::onGPLoginSuccess() {
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	cocos2d::UserDefault::getInstance()->setStringForKey("Accounts",
			pGlobalUserData->szAccounts);
	cocos2d::UserDefault::getInstance()->setStringForKey("Password",
			m_kPssword);

	HNScenceManager::Instance().InHomeScence();

	UserInfo::Instance().modeHeadHttp(UserInfo::Instance().getHeadHttp());
    UserInfo::Instance().modeGender(m_kWeiXinUserInfo.sex);

    //TODO
    //this->reqRongToken();
}
void HNLogonScence::onGPLoginComplete() {
	HNScenceManager::Instance().InHomeScence();
    this->reqRongToken();

}
void HNLogonScence::onGPLoginFailure(unsigned int iErrorCode,
		const char* szDescription) {
	if (iErrorCode == 3 || iErrorCode == 1) {
		RegisterAccount();
	} else {
		NoticeMsg::Instance().ShowTopMsg((szDescription));
	}
}
void HNLogonScence::onGPError(int err) {
	NoticeMsg::Instance().ShowTopMsg(utility::getScriptString("NetError"));
}
void HNLogonScence::onWxLoginSuccess(WxUserInfo kWxUserInfo) {
	m_kWeiXinUserInfo = kWxUserInfo;

	std::string kAccounts = "WeiXin" + m_kWeiXinUserInfo.openid;
	m_kPssword = "WeiXinPassword";
	CMD_GP_LogonAccounts loginAccount;
	loginAccount.dwPlazaVersion = DF::shared()->GetPlazaVersion();
	loginAccount.cbValidateFlags = MB_VALIDATE_FLAGS | LOW_VER_VALIDATE_FLAGS;
	strcpy(loginAccount.szAccounts, kAccounts.c_str());
	strcpy(loginAccount.szPassword, m_kPssword.c_str());
	m_kLoginMission.loginAccount(loginAccount);
    
    YvVoiceManager::GetInstance()->CpLogin(kWxUserInfo.nickname, kWxUserInfo.openid);

	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
    pGlobalUserData->cbGender = kWxUserInfo.sex;
	strncpy(pGlobalUserData->szHeadHttp, kWxUserInfo.headimgurl.c_str(),
			countarray(pGlobalUserData->szHeadHttp));
}
void HNLogonScence::onWxLoginFail(std::string kError) {

}
void HNLogonScence::Button_WeiXinLogon(cocos2d::Ref*, WidgetUserInfo*) {
	if (!WidgetFun::isChecked(this, "Button_UserXieYiCheak")) {
		NoticeMsg::Instance().ShowTopMsgByScript("AcceptXieYi");
		return;
	}
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32

	int start = 100;
	int end = 10000;
	int random = CCRANDOM_0_1() * (end - start + 1) * start;

	char p[10];
	sprintf(p, "%d", random);
	std::string strrandom(p);

	std::string kNickName = "grwoing_kkkk" + strrandom;

	//std::string kNickName = utility::toString(time(NULL));
	m_kWeiXinUserInfo.openid = kNickName;
	m_kWeiXinUserInfo.nickname = kNickName;
	m_kWeiXinUserInfo.headimgurl = "http:\\assssssssssssssssssssssss";

	onWxLoginSuccess(m_kWeiXinUserInfo);
#else
	cocos2d::log("调用微信！");
	MissionWeiXin::Instance().logonWeiXin();
#endif
}

void HNLogonScence::Button_GuestLogon(cocos2d::Ref*, WidgetUserInfo*) {
    if (!WidgetFun::isChecked(this, "Button_UserXieYiCheak")) {
        NoticeMsg::Instance().ShowTopMsgByScript("AcceptXieYi");
        return;
    }
    
    int start = 100;
    int end = 10000;
    int random = CCRANDOM_0_1() * (end - start + 1) * start;
    
    char p[10];
    sprintf(p, "%d", random);
    std::string strrandom(p);
    
    std::string kNickName = "grwoing_kkkk" + strrandom;
    
    //std::string kNickName = utility::toString(time(NULL));
    m_kWeiXinUserInfo.openid = kNickName;
    m_kWeiXinUserInfo.nickname = kNickName;
    m_kWeiXinUserInfo.headimgurl = "http:\\assssssssssssssssssssssss";
    
    onWxLoginSuccess(m_kWeiXinUserInfo);
}

void HNLogonScence::Button_UserXieYiCheak(cocos2d::Ref*, WidgetUserInfo*) {

}
