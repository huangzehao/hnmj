#pragma once
#include "cocos2d.h"
namespace HNMJ {
	class MJTools
	{
	public:
		static void setCardImagic(cocos2d::Node* pNode, int kValue, std::string kImagicFront);

		static void setMagicCardImagic(cocos2d::Node* cardSprite, int kValue);
	};

}