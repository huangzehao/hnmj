#include "HNMJGameScence.h"
#include "Game/GameLib.h"

#include "CMD_HNMJ.h"
#include "HNMJPlayer.h"
#include "HNMJGameLogic.h"
#include "HNMJSoundFun.h"
#include "GameLib/JniCross/JniFun.h"
#include "Voice/YvVoiceManager.hpp"


void HNMJGameScence::initButton()
{
	WidgetManager::addButtonCB("HNMJButton_Ready",this,button_selector(HNMJGameScence::HNMJButton_Ready));
    WidgetManager::addButtonCB("HNMJButton_UnReady", this, button_selector(HNMJGameScence::HNMJButton_UnReady));
	WidgetManager::addButtonCB("HNMJButton_GuoAction",this,button_selector(HNMJGameScence::HNMJButton_GuoAction));
	WidgetManager::addButtonCB("HNMJButton_GangAction",this,button_selector(HNMJGameScence::HNMJButton_GangAction));
    WidgetManager::addButtonCB("HNMJButton_LiangAction", this, button_selector(HNMJGameScence::HNMJButton_LiangAction));
	WidgetManager::addButtonCB("HNMJButton_GangAction_Card",this,button_selector(HNMJGameScence::HNMJButton_GangAction));
	WidgetManager::addButtonCB("HNMJButton_PengAction",this,button_selector(HNMJGameScence::HNMJButton_PengAction));
	WidgetManager::addButtonCB("HNMJButton_PengAction_Card",this,button_selector(HNMJGameScence::HNMJButton_PengAction));
	WidgetManager::addButtonCB("HNMJButton_ChiAction",this,button_selector(HNMJGameScence::HNMJButton_ChiAction));
	WidgetManager::addButtonCB("HNMJButton_ChiAction_Card",this,button_selector(HNMJGameScence::HNMJButton_ChiAction));
	WidgetManager::addButtonCB("HNMJButton_HuAction",this,button_selector(HNMJGameScence::HNMJButton_HuAction));
	WidgetManager::addButtonCB("HNMJButton_HuAction_Card",this,button_selector(HNMJGameScence::HNMJButton_HuAction));
	WidgetManager::addButtonCB("HNMJButton_XiaoHuAction",this,button_selector(HNMJGameScence::HNMJButton_XiaoHuAction));
	WidgetManager::addButtonCB("HNMJButton_GangCS",this,button_selector(HNMJGameScence::HNMJButton_GangCS));
	WidgetManager::addButtonCB("HNMJButton_GangCS_Card",this,button_selector(HNMJGameScence::HNMJButton_GangCS));
	WidgetManager::addButtonCB("HNMJButton_BuAction",this,button_selector(HNMJGameScence::HNMJButton_BuAction));
	WidgetManager::addButtonCB("HNMJButton_GangCard",this,button_selector(HNMJGameScence::HNMJButton_GangCardList));
	WidgetManager::addButtonCB("HNMJButton_ChiCard0",this,button_selector(HNMJGameScence::HNMJButton_ChiCardList));
	WidgetManager::addButtonCB("HNMJButton_ChiCard1",this,button_selector(HNMJGameScence::HNMJButton_ChiCardList));
	WidgetManager::addButtonCB("HNMJButton_ChiCard2",this,button_selector(HNMJGameScence::HNMJButton_ChiCardList));
	WidgetManager::addButtonCB("HNMJButton_QuXiaoTuoGuan",this,button_selector(HNMJGameScence::HNMJButton_QuXiaoTuoGuan));

	WidgetManager::addButtonCB("HNMJButtonAction_ShowCard",this,button_selector(HNMJGameScence::HNMJButtonAction_ShowCard));
	WidgetManager::addButtonCB("HNMJButtonAction_ShowNext",this,button_selector(HNMJGameScence::HNMJButtonAction_ShowNext));
	
	WidgetManager::addButtonCB("HNMJButton_JieSuanShare",this,button_selector(HNMJGameScence::HNMJButton_JieSuanShare));
	WidgetManager::addButtonCB("HNMJButton_JieSuanStart",this,button_selector(HNMJGameScence::HNMJButton_JieSuanStart));
	WidgetManager::addButtonCB("HNMJButton_JieSuanClose",this,button_selector(HNMJGameScence::HNMJButton_JieSuanClose));

	//WidgetManager::addButtonCBBegin("HNMJButton_Talk",this,button_selector(HNMJGameScence::HNMJButton_TalkBegin));
	//WidgetManager::addButtonCB("HNMJButton_Talk",this,button_selector(HNMJGameScence::HNMJButton_TalkEnd));
    cocos2d::ui::Button* talkBtn = dynamic_cast<cocos2d::ui::Button*>(WidgetFun::getChildWidget(this, "HNMJButton_Talk"));
    talkBtn->addTouchEventListener([=](cocos2d::Ref*, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::BEGAN)
        {
            this->HNMJButton_TalkBegin(nullptr, nullptr);
        }
        else if(type == cocos2d::ui::Widget::TouchEventType::ENDED || type == cocos2d::ui::Widget::TouchEventType::CANCELED)
        {
            this->HNMJButton_TalkEnd(nullptr, nullptr);
        }
    });

	WidgetManager::addButtonCB("HNMJButton_Msg",this,button_selector(HNMJGameScence::HNMJButton_Msg));

	WidgetManager::addButtonCB("Button_Master",this,button_selector(HNMJGameScence::HNMJButton_Master));
	WidgetManager::addButtonCB("Button_MasterClose",this,button_selector(HNMJGameScence::Button_MasterClose));
	WidgetManager::addButtonCB("NHMJ_MASTER_LEFTCARD",this,button_selector(HNMJGameScence::NHMJ_MASTER_LEFTCARD));

	WidgetManager::addButtonCB("MasterZhaNiao1",this,button_selector(HNMJGameScence::MasterZhaNiao));
	WidgetManager::addButtonCB("MasterZhaNiao2",this,button_selector(HNMJGameScence::MasterZhaNiao));
	WidgetManager::addButtonCB("MasterZhaNiao3",this,button_selector(HNMJGameScence::MasterZhaNiao));
	WidgetManager::addButtonCB("MasterZhaNiao4",this,button_selector(HNMJGameScence::MasterZhaNiao));
	WidgetManager::addButtonCB("MasterZhaNiao5",this,button_selector(HNMJGameScence::MasterZhaNiao));
	WidgetManager::addButtonCB("MasterZhaNiao6",this,button_selector(HNMJGameScence::MasterZhaNiao));

	WidgetManager::addButtonCB("Button_TalkDefine",this,button_selector(HNMJGameScence::Button_TalkDefine));
	WidgetManager::addButtonCB("Button_Send_TalkStr",this,button_selector(HNMJGameScence::Button_Send_TalkStr));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing0",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing1",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing2",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing3",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing4",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing5",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing6",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
	WidgetManager::addButtonCB("HNMJButton_BiaoQing7",this,button_selector(HNMJGameScence::HNMJButton_BiaoQing));
}
void HNMJGameScence::HNMJButton_Ready(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
    if (this->canDoReady())
    {
        sendReady();
        defaultState();
    }
    else
    {
        NoticeMsg::Instance().ShowTopMsgByScript("TooClose");
    }
}

bool HNMJGameScence::canDoReady()
{
#if WIN32
    return true;
#endif // WIN32
    if (m_kRoomInfo.isBanDisReady == false)
    {
        return true;
    }

    for (int i = 0; i<6; i++)
    {
        if (allPlayerDistance_.at(i) >= 0.0000000f && allPlayerDistance_.at(i) < 100.0000000f)
        {
            return false;
        }
    }
    return true;
}

void HNMJGameScence::HNMJButton_UnReady(cocos2d::Ref *, WidgetUserInfo *)
{
    sendUnReady();
    defaultState();
}


void HNMJGameScence::sendOperateCMD(int nCode,int nCard)
{
	WidgetFun::setVisible(this,"SelfActionNode",false);

	CMD_C_OperateCard OperateCard;
	OperateCard.cbOperateCode=nCode;
	OperateCard.cbOperateCard=nCard;
	SendSocketData(SUB_C_OPERATE_CARD,&OperateCard,sizeof(OperateCard));

	remmoveAutoAction();
}

void HNMJGameScence::HNMJButton_GuoAction(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	if (pUserInfo == NULL)
	{
		sendOperateCMD(WIK_NULL,0);
		return;
	}
	std::string kXiaoHu = WidgetFun::getUserInfoValue(pUserInfo,"IsXiaoHu");
	if (kXiaoHu == "XiaoHu")
	{
		WidgetFun::setVisible(this,"SelfActionNode",false);
		CMD_C_XiaoHu OperateCard;
		OperateCard.cbOperateCode=WIK_NULL;
		OperateCard.cbOperateCard=0;
		SendSocketData(SUB_C_XIAOHU,&OperateCard,sizeof(OperateCard));
	}
	else
	{
		sendOperateCMD(WIK_NULL,0);
	}
}
void HNMJGameScence::HNMJButton_GangAction(cocos2d::Ref* pNode,WidgetUserInfo* pUserInfo)
{
	BYTE nGangCardCount = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Gang_CardCount"));
	if (nGangCardCount ==1)
	{	
		int nCard = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Gang_Card1"));
		//ASSERT(m_gang_type == WIK_GANG || m_gang_type == WIK_PIZI_GANG || m_gang_type == WIK_PIZI_ZFB_GANG);
		//sendOperateCMD(WIK_GANG, nCard);
		sendOperateCMD(WIK_GANG, nCard);
		//sendOperateCMD(WIK_GANG | WIK_PIZI_GANG | WIK_PIZI_ZFB_GANG,nCard);
	}
	else if (nGangCardCount >=2)
	{
		cocos2d::Node* pCheckNode = WidgetFun::getChildWidget(this,"GangCardBg");
		pCheckNode->setVisible(true);
		showGangCheckNode(pCheckNode,pUserInfo);
	}
}
void HNMJGameScence::HNMJButton_LiangAction(cocos2d::Ref* pNode, WidgetUserInfo* pUserInfo)
{
    sendOperateCMD(WIK_PIZI_ZFB_GANG, 0x36);
}
void HNMJGameScence::HNMJButton_PengAction(cocos2d::Ref*,WidgetUserInfo*)
{
	sendOperateCMD(WIK_PENG,0);
}
void HNMJGameScence::HNMJButton_ChiAction(cocos2d::Ref* pNode,WidgetUserInfo* pUserInfo)
{
	int nChiAction  = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Chi_Type"));
	BYTE nChiCard  = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Chi_Card"));

	int nChiCount =0;
	int nSendChiAction = 0;
	if (nChiAction & WIK_LEFT)
	{
		nChiCount++;
		nSendChiAction = WIK_LEFT;
	}
	if (nChiAction & WIK_CENTER)
	{
		nChiCount++;
		nSendChiAction = WIK_CENTER;
	}
	if (nChiAction & WIK_RIGHT)
	{
		nChiCount++;
		nSendChiAction = WIK_RIGHT;
	}
	
	if (nChiCount ==1)
	{	
		sendOperateCMD(nSendChiAction,0);
	}
	else if (nChiCount >=2)
	{
		cocos2d::Node* pCheckNode = WidgetFun::getChildWidget(this,"ChiCardBg");
		pCheckNode->setVisible(true);
		showChiCheckNode(pCheckNode,nChiAction,nChiCard,nChiCount);
		
	}
}

void HNMJGameScence::showChiCheckNode(cocos2d::Node* pCheckNode, BYTE cbAction,BYTE cbCard,int nChiCount)
{
	ASSERT(pCheckNode&&nChiCount>=2&&nChiCount<=3);
	std::string kImagicFront = WidgetFun::getWidgetUserInfo(pCheckNode,"ImagicFront");
	if (nChiCount==2)
	{
		cocos2d::Size kTowSize = utility::parseSize(WidgetFun::getWidgetUserInfo(pCheckNode,"TowSize"));
		WidgetFun::set9Size(pCheckNode,kTowSize);
		WidgetFun::setVisible(pCheckNode,"Chi_Card_Temp3",false);

		cocos2d::Node* pChi_Card_Temp1 = WidgetFun::getChildWidget(pCheckNode,"Chi_Card_Temp1");
		cocos2d::Node* pChi_Card_Temp2 = WidgetFun::getChildWidget(pCheckNode,"Chi_Card_Temp2");
		if (cbAction&WIK_LEFT && cbAction&WIK_CENTER)
		{
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp1,"ChiIndex",utility::toString((int)WIK_LEFT));
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp2,"ChiIndex",utility::toString((int)WIK_CENTER));

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard0"),cbCard,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard1"),cbCard + 1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard2"),cbCard+2,kImagicFront);

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard0"),cbCard-1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard1"),cbCard,  kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard2"),cbCard+1,kImagicFront);
		}
		if (cbAction&WIK_CENTER && cbAction&WIK_RIGHT)
		{
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp1,"ChiIndex",utility::toString((int)WIK_CENTER));
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp2,"ChiIndex",utility::toString((int)WIK_RIGHT));

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard0"),cbCard-1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard1"),cbCard,  kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard2"),cbCard+1,kImagicFront);

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard0"),cbCard-2,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard1"),cbCard - 1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard2"),cbCard,kImagicFront);

		}
		if (cbAction&WIK_LEFT && cbAction&WIK_RIGHT)
		{
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp1,"ChiIndex",utility::toString((int)WIK_LEFT));
			WidgetFun::setWidgetUserInfo(pChi_Card_Temp2,"ChiIndex",utility::toString((int)WIK_RIGHT));

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard0"),cbCard,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard1"),cbCard + 1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard2"),cbCard+2,kImagicFront);

			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard0"),cbCard-2,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard1"),cbCard - 1,kImagicFront);
			setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard2"),cbCard,kImagicFront);
		}
	}
	else
	{
		cocos2d::Size kThreeSize = utility::parseSize(WidgetFun::getWidgetUserInfo(pCheckNode,"ThreeSize"));
		WidgetFun::set9Size(pCheckNode,kThreeSize);
		WidgetFun::setVisible(pCheckNode,"Chi_Card_Temp3",true);

		cocos2d::Node* pChi_Card_Temp1 = WidgetFun::getChildWidget(pCheckNode,"Chi_Card_Temp1");
		WidgetFun::setWidgetUserInfo(pChi_Card_Temp1,"ChiIndex",utility::toString((int)WIK_RIGHT));

		cocos2d::Node* pChi_Card_Temp2 = WidgetFun::getChildWidget(pCheckNode,"Chi_Card_Temp2");
		WidgetFun::setWidgetUserInfo(pChi_Card_Temp2,"ChiIndex",utility::toString((int)WIK_CENTER));

		cocos2d::Node* pChi_Card_Temp3 = WidgetFun::getChildWidget(pCheckNode,"Chi_Card_Temp3");
		WidgetFun::setWidgetUserInfo(pChi_Card_Temp3,"ChiIndex",utility::toString((int)WIK_LEFT));

		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard0"),cbCard - 2,kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard1"),cbCard - 1,kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp1,"HNMJButton_ChiCard2"),cbCard,kImagicFront);

		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard0"),cbCard-1,kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard1"),cbCard,  kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp2,"HNMJButton_ChiCard2"),cbCard+1,kImagicFront);

		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp3,"HNMJButton_ChiCard0"),cbCard,kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp3,"HNMJButton_ChiCard1"),cbCard + 1,kImagicFront);
		setActionBtnCard(WidgetFun::getChildWidget(pChi_Card_Temp3,"HNMJButton_ChiCard2"),cbCard+2,kImagicFront);
	}
}

void HNMJGameScence::showGangCheckNode(cocos2d::Node* pCheckNode,WidgetUserInfo* pUserInfo)
{
	BYTE nGangCardCount = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Gang_CardCount"));

	std::string kImagicFront = WidgetFun::getWidgetUserInfo(pCheckNode,"ImagicFront");
    WidgetFun::setVisible(this, "GangCardBg", true);
    for (int i = 1; i <= 6; i++)
    {
        cocos2d::Node* cardNode = WidgetFun::getChildWidget(pCheckNode, cocos2d::StringUtils::format("Gang_Card_Temp%d", i));
        if (cardNode == nullptr)
        {
            continue;
        }

        if (i <= nGangCardCount)
        {
            cardNode->setVisible(true);
            std::string infoKey = cocos2d::StringUtils::format("Gang_Card%d", i);
            setActionBtnCard(WidgetFun::getChildWidget(cardNode, "HNMJButton_GangCard"), utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo, infoKey)), kImagicFront);
        }
        else
        {
            cardNode->setVisible(false);
        }
    }
}

void HNMJGameScence::HNMJButton_ChiCardList(cocos2d::Ref* pNode,WidgetUserInfo*)
{
	cocos2d::Node* pParent = ((cocos2d::Node*)pNode)->getParent();
	int nChiIndex = utility::parseInt(WidgetFun::getWidgetUserInfo(pParent,"ChiIndex"));
	sendOperateCMD(nChiIndex,0);
}

void HNMJGameScence::HNMJButton_GangCardList(cocos2d::Ref* pNode,WidgetUserInfo*)
{
	cocos2d::Node* pParent = ((cocos2d::Node*)pNode)->getParent();
	int nGangIndex = utility::parseInt(WidgetFun::getWidgetUserInfo(pParent,"GangIndex"));

	//int nCard = m_pLocal->getGangCard(utility::parseInt(WidgetFun::getWidgetUserInfo(this,"GangIndex"+nGangIndex)));
	cocos2d::log("点击了 %x ", nGangIndex);
	//sendOperateCMD(WIK_GANG,nGangIndex);
	//因为有痞子杠, 所以不能直接发WIK_GANG
	//ASSERT(m_gang_type == WIK_GANG || m_gang_type == WIK_PIZI_GANG || m_gang_type == WIK_PIZI_ZFB_GANG);
	sendOperateCMD(WIK_GANG, nGangIndex);
}
void HNMJGameScence::HNMJButton_QuXiaoTuoGuan(cocos2d::Ref*,WidgetUserInfo*)
{
	CMD_C_Trustee Trustee;
	Trustee.bTrustee=false;
	SendSocketData(SUB_C_TRUSTEE,&Trustee,sizeof(Trustee));
}

void HNMJGameScence::HNMJButton_HuAction(cocos2d::Ref*,WidgetUserInfo*)
{
	sendOperateCMD(WIK_CHI_HU,0);
}

void HNMJGameScence::HNMJButton_XiaoHuAction(cocos2d::Ref*,WidgetUserInfo*)
{
	WidgetFun::setVisible(this,"SelfActionNode",false);
	CMD_C_XiaoHu OperateCard;
	OperateCard.cbOperateCode=WIK_XIAO_HU;
	OperateCard.cbOperateCard=0;
	SendSocketData(SUB_C_XIAOHU,&OperateCard,sizeof(OperateCard));
}

void HNMJGameScence::HNMJButton_GangCS(cocos2d::Ref*,WidgetUserInfo*)
{
	int nCard = m_pLocal->getGangCard(utility::parseInt(WidgetFun::getWidgetUserInfo(this,"NotifyCard")));
	sendOperateCMD(WIK_CS_GANG,nCard);
}
void HNMJGameScence::HNMJButton_BuAction(cocos2d::Ref*,WidgetUserInfo*)
{
	int nCard = m_pLocal->getGangCard(utility::parseInt(WidgetFun::getWidgetUserInfo(this,"NotifyCard")));
	sendOperateCMD(WIK_BU_ZHANG,nCard);
}
void HNMJGameScence::HNMJButtonAction_ShowNext(cocos2d::Ref*,WidgetUserInfo*)
{
	showSaiZi(utility::parseUInt(WidgetFun::getWidgetUserInfo(this,"SaiZiNode","NextValue")));
}
void HNMJGameScence::HNMJButtonAction_ShowCard(cocos2d::Ref*,WidgetUserInfo*)
{
	for (int i = 0;i<MAX_PLAYER;i++)
	{
		m_pPlayer[i]->showHandCard();
	}
	setCurrentPlayer(m_iCurrentUser,WIK_NULL);
	WidgetFun::setVisible(this,"TimeNode",true);
	WidgetFun::setVisible(this,"LastCardNode",true);
}

void HNMJGameScence::HNMJButton_JieSuanShare(cocos2d::Ref*,WidgetUserInfo*)
{
	Button_WeiXinImagic(NULL,NULL);
}
void HNMJGameScence::HNMJButton_JieSuanStart(cocos2d::Ref*,WidgetUserInfo*)
{
	HNMJButton_Ready(NULL,NULL);
	WidgetFun::setVisible(this,"FreeStateNode",m_pLocal->GetUserStatus() < US_READY);
}

void HNMJGameScence::HNMJButton_JieSuanClose(cocos2d::Ref*,WidgetUserInfo*)
{
	WidgetFun::setVisible(this,"GameJieSuanNode",false);
	WidgetFun::setVisible(this,"FreeStateNode",m_pLocal->GetUserStatus() < US_READY);

}

void HNMJGameScence::HNMJButton_TalkBegin(cocos2d::Ref*,WidgetUserInfo*)
{
	//if (WidgetFun::isWidgetVisble(this,"TalkImagic"))
	//{
	//	JniFun::stopSoundRecord();
	//	WidgetFun::setVisible(this,"TalkImagic",false);
	//	return;
	//}
	SoundFun::Instance().PaseBackMusic();
	WidgetFun::setVisible(this,"TalkImagic",true);
	//JniFun::startSoundRecord();
    YvVoiceManager::GetInstance()->StartRecord();
    
    
	//int iTimeID = TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::endButtonTalk,this),20.0f)->iIdex;
	//WidgetFun::setWidgetUserInfo(this,"TalkImagic","TimeID",utility::toString(iTimeID));
}
void HNMJGameScence::endButtonTalk()
{
	HNMJButton_TalkEnd(NULL,NULL);
}



void HNMJGameScence::HNMJButton_TalkEnd(cocos2d::Ref*,WidgetUserInfo*)
{
	SoundFun::Instance().ResumeBackMusic();
	if (!WidgetFun::isWidgetVisble(this,"TalkImagic"))
	{
		return;
	}
	//int iTimeID = utility::parseInt(WidgetFun::getWidgetUserInfo(this,"TalkImagic","TimeID"));
	//TimeManager::Instance().removeByID(iTimeID);
    
	WidgetFun::setVisible(this,"TalkImagic",false);
    //YvVoiceManager::GetInstance()->setUploadListern(this);
    YvVoiceManager::GetInstance()->setUploadSuccessCallBack([=](const char* fileID) {
        std::string url = fileID;
        sendTalkFile(m_pLocal->GetChairID(), url);
    });
    YvVoiceManager::GetInstance()->StopRecord();
    
	//std::string kFileName = JniFun::stopSoundRecord();
	//sendTalkFile(m_pLocal->GetChairID(),kFileName);
}

//void HNMJGameScence::onUpLoadFileListern(YVSDK::UpLoadFileRespond* resp){
//    std::string url = resp->fileurl;
//    sendTalkFile(m_pLocal->GetChairID(),url);
//}

void HNMJGameScence::HNMJButton_Msg(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	WidgetFun::setVisible(this,"GameTalkPlane",true);
}

void HNMJGameScence::Button_TalkDefine(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	int iUserIdex = utility::parseInt(WidgetFun::getUserInfoValue(pUserInfo,"Idex"))+1;
	std::string kTaskStr = WidgetFun::getUserInfoValue(pUserInfo,"Txt");
	std::string kTxt = HNMJSoundFun::getDefineSound(m_pLocal->GetGender(),utility::toString(iUserIdex));
	sendTalkDefine(m_pLocal->GetChairID(),utility::toString(kTxt,":",kTaskStr));
   
    WidgetFun::setVisible(this,"GameTalkPlane",false);
}
void HNMJGameScence::Button_Send_TalkStr(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	std::string kTxt = WidgetFun::getEditeText(this,"GameTalkEdit");
	sendTalkString(m_pLocal->GetChairID(),kTxt);
	WidgetFun::setVisible(this,"GameTalkPlane",false);
	WidgetFun::setEditeText(this,"GameTalkEdit","");
}
void HNMJGameScence::HNMJButton_BiaoQing(cocos2d::Ref*,WidgetUserInfo* pUserInfo)
{
	std::string kFile = WidgetFun::getUserInfoValue(pUserInfo,"File");
	sendTalkBiaoQing(m_pLocal->GetChairID(),kFile);
	WidgetFun::setVisible(this,"GameTalkPlane",false);
}