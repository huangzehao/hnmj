#include "HNMJGameLogic.h"

//////////////////////////////////////////////////////////////////////////

FV_SINGLETON_STORAGE(HNMJ::CGameLogic);
namespace HNMJ
{
	//æ≤Ã¨±‰¡ø
	bool		CChiHuRight::m_bInit = false;
	dword		CChiHuRight::m_dwRightMask[MAX_RIGHT_COUNT];

	//ππ‘Ï∫Ø ˝
	CChiHuRight::CChiHuRight()
	{
		zeromemory( m_dwRight,sizeof(m_dwRight) );

		if( !m_bInit )
		{
			m_bInit = true;
			for( BYTE i = 0; i < CountArray(m_dwRightMask); i++ )
			{
				if( 0 == i )
					m_dwRightMask[i] = 0;
				else
					m_dwRightMask[i] = (dword(pow((float)2,(float)(i-1))))<<28;
			}
		}
	}

	//∏≥÷µ∑˚÷ÿ‘ÿ
	CChiHuRight & CChiHuRight::operator = ( dword dwRight )
	{
		dword dwOtherRight = 0;
		//—È÷§»®Œª
		if( !IsValidRight( dwRight ) )
		{
			//—È÷§»°∑¥»®Œª
			ASSERT( IsValidRight( ~dwRight ) );
			if( !IsValidRight( ~dwRight ) ) return *this;
			dwRight = ~dwRight;
			dwOtherRight = MASK_CHI_HU_RIGHT;
		}

		for( BYTE i = 0; i < CountArray(m_dwRightMask); i++ )
		{
			if( (dwRight&m_dwRightMask[i]) || (i==0&&dwRight<0x10000000) )
				m_dwRight[i] = dwRight&MASK_CHI_HU_RIGHT;
			else m_dwRight[i] = dwOtherRight;
		}

		return *this;
	}

	//”Îµ»”⁄
	CChiHuRight & CChiHuRight::operator &= ( dword dwRight )
	{
		bool bNavigate = false;
		//—È÷§»®Œª
		if( !IsValidRight( dwRight ) )
		{
			//—È÷§»°∑¥»®Œª
			ASSERT( IsValidRight( ~dwRight ) );
			if( !IsValidRight( ~dwRight ) ) return *this;
			//µ˜’˚»®Œª
			dword dwHeadRight = (~dwRight)&0xF0000000;
			dword dwTailRight = dwRight&MASK_CHI_HU_RIGHT;
			dwRight = dwHeadRight|dwTailRight;
			bNavigate = true;
		}

		for( BYTE i = 0; i < CountArray(m_dwRightMask); i++ )
		{
			if( (dwRight&m_dwRightMask[i]) || (i==0&&dwRight<0x10000000) )
			{
				m_dwRight[i] &= (dwRight&MASK_CHI_HU_RIGHT);
			}
			else if( !bNavigate )
				m_dwRight[i] = 0;
		}

		return *this;
	}

	//ªÚµ»”⁄
	CChiHuRight & CChiHuRight::operator |= ( dword dwRight )
	{
		//—È÷§»®Œª
		if( !IsValidRight( dwRight ) ) return *this;

		for( BYTE i = 0; i < CountArray(m_dwRightMask); i++ )
		{
			if( (dwRight&m_dwRightMask[i]) || (i==0&&dwRight<0x10000000) )
				m_dwRight[i] |= (dwRight&MASK_CHI_HU_RIGHT);
		}

		return *this;
	}

	//”Î
	CChiHuRight CChiHuRight::operator & ( dword dwRight )
	{
		CChiHuRight chr = *this;
		return (chr &= dwRight);
	}

	//”Î
	CChiHuRight CChiHuRight::operator & ( dword dwRight ) const
	{
		CChiHuRight chr = *this;
		return (chr &= dwRight);
	}

	//ªÚ
	CChiHuRight CChiHuRight::operator | ( dword dwRight )
	{
		CChiHuRight chr = *this;
		return chr |= dwRight;
	}

	//ªÚ
	CChiHuRight CChiHuRight::operator | ( dword dwRight ) const
	{
		CChiHuRight chr = *this;
		return chr |= dwRight;
	}

	// «∑Ò»®ŒªŒ™ø’
	bool CChiHuRight::IsEmpty()
	{
		for( BYTE i = 0; i < CountArray(m_dwRight); i++ )
			if( m_dwRight[i] ) return false;
		return true;
	}

	//…Ë÷√»®ŒªŒ™ø’
	void CChiHuRight::SetEmpty()
	{
		zeromemory( m_dwRight,sizeof(m_dwRight) );
		return;
	}

	//ªÒ»°»®Œª ˝÷µ
	BYTE CChiHuRight::GetRightData( dword dwRight[], BYTE cbMaxCount )
	{
		ASSERT( cbMaxCount >= CountArray(m_dwRight) );
		if( cbMaxCount < CountArray(m_dwRight) ) return 0;

		memcpy( dwRight,m_dwRight,sizeof(dword)*CountArray(m_dwRight) );
		return CountArray(m_dwRight);
	}

	//…Ë÷√»®Œª ˝÷µ
	bool CChiHuRight::SetRightData( const dword dwRight[], BYTE cbRightCount )
	{
		ASSERT( cbRightCount <= CountArray(m_dwRight) );
		if( cbRightCount > CountArray(m_dwRight) ) return false;

		zeromemory( m_dwRight,sizeof(m_dwRight) );
		memcpy( m_dwRight,dwRight,sizeof(dword)*cbRightCount );

		return true;
	}

	//ºÏ≤ÈΩˆŒª «∑Ò’˝»∑
	bool CChiHuRight::IsValidRight( dword dwRight )
	{
		dword dwRightHead = dwRight & 0xF0000000;
		for( BYTE i = 0; i < CountArray(m_dwRightMask); i++ )
			if( m_dwRightMask[i] == dwRightHead ) return true;
		return false;
	}

	//////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////
	//æ≤Ã¨±‰¡ø

	//∆ÀøÀ ˝æ›
	const BYTE CGameLogic::m_cbCardDataArray[MAX_REPERTORY]=
	{
		0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//ÕÚ◊”
		0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//ÕÚ◊”
		0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//ÕÚ◊”
		0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//ÕÚ◊”
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//À˜◊”
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//À˜◊”
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//À˜◊”
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//À˜◊”
		0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//Õ¨◊”
		0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//Õ¨◊”
		0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//Õ¨◊”
		0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//Õ¨◊”
	};

	//const BYTE CGameLogic::m_cbCardDataArray_HSHH[MAX_REPERTORY_HSHH] =
	//{
	//	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	//	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	//	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	//	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	//	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	//	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	//	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	//	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	//	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	//	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	//	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	//	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子

	//	0x35,0x35,0x35,0x35,												//红中
	//	0x36,0x36,0x36,0x36,												//发财
	//	0x37,0x37,0x37,0x37,												//白板

	//};
	//////////////////////////////////////////////////////////////////////////

	//ππ‘Ï∫Ø ˝
	CGameLogic::CGameLogic()
	{
		m_cbMagicIndex = MAX_INDEX;
		this->SetPiZiData(0x35, 0x36);
	}

	//Œˆππ∫Ø ˝
	CGameLogic::~CGameLogic()
	{
	}

	//ªÏ¬“∆ÀøÀ
	void CGameLogic::RandCardData(BYTE cbCardData[], BYTE cbMaxCount)
	{
		//ªÏ¬“◊º±∏
		BYTE cbCardDataTemp[CountArray(m_cbCardDataArray)];
		memcpy(cbCardDataTemp,m_cbCardDataArray,sizeof(m_cbCardDataArray));

		//ªÏ¬“∆ÀøÀ
		BYTE cbRandCount=0,cbPosition=0;
		do
		{
			cbPosition=rand()%(cbMaxCount-cbRandCount);
			cbCardData[cbRandCount++]=cbCardDataTemp[cbPosition];
			cbCardDataTemp[cbPosition]=cbCardDataTemp[cbMaxCount-cbRandCount];
		} while (cbRandCount<cbMaxCount);

		return;
	}

	//…æ≥˝∆ÀøÀ
	bool CGameLogic::RemoveCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbRemoveCard)
	{
		//–ß—È∆ÀøÀ
		ASSERT(IsValidCard(cbRemoveCard));
		ASSERT(cbCardIndex[SwitchToCardIndex(cbRemoveCard)]>0);

		//…æ≥˝∆ÀøÀ
		BYTE cbRemoveIndex=SwitchToCardIndex(cbRemoveCard);
		if (cbCardIndex[cbRemoveIndex]>0)
		{
			cbCardIndex[cbRemoveIndex]--;
			return true;
		}

		// ß∞‹–ß—È
		ASSERT(FALSE);

		return false;
	}

	//…æ≥˝∆ÀøÀ
	bool CGameLogic::RemoveCard(BYTE cbCardIndex[MAX_INDEX], const BYTE cbRemoveCard[], BYTE cbRemoveCount)
	{
		//…æ≥˝∆ÀøÀ
		for (BYTE i=0;i<cbRemoveCount;i++)
		{
			//–ß—È∆ÀøÀ
			ASSERT(IsValidCard(cbRemoveCard[i]));
			ASSERT(cbCardIndex[SwitchToCardIndex(cbRemoveCard[i])]>0);

			//…æ≥˝∆ÀøÀ
			BYTE cbRemoveIndex=SwitchToCardIndex(cbRemoveCard[i]);
			if (cbCardIndex[cbRemoveIndex]==0)
			{
				//¥ÌŒÛ∂œ—‘
				ASSERT(FALSE);

				//ªπ‘≠…æ≥˝
				for (BYTE j=0;j<i;j++) 
				{
					ASSERT(IsValidCard(cbRemoveCard[j]));
					cbCardIndex[SwitchToCardIndex(cbRemoveCard[j])]++;
				}

				return false;
			}
			else 
			{
				//…æ≥˝∆ÀøÀ
				--cbCardIndex[cbRemoveIndex];
			}
		}

		return true;
	}

	BYTE CGameLogic::RemoveValueCardAll(BYTE cbCardData[], BYTE cbCardCount,BYTE cbRemoveCard)
	{

		BYTE cbCardIndex[MAX_INDEX];			// ÷÷–∆ÀøÀ
		SwitchToCardIndex(cbCardData,cbCardCount,cbCardIndex);
		BYTE cbRemoveCardArray[MAX_INDEX];
		BYTE cbRemoveCout = cbCardIndex[SwitchToCardIndex(cbRemoveCard)];
		for (int i = 0;i<cbRemoveCout;i++)
		{
			cbRemoveCardArray[i] = cbRemoveCard;
		}
		RemoveValueCard(cbCardData,cbCardCount,cbRemoveCardArray,cbRemoveCout);
		return cbCardCount - cbRemoveCout;
	}
	bool CGameLogic::RemoveValueCardOne(BYTE cbCardData[], BYTE cbCardCount,BYTE cbRemoveCard)
	{
		BYTE cbRemoveCardArray[MAX_INDEX];
		cbRemoveCardArray[0] = cbRemoveCard;
		return RemoveValueCard(cbCardData,cbCardCount,cbRemoveCardArray,1);
	}
	//…æ≥˝∆ÀøÀ
	bool CGameLogic::RemoveValueCard(BYTE cbCardData[], BYTE cbCardCount, const BYTE cbRemoveCard[], BYTE cbRemoveCount)
	{
		//ºÏ—È ˝æ›
		ASSERT(cbCardCount<=14);
		ASSERT(cbRemoveCount<=cbCardCount);

		//∂®“Â±‰¡ø
		BYTE cbDeleteCount=0,cbTempCardData[14];
		if (cbCardCount>CountArray(cbTempCardData))
			return false;
		memcpy(cbTempCardData,cbCardData,cbCardCount*sizeof(cbCardData[0]));

		//÷√¡„∆ÀøÀ
		for (BYTE i=0;i<cbRemoveCount;i++)
		{
			for (BYTE j=0;j<cbCardCount;j++)
			{
				if (cbRemoveCard[i]==cbTempCardData[j])
				{
					cbDeleteCount++;
					cbTempCardData[j]=0;
					break;
				}
			}
		}

		//≥…π¶≈–∂œ
		//if (cbDeleteCount!=cbRemoveCount) 
		//{
		//	ASSERT(FALSE);
		//	return false;
		//}

		//«Â¿Ì∆ÀøÀ
		BYTE cbCardPos=0;
		for (BYTE i=0;i<cbCardCount;i++)
		{
			if (cbTempCardData[i]!=0) 
				cbCardData[cbCardPos++]=cbTempCardData[i];
		}

		return true;
	}

	//”––ß≈–∂œ
	bool CGameLogic::IsValidCard(BYTE cbCardData)
	{
		BYTE cbValue=(cbCardData&MASK_VALUE);
		BYTE cbColor=(cbCardData&MASK_COLOR)>>4;
		return (((cbValue>=1)&&(cbValue<=9)&&(cbColor<=2))||((cbValue>=1)&&(cbValue<=7)&&(cbColor==3)));
	}

	//∆ÀøÀ ˝ƒø
	BYTE CGameLogic::GetCardCount(const BYTE cbCardIndex[MAX_INDEX])
	{
		// ˝ƒøÕ≥º∆
		BYTE cbCardCount=0;
		for (BYTE i=0;i<MAX_INDEX;i++) 
			cbCardCount+=cbCardIndex[i];

		return cbCardCount;
	}

	//ªÒ»°◊È∫œ
	BYTE CGameLogic::GetWeaveCard(BYTE cbWeaveKind, BYTE cbCenterCard, BYTE cbCardBuffer[4])
	{
		//◊È∫œ∆ÀøÀ
		switch (cbWeaveKind)
		{
		case WIK_LEFT:		//…œ≈∆≤Ÿ◊˜
			{
				//…Ë÷√±‰¡ø
				cbCardBuffer[0]=cbCenterCard;
				cbCardBuffer[1]=cbCenterCard+1;
				cbCardBuffer[2]=cbCenterCard+2;

				return 3;
			}
		case WIK_RIGHT:		//…œ≈∆≤Ÿ◊˜
			{
				//…Ë÷√±‰¡ø
				cbCardBuffer[0]=cbCenterCard;
				cbCardBuffer[1]=cbCenterCard-1;
				cbCardBuffer[2]=cbCenterCard-2;

				return 3;
			}
		case WIK_CENTER:	//…œ≈∆≤Ÿ◊˜
			{
				//…Ë÷√±‰¡ø
				cbCardBuffer[0]=cbCenterCard;
				cbCardBuffer[1]=cbCenterCard-1;
				cbCardBuffer[2]=cbCenterCard+1;

				return 3;
			}
		case WIK_PENG:		//≈ˆ≈∆≤Ÿ◊˜
			{
				//…Ë÷√±‰¡ø
				cbCardBuffer[0]=cbCenterCard;
				cbCardBuffer[1]=cbCenterCard;
				cbCardBuffer[2]=cbCenterCard;

				return 3;
			}
		case WIK_GANG:		//∏‹≈∆≤Ÿ◊˜
			{
				//…Ë÷√±‰¡ø
				cbCardBuffer[0]=cbCenterCard;
				cbCardBuffer[1]=cbCenterCard;
				cbCardBuffer[2]=cbCenterCard;
				cbCardBuffer[3]=cbCenterCard;

				return 4;
			}
		default:
            {
                cocos2d::log("GetWeaveCard：%x",cbWeaveKind);
                //ASSERT(FALSE);
            }
        }
        
        return 0;
	}

	//∂Ø◊˜µ»º∂
	BYTE CGameLogic::GetUserActionRank(BYTE cbUserAction)
	{
		//∫˙≈∆µ»º∂
		if (cbUserAction&WIK_CHI_HU) { return 4; }

		//∏‹≈∆µ»º∂
		if (cbUserAction&WIK_GANG) { return 3; }

		//≈ˆ≈∆µ»º∂
		if (cbUserAction&WIK_PENG) { return 2; }

		//…œ≈∆µ»º∂
		if (cbUserAction&(WIK_RIGHT|WIK_CENTER|WIK_LEFT)) { return 1; }

		return 0;
	}

	//∫˙≈∆µ»º∂
	WORD CGameLogic::GetChiHuActionRank(const CChiHuRight & ChiHuRight)
	{
		WORD wFanShu = 0;
		return wFanShu;
	}

	//≥‘≈∆≈–∂œ
	BYTE CGameLogic::EstimateEatCard(const BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
	{
		//≤Œ ˝–ß—È
		ASSERT(IsValidCard(cbCurrentCard));

		//π˝¬À≈–∂œ
		if ( cbCurrentCard>=0x31 || IsMagicCard(cbCurrentCard) ) 
			return WIK_NULL;

		//±‰¡ø∂®“Â
		BYTE cbExcursion[3]={0,1,2};
		BYTE cbItemKind[3]={WIK_LEFT,WIK_CENTER,WIK_RIGHT};

		//≥‘≈∆≈–∂œ
		BYTE cbEatKind=0,cbFirstIndex=0;
		BYTE cbCurrentIndex=SwitchToCardIndex(cbCurrentCard);
		for (BYTE i=0;i<CountArray(cbItemKind);i++)
		{
			BYTE cbValueIndex=cbCurrentIndex%9;
			if ((cbValueIndex>=cbExcursion[i])&&((cbValueIndex-cbExcursion[i])<=6))
			{
				//≥‘≈∆≈–∂œ
				cbFirstIndex=cbCurrentIndex-cbExcursion[i];

				//≥‘≈∆≤ªƒ‹∞¸∫¨”–Õı∞‘
				if( m_cbMagicIndex != MAX_INDEX &&
					m_cbMagicIndex >= cbFirstIndex && m_cbMagicIndex <= cbFirstIndex+2 ) continue;

				if ((cbCurrentIndex!=cbFirstIndex)&&(cbCardIndex[cbFirstIndex]==0))
					continue;
				if ((cbCurrentIndex!=(cbFirstIndex+1))&&(cbCardIndex[cbFirstIndex+1]==0))
					continue;
				if ((cbCurrentIndex!=(cbFirstIndex+2))&&(cbCardIndex[cbFirstIndex+2]==0))
					continue;

				//…Ë÷√¿‡–Õ
				cbEatKind|=cbItemKind[i];
			}
		}

		return cbEatKind;
	}

	//≈ˆ≈∆≈–∂œ
	BYTE CGameLogic::EstimatePengCard(const BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
	{
		//≤Œ ˝–ß—È
		ASSERT(IsValidCard(cbCurrentCard));

		//π˝¬À≈–∂œ
		if ( IsMagicCard(cbCurrentCard) ) 
			return WIK_NULL;

		//≈ˆ≈∆≈–∂œ
		return (cbCardIndex[SwitchToCardIndex(cbCurrentCard)]>=2)?WIK_PENG:WIK_NULL;
	}

	//∏‹≈∆≈–∂œ
	BYTE CGameLogic::EstimateGangCard(const BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
	{
		//≤Œ ˝–ß—È
		ASSERT(IsValidCard(cbCurrentCard));

		//π˝¬À≈–∂œ
		if ( IsMagicCard(cbCurrentCard) ) 
			return WIK_NULL;
        ////判断痞子
        //if (IsPiZiCard(cbCurrentCard))
        //    return WIK_GANG;
		//∏‹≈∆≈–∂œ
		return (cbCardIndex[SwitchToCardIndex(cbCurrentCard)]==3)?WIK_GANG:WIK_NULL;
	}

	//∏‹≈∆∑÷Œˆ
	BYTE CGameLogic::AnalyseGangCard(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount, tagGangCardResult & GangCardResult)
	{//cbCardIndex是手上的牌, 不包含放下去的组合牌
		//…Ë÷√±‰¡ø
		BYTE cbActionMask=WIK_NULL;
		zeromemory(&GangCardResult,sizeof(GangCardResult));

		// ÷…œ∏‹≈∆
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			if( i == m_cbMagicIndex ) continue;

			if (IsPiZiCard(SwitchToCardData(i)) && cbCardIndex[i] > 0)
			{//手牌有痞子可以一个就杠
				cbActionMask |= WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount++] = SwitchToCardData(i);
			}

			if (cbCardIndex[i]==4)
			{
				cbActionMask|=WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount++]=SwitchToCardData(i);
			}
		}

		//◊È∫œ∏‹≈∆
		for (BYTE i=0;i<cbWeaveCount;i++)
		{
			if (WeaveItem[i].cbWeaveKind==WIK_PENG)
			{
				if (cbCardIndex[SwitchToCardIndex(WeaveItem[i].cbCenterCard)]==1)
				{
					cbActionMask|=WIK_GANG;
					GangCardResult.cbCardData[GangCardResult.cbCardCount++]=WeaveItem[i].cbCenterCard;
				}
			}
		}

		return cbActionMask;
	}


	BYTE CGameLogic::AnalyseGangCard(const BYTE cbCardIndex[MAX_INDEX], const CMD_WeaveItem WeaveItem[], BYTE cbWeaveCount, tagGangCardResult & GangCardResult)
	{
		//…Ë÷√±‰¡ø
		BYTE cbActionMask=WIK_NULL;
		zeromemory(&GangCardResult,sizeof(GangCardResult));

		// ÷…œ∏‹≈∆
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			if( i == m_cbMagicIndex ) continue;

			if (IsPiZiCard(SwitchToCardData(i)) && cbCardIndex[i] > 0)
			{//手牌有痞子可以一个就杠
				cbActionMask |= WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount++] = SwitchToCardData(i);
			}

			if (cbCardIndex[i]==4)
			{
				cbActionMask|=WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount++]=SwitchToCardData(i);
			}
		}

		//◊È∫œ∏‹≈∆
		for (BYTE i=0;i<cbWeaveCount;i++)
		{
			if (WeaveItem[i].cbWeaveKind==WIK_PENG)
			{
				if (cbCardIndex[SwitchToCardIndex(WeaveItem[i].cbCenterCard)]==1)
				{
					cbActionMask|=WIK_GANG;
					GangCardResult.cbCardData[GangCardResult.cbCardCount++]=WeaveItem[i].cbCenterCard;
				}
			}
		}

		return cbActionMask;
	}

	//∆ÀøÀ◊™ªª
	BYTE CGameLogic::SwitchToCardData(BYTE cbCardIndex)
	{
		ASSERT(cbCardIndex<34);
		return ((cbCardIndex/9)<<4)|(cbCardIndex%9+1);
	}

	//∆ÀøÀ◊™ªª
	BYTE CGameLogic::SwitchToCardIndex(BYTE cbCardData)
	{
		ASSERT(IsValidCard(cbCardData));
		return ((cbCardData&MASK_COLOR)>>4)*9+(cbCardData&MASK_VALUE)-1;
	}

	//∆ÀøÀ◊™ªª
	BYTE CGameLogic::SwitchToCardData(const BYTE cbCardIndex[MAX_INDEX], BYTE cbCardData[MAX_COUNT])
	{
		//◊™ªª∆ÀøÀ
		BYTE cbPosition=0;
		//◊Í≈∆
		if( m_cbMagicIndex != MAX_INDEX )
		{
			for( BYTE i = 0; i < cbCardIndex[m_cbMagicIndex]; i++ )
				cbCardData[cbPosition++] = SwitchToCardData(m_cbMagicIndex);
		}
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			if( i == m_cbMagicIndex ) continue;
			if (cbCardIndex[i]!=0)
			{
				for (BYTE j=0;j<cbCardIndex[i];j++)
				{
					ASSERT(cbPosition<MAX_COUNT);
					cbCardData[cbPosition++]=SwitchToCardData(i);
				}
			}
		}

		return cbPosition;
	}

	//∆ÀøÀ◊™ªª
	BYTE CGameLogic::SwitchToCardIndex(const BYTE cbCardData[], BYTE cbCardCount, BYTE cbCardIndex[MAX_INDEX])
	{
		//…Ë÷√±‰¡ø
		zeromemory(cbCardIndex,sizeof(BYTE)*MAX_INDEX);

		//◊™ªª∆ÀøÀ
		for (BYTE i=0;i<cbCardCount;i++)
		{
			ASSERT(IsValidCard(cbCardData[i]));
			cbCardIndex[SwitchToCardIndex(cbCardData[i])]++;
		}

		return cbCardCount;
	}

    void CGameLogic::SetMagicData(BYTE cbMagicData)
    {
        this->SetMagicIndex(this->SwitchToCardIndex(cbMagicData));
    }

    //◊Í≈∆
	bool CGameLogic::IsMagicCard( BYTE cbCardData )
	{
		if( m_cbMagicIndex != MAX_INDEX )
			return SwitchToCardIndex(cbCardData) == m_cbMagicIndex;
		return false;
	}

    void CGameLogic::SetPiZiData(BYTE cbPiZiData_0, BYTE cbPiZiData_1)
    {
        assert(cbPiZiData_0 != SwitchToCardIndex(0x35) && cbPiZiData_0 != SwitchToCardIndex(0x36));
        assert(cbPiZiData_1 != SwitchToCardIndex(0x35) && cbPiZiData_1 != SwitchToCardIndex(0x36));
        m_cbPiZiIndex[0] = SwitchToCardIndex(cbPiZiData_0);
        m_cbPiZiIndex[1] = SwitchToCardIndex(cbPiZiData_1);
    }

    bool CGameLogic::IsPiZiCard(BYTE cbCardData)
    {
		bool result = false;
		if (m_cbMagicIndex != MAX_INDEX)
		{
			result = SwitchToCardIndex(cbCardData) == m_cbPiZiIndex[0] ||
				SwitchToCardIndex(cbCardData) == m_cbPiZiIndex[1];
		}
		return result;
    }

	//排序,根据牌值排序
	bool CGameLogic::SortCardList( BYTE cbCardData[MAX_COUNT], BYTE cbCardCount )
	{//黄石晃晃 红发赖筒万条白板   (筒万条ui那已经调好顺序了的)
		//数目过虑
		if (cbCardCount==0||cbCardCount>MAX_COUNT) return false;

		//排序操作
		bool bSorted=true;
		BYTE cbSwitchData=0,cbLast=cbCardCount-1;
		do
		{
			bSorted=true;
			for (BYTE i=0;i<cbLast;i++)
			{
				int iTemp1 = cbCardData[i];
				int iTemp2 = cbCardData[i+1];

                if (iTemp1 > 0x30 && iTemp1 < 0x37)
				{//中发
					iTemp1 -= 0x120;
				}
                else if (iTemp1 > 0 && this->IsMagicCard(iTemp1))
                {//癞子
                    iTemp1 -= 0x80;
                }
                else if (iTemp1 == 0x37)
                {//白板
                    iTemp1 += 0x40;
                }

                if (iTemp2 > 0x30 && iTemp2 < 0x37)
                {//中发
                    iTemp2 -= 0x120;
                }
                else if (iTemp2>0 && this->IsMagicCard(iTemp2))
                {//癞子
                    iTemp2 -= 0x80;
                }
                else if (iTemp1 == 0x37)
                {//白板
                    iTemp2 += 0x40;
                }

				if (iTemp1>iTemp2)
				{
					//设置标志
					bSorted=false;
					//扑克数据
					cbSwitchData=cbCardData[i];
					cbCardData[i]=cbCardData[i+1];
					cbCardData[i+1]=cbSwitchData;
				}	
			}
			cbLast--;
		} while(bSorted==false);

		return true;
	}

    /*
    // 胡法分析函数
    */

    //大对子
	bool CGameLogic::IsPengPeng( const tagAnalyseItem *pAnalyseItem )
	{
		for( BYTE i = 0; i < CountArray(pAnalyseItem->cbWeaveKind); i++ )
		{
			if( pAnalyseItem->cbWeaveKind[i]&(WIK_LEFT|WIK_CENTER|WIK_RIGHT) )
				return false;
		}
		return true;
	}

	//«Â“ª…´≈∆
	bool CGameLogic::IsQingYiSe(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], const BYTE cbItemCount,const BYTE cbCurrentCard)
	{
		//∫˙≈∆≈–∂œ
		BYTE cbCardColor=0xFF;

		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			if(i==m_cbMagicIndex) continue;
			if (cbCardIndex[i]!=0)
			{
				//ª®…´≈–∂œ
				if (cbCardColor!=0xFF)
					return false;

				//…Ë÷√ª®…´
				cbCardColor=(SwitchToCardData(i)&MASK_COLOR);

				//…Ë÷√À˜“˝
				i=(i/9+1)*9-1;
			}
		}

		//»Áπ˚ ÷…œ÷ª”–Õı∞‘
		if( cbCardColor == 0xFF )
		{
			ASSERT( m_cbMagicIndex != MAX_INDEX && cbCardIndex[m_cbMagicIndex] > 0 );
			//ºÏ≤È◊È∫œ
			ASSERT( cbItemCount > 0 );
			cbCardColor = WeaveItem[0].cbCenterCard&MASK_COLOR;
		}

		if((cbCurrentCard&MASK_COLOR)!=cbCardColor && !IsMagicCard(cbCurrentCard) ) return false;

		//◊È∫œ≈–∂œ
		for (BYTE i=0;i<cbItemCount;i++)
		{
			BYTE cbCenterCard=WeaveItem[i].cbCenterCard;
			if ((cbCenterCard&MASK_COLOR)!=cbCardColor)	return false;
		}

		return true;
	}

	//∆ﬂ–°∂‘≈∆
	bool CGameLogic::IsQiXiaoDui(const BYTE cbCardIndex[MAX_INDEX], const tagWeaveItem WeaveItem[], const BYTE cbWeaveCount,const BYTE cbCurrentCard)
	{
		//◊È∫œ≈–∂œ
		if (cbWeaveCount!=0) return false;

		//µ•≈∆ ˝ƒø
		BYTE cbReplaceCount = 0;

		//¡Ÿ ± ˝æ›
		BYTE cbCardIndexTemp[MAX_INDEX];
		memcpy(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));

		//≤Â»Î ˝æ›
		BYTE cbCurrentIndex = SwitchToCardIndex(cbCurrentCard);
		cbCardIndexTemp[cbCurrentIndex]++;

		//º∆À„µ•≈∆
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			BYTE cbCardCount=cbCardIndexTemp[i];

			//Õı≈∆π˝¬À
			if( i == m_cbMagicIndex ) continue;

			//µ•≈∆Õ≥º∆
			if( cbCardCount == 1 || cbCardCount == 3 ) 	cbReplaceCount++;
		}

		//Õı≈∆≤ªπª
		if( m_cbMagicIndex != MAX_INDEX && cbReplaceCount > cbCardIndexTemp[m_cbMagicIndex] ||
			m_cbMagicIndex == MAX_INDEX && cbReplaceCount > 0 )
			return false;

		return true;

	}

	//¥¯Á€
	bool CGameLogic::IsDaiYao( const tagAnalyseItem *pAnalyseItem )
	{
		//ºÏ≤È≈∆—€
		BYTE cbCardValue = pAnalyseItem->cbCardEye&MASK_VALUE;
		if( cbCardValue != 1 && cbCardValue != 9 ) return false;

		for( BYTE i = 0; i < CountArray(pAnalyseItem->cbWeaveKind); i++ )
		{
			if( pAnalyseItem->cbWeaveKind[i]&(WIK_LEFT|WIK_CENTER|WIK_RIGHT) )
			{
				BYTE j = 0;
				for(; j < 3; j++ )
				{
					cbCardValue = pAnalyseItem->cbCardData[i][j]&MASK_VALUE;
					if( cbCardValue == 1 || cbCardValue == 9 ) break;
				}
				if( j == 3 ) return false;
			}
			else
			{
				cbCardValue = pAnalyseItem->cbCenterCard[i]&MASK_VALUE;
				if( cbCardValue != 1 && cbCardValue != 9 ) return false;
			}
		}
		return true;
	}

	//Ω´∂‘
	bool CGameLogic::IsJiangDui( const tagAnalyseItem *pAnalyseItem )
	{
		// «∑Ò¥Û∂‘◊”
		if( !IsPengPeng(pAnalyseItem) ) return false;

		//ºÏ≤È≈∆—€
		BYTE cbCardValue = pAnalyseItem->cbCardEye&MASK_VALUE;
		if( cbCardValue != 2 && cbCardValue != 5 && cbCardValue != 8 ) return false;

		for( BYTE i = 0; i < CountArray(pAnalyseItem->cbWeaveKind); i++ )
		{
			if( pAnalyseItem->cbWeaveKind[i]&(WIK_LEFT|WIK_CENTER|WIK_RIGHT) )
			{
				BYTE j = 0;
				for(; j < 3; j++ )
				{
					cbCardValue = pAnalyseItem->cbCardData[i][j]&MASK_VALUE;
					if( cbCardValue == 2 || cbCardValue == 5 || cbCardValue == 8 ) break;
				}
				if( j == 3 ) return false;
			}
			else
			{
				cbCardValue = pAnalyseItem->cbCenterCard[i]&MASK_VALUE;
				if( cbCardValue != 2 && cbCardValue != 5 && cbCardValue != 8 ) return false;
			}
		}
		return true;
	}

	// «∑Òª®÷Ì
	bool CGameLogic::IsHuaZhu( const BYTE cbCardIndex[], const tagWeaveItem WeaveItem[], BYTE cbWeaveCount )
	{
		BYTE cbColor[3] = { 0,0,0 };
		for( BYTE i = 0; i < MAX_INDEX; i++ )
		{
			if( cbCardIndex[i] > 0 )
			{
				BYTE cbCardColor = SwitchToCardData(i)&MASK_COLOR;
				cbColor[cbCardColor>>4]++;

				i = (i/9+1)*9-1;
			}
		}
		for( BYTE i = 0; i < cbWeaveCount; i++ )
		{
			BYTE cbCardColor = WeaveItem[i].cbCenterCard&MASK_COLOR;
			cbColor[cbCardColor>>4]++;
		}
		//»±“ª√≈æÕ≤ª «ª®÷Ì
		for( BYTE i = 0; i < CountArray(cbColor); i++ )
			if( cbColor[i] == 0 ) return false;

		return true;
	}

	BYTE CGameLogic::GetCardColor(BYTE cbCardDat)
	{
		ASSERT(IsValidCard(cbCardDat));
		return ((cbCardDat&MASK_COLOR)>>4)+1;
	}

	BYTE CGameLogic::GetCardValue(BYTE cbCardDat)
	{
		ASSERT(IsValidCard(cbCardDat));
		return (cbCardDat&MASK_VALUE);
	}

	bool CGameLogic::AnalyseCard(const BYTE cbCardIndex[MAX_INDEX], const CMD_WeaveItem WeaveItem[], BYTE cbWeaveCount, AnalyseItemList & kAnalyseItemList)
	{
		//º∆À„ ˝ƒø
		BYTE cbCardCount=GetCardCount(cbCardIndex);

		//–ß—È ˝ƒø
		ASSERT((cbCardCount>=2)&&(cbCardCount<=MAX_COUNT)&&((cbCardCount-2)%3==0));
		if ((cbCardCount<2)||(cbCardCount>MAX_COUNT)||((cbCardCount-2)%3!=0))
			return false;

		//±‰¡ø∂®“Â
		BYTE cbKindItemCount=0;
		tagKindItem KindItem[27*2+7+14];
		zeromemory(KindItem,sizeof(KindItem));

		//–Ë«Û≈–∂œ
		BYTE cbLessKindItem=(cbCardCount-2)/3;
		ASSERT((cbLessKindItem+cbWeaveCount)==4);

		//µ•µı≈–∂œ
		if (cbLessKindItem==0)
		{
			//–ß—È≤Œ ˝
			ASSERT((cbCardCount==2)&&(cbWeaveCount==4));

			//≈∆—€≈–∂œ
			for (BYTE i=0;i<MAX_INDEX;i++)
			{
				if (cbCardIndex[i]==2 || 
					( m_cbMagicIndex != MAX_INDEX && i != m_cbMagicIndex && cbCardIndex[m_cbMagicIndex]+cbCardIndex[i]==2 ) )
				{
					//±‰¡ø∂®“Â
					tagAnalyseItem AnalyseItem;
					zeromemory(&AnalyseItem,sizeof(AnalyseItem));

					//…Ë÷√Ω·π˚
					for (BYTE j=0;j<cbWeaveCount;j++)
					{
						AnalyseItem.cbWeaveKind[j]=WeaveItem[j].cbWeaveKind;
						AnalyseItem.cbCenterCard[j]=WeaveItem[j].cbCenterCard;
						GetWeaveCard( WeaveItem[j].cbWeaveKind,WeaveItem[j].cbCenterCard,AnalyseItem.cbCardData[j] );
					}
					AnalyseItem.cbCardEye=SwitchToCardData(i);
					if( cbCardIndex[i] < 2 || i == m_cbMagicIndex )
						AnalyseItem.bMagicEye = true;
					else AnalyseItem.bMagicEye = false;

					//≤Â»ÎΩ·π˚
					kAnalyseItemList.push_back(AnalyseItem);

					return true;
				}
			}

			return false;
		}

		//≤∑÷∑÷Œˆ
		BYTE cbMagicCardIndex[MAX_INDEX];
		memcpy(cbMagicCardIndex,cbCardIndex,sizeof(cbMagicCardIndex));
		BYTE cbMagicCardCount = 0;
		if( m_cbMagicIndex != MAX_INDEX )
		{
			cbMagicCardCount = cbCardIndex[m_cbMagicIndex];
			if( cbMagicCardIndex[m_cbMagicIndex] ) cbMagicCardIndex[m_cbMagicIndex] = 1;		//ºı–°∂‡”‡◊È∫œ
		}
		if (cbCardCount>=3)
		{
			for (BYTE i=0;i<MAX_INDEX;i++)
			{
				//Õ¨≈∆≈–∂œ
				if (cbMagicCardIndex[i]+cbMagicCardCount>=3)
				{
					ASSERT( cbKindItemCount < CountArray(KindItem) );
					KindItem[cbKindItemCount].cbCardIndex[0]=i;
					KindItem[cbKindItemCount].cbCardIndex[1]=i;
					KindItem[cbKindItemCount].cbCardIndex[2]=i;
					KindItem[cbKindItemCount].cbWeaveKind=WIK_PENG;
					KindItem[cbKindItemCount].cbCenterCard=SwitchToCardData(i);
					KindItem[cbKindItemCount].cbValidIndex[0] = cbMagicCardIndex[i]>0?i:m_cbMagicIndex;
					KindItem[cbKindItemCount].cbValidIndex[1] = cbMagicCardIndex[i]>1?i:m_cbMagicIndex;
					KindItem[cbKindItemCount].cbValidIndex[2] = cbMagicCardIndex[i]>2?i:m_cbMagicIndex;
					cbKindItemCount++;
					if(cbMagicCardIndex[i]+cbMagicCardCount>=6)
					{
						ASSERT( cbKindItemCount < CountArray(KindItem) );
						KindItem[cbKindItemCount].cbCardIndex[0]=i;
						KindItem[cbKindItemCount].cbCardIndex[1]=i;
						KindItem[cbKindItemCount].cbCardIndex[2]=i;
						KindItem[cbKindItemCount].cbWeaveKind=WIK_PENG;
						KindItem[cbKindItemCount].cbCenterCard=SwitchToCardData(i);
						KindItem[cbKindItemCount].cbValidIndex[0] = cbMagicCardIndex[i]>3?i:m_cbMagicIndex;
						KindItem[cbKindItemCount].cbValidIndex[1] = m_cbMagicIndex;
						KindItem[cbKindItemCount].cbValidIndex[2] = m_cbMagicIndex;
						cbKindItemCount++;
					}
				}

				//¡¨≈∆≈–∂œ
				if ((i<(MAX_INDEX-9))&&((i%9)<7))
				{
					//÷ª“™≤∆…Ò≈∆ ˝º”…œ3∏ˆÀ≥–ÚÀ˜“˝µƒ≈∆ ˝¥Û”⁄µ»”⁄3,‘ÚΩ¯––◊È∫œ

					if( cbMagicCardCount+cbMagicCardIndex[i]+cbMagicCardIndex[i+1]+cbMagicCardIndex[i+2] >= 3 )
					{
						BYTE cbIndex[3] = { i==m_cbMagicIndex?(BYTE)0:
							cbMagicCardIndex[i],(i+1)==m_cbMagicIndex?(BYTE)0:cbMagicCardIndex[i+1],
							(i+2)==m_cbMagicIndex?(BYTE)0:cbMagicCardIndex[i+2] };
						int nMagicCountTemp = cbMagicCardCount;
						BYTE cbValidIndex[3];
						while( nMagicCountTemp+cbIndex[0]+cbIndex[1]+cbIndex[2] >= 3 )
						{
							for( BYTE j = 0; j < CountArray(cbIndex); j++ )
							{
								if( cbIndex[j] > 0 ) 
								{
									cbIndex[j]--;
									cbValidIndex[j] = i+j;
								}
								else 
								{
									nMagicCountTemp--;
									cbValidIndex[j] = m_cbMagicIndex;
								}
							}
							if( nMagicCountTemp >= 0 )
							{
								ASSERT( cbKindItemCount < CountArray(KindItem) );
								KindItem[cbKindItemCount].cbCardIndex[0]=i;
								KindItem[cbKindItemCount].cbCardIndex[1]=i+1;
								KindItem[cbKindItemCount].cbCardIndex[2]=i+2;
								KindItem[cbKindItemCount].cbWeaveKind=WIK_LEFT;
								KindItem[cbKindItemCount].cbCenterCard=SwitchToCardData(i);
								memcpy( KindItem[cbKindItemCount].cbValidIndex,cbValidIndex,sizeof(cbValidIndex) );
								cbKindItemCount++;
							}
							else break;
						}
					}
				}
			}
		}

		//◊È∫œ∑÷Œˆ
		if (cbKindItemCount>=cbLessKindItem)
		{
			//±‰¡ø∂®“Â
			BYTE cbCardIndexTemp[MAX_INDEX];
			zeromemory(cbCardIndexTemp,sizeof(cbCardIndexTemp));

			//±‰¡ø∂®“Â
			BYTE cbIndex[4]={0,1,2,3};
			tagKindItem pKindItem[4];
			zeromemory(pKindItem,sizeof(pKindItem));

			//ø™ º◊È∫œ
			do
			{
				//…Ë÷√±‰¡ø
				memcpy(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));
				for (BYTE i=0;i<cbLessKindItem;i++)
					pKindItem[i]=KindItem[cbIndex[i]];

				// ˝¡ø≈–∂œ
				bool bEnoughCard=true;
				for (BYTE i=0;i<cbLessKindItem*3;i++)
				{
					//¥Ê‘⁄≈–∂œ
					BYTE cbCardIndex=pKindItem[i/3].cbValidIndex[i%3]; 
					if (cbCardIndexTemp[cbCardIndex]==0)
					{	
						if( m_cbMagicIndex != MAX_INDEX && cbCardIndexTemp[m_cbMagicIndex] > 0 )
						{
							if (cbCardIndexTemp[m_cbMagicIndex] > 0)
							{
								cbCardIndexTemp[m_cbMagicIndex]--;
								pKindItem[i/3].cbValidIndex[i%3] = m_cbMagicIndex;
							}
						}
						else
						{
							bEnoughCard=false;
							break;
						}
					}
					else 
						cbCardIndexTemp[cbCardIndex]--;
				}

				//∫˙≈∆≈–∂œ
				if (bEnoughCard==true)
				{
					//≈∆—€≈–∂œ
					BYTE cbCardEye=0;
					bool bMagicEye = false;
					for (BYTE i=0;i<MAX_INDEX;i++)
					{
						if (cbCardIndexTemp[i]==2)
						{
							cbCardEye=SwitchToCardData(i);
							if( i == m_cbMagicIndex ) bMagicEye = true;
							break;
						}
						else if( i!=m_cbMagicIndex && 
							m_cbMagicIndex != MAX_INDEX && cbCardIndexTemp[i]+cbCardIndexTemp[m_cbMagicIndex]==2 )
						{
							cbCardEye = SwitchToCardData(i);
							bMagicEye = true;
						}
					}

					//◊È∫œ¿‡–Õ
					if (cbCardEye!=0)
					{
						//±‰¡ø∂®“Â
						tagAnalyseItem AnalyseItem;
						zeromemory(&AnalyseItem,sizeof(AnalyseItem));

						//…Ë÷√◊È∫œ
						for (BYTE i=0;i<cbWeaveCount;i++)
						{
							AnalyseItem.cbWeaveKind[i]=WeaveItem[i].cbWeaveKind;
							AnalyseItem.cbCenterCard[i]=WeaveItem[i].cbCenterCard;
                            cocos2d::log("WeaveItem[i].cbWeaveKind：%x",WeaveItem[i].cbWeaveKind);
							GetWeaveCard( WeaveItem[i].cbWeaveKind,WeaveItem[i].cbCenterCard,
								AnalyseItem.cbCardData[i] );
						}

						//…Ë÷√≈∆–Õ
						for (BYTE i=0;i<cbLessKindItem;i++) 
						{
							AnalyseItem.cbWeaveKind[i+cbWeaveCount]=pKindItem[i].cbWeaveKind;
							AnalyseItem.cbCenterCard[i+cbWeaveCount]=pKindItem[i].cbCenterCard;
							AnalyseItem.cbCardData[cbWeaveCount+i][0] = SwitchToCardData(pKindItem[i].cbValidIndex[0]);
							AnalyseItem.cbCardData[cbWeaveCount+i][1] = SwitchToCardData(pKindItem[i].cbValidIndex[1]);
							AnalyseItem.cbCardData[cbWeaveCount+i][2] = SwitchToCardData(pKindItem[i].cbValidIndex[2]);
						}

						//…Ë÷√≈∆—€
						AnalyseItem.cbCardEye=cbCardEye;
						AnalyseItem.bMagicEye = bMagicEye;

						//≤Â»ÎΩ·π˚
						kAnalyseItemList.push_back(AnalyseItem);
					}
				}

				//…Ë÷√À˜“˝
				if (cbIndex[cbLessKindItem-1]==(cbKindItemCount-1))
				{
					BYTE i=cbLessKindItem-1;
					for (;i>0;i--)
					{
						if ((cbIndex[i-1]+1)!=cbIndex[i])
						{
							BYTE cbNewIndex=cbIndex[i-1];
							for (BYTE j=(i-1);j<cbLessKindItem;j++) 
								cbIndex[j]=cbNewIndex+j-i+2;
							break;
						}
					}
					if (i==0)
						break;
				}
				else
					cbIndex[cbLessKindItem-1]++;
			} while (true);

		}

		return kAnalyseItemList.size()>0;
	}

	bool CGameLogic::isGangTing(const BYTE cbCardIndex[MAX_INDEX],const CMD_WeaveItem WeaveItem[], BYTE cbWeaveCount)
	{
		BYTE cbTempCardIndex[MAX_INDEX];
		for (int i = 0;i<MAX_INDEX;i++)
		{
			cbTempCardIndex[i] = cbCardIndex[i];
		}
		BYTE							cbWeaveItemCount = cbWeaveCount;		//◊È∫œ ˝ƒø
		CMD_WeaveItem					tagWeaveItemArray[MAX_WEAVE];//◊È∫œ∆ÀøÀ
		for (int i = 0;i<MAX_WEAVE;i++)
		{
			tagWeaveItemArray[i] = WeaveItem[i];
		}
		bool bSucessGang = false;
		for (int i = 0;i<MAX_INDEX;i++)
		{
			if (cbTempCardIndex[i] == 1)
			{
				BYTE cbCard = SwitchToCardData(i);
				for (BYTE m=0;m<cbWeaveItemCount;m++)
				{
					DWORD cbWeaveKind=tagWeaveItemArray[m].cbWeaveKind;
					DWORD cbCenterCard=tagWeaveItemArray[m].cbCenterCard;
					if ((cbCenterCard==cbCard)&&(cbWeaveKind==WIK_PENG))
					{
						cbTempCardIndex[i] = 0;
						bSucessGang = true;
						break;
					}
				}
			}
			if (bSucessGang)
			{
				break;
			}
		}
		for (int i = 0;i<MAX_INDEX && !bSucessGang;i++)
		{
			if (cbTempCardIndex[i] == 4)
			{
				BYTE cbCard = SwitchToCardData(i);
				cbWeaveItemCount++;
				tagWeaveItemArray[cbWeaveItemCount].cbPublicCard = true;
				tagWeaveItemArray[cbWeaveItemCount].cbCenterCard = cbCard;
				tagWeaveItemArray[cbWeaveItemCount].cbWeaveKind = WIK_GANG;
				BYTE cbRemoveCard[]={cbCard,cbCard,cbCard,cbCard};
				RemoveCard(cbTempCardIndex,cbRemoveCard,CountArray(cbRemoveCard));
				break;
			}
		}
		std::vector<BYTE> kCards;
		getTingCardHZ(cbTempCardIndex,tagWeaveItemArray,cbWeaveItemCount,kCards);
		return kCards.size() > 0;
	}
	void CGameLogic::getTingCardHZ(const BYTE cbCardIndex[MAX_INDEX],const CMD_WeaveItem WeaveItem[], BYTE cbWeaveCount,std::vector<BYTE>& kTingCardList)
	{
		kTingCardList.clear();
		HNMJ::AnalyseItemList kAnalyseItemList;

		HNMJ::CGameLogic::Instance().SetMagicIndex(SwitchToCardIndex(0x35));
		//ππ‘Ï∆ÀøÀ
		BYTE cbCardIndexTemp[MAX_INDEX];
		memcpy(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));

		for (int i=0;i<MAX_INDEX-ZI_PAI_COUNT;i++)
		{
			cbCardIndexTemp[i]++;
			kAnalyseItemList.clear();
			bool bValue = HNMJ::CGameLogic::Instance().AnalyseCard(cbCardIndexTemp,WeaveItem,cbWeaveCount,kAnalyseItemList);
			if (bValue)
			{
				kTingCardList.push_back(HNMJ::CGameLogic::Instance().SwitchToCardData(i));
			}
			cbCardIndexTemp[i]--;
		}
		if ((int)kTingCardList.size()>0)
		{
			kTingCardList.push_back(0x35);
		}
	}

    int CGameLogic::GetPiZiZFBGangCount(const BYTE cbCardData[MAX_COUNT])
    {
        int zhongCount = 0;
        int faCount = 0;
        int baiCount = 0;
        for (int i = 0; i < MAX_COUNT; i++)
        {
            if (cbCardData[i] == 0x35)
            {
                zhongCount++;
            }
            if (cbCardData[i] == 0x36)
            {
                faCount++;
            }
            if (cbCardData[i] == 0x37)
            {
                baiCount++;
            }
        }

        int count = MIN(MIN(zhongCount, faCount), baiCount);
        return count;
    }

}

//////////////////////////////////////////////////////////////////////////
