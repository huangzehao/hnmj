#include "MJTools.h"
#include "HNMJGameLogic.h"
#include "Game/GameLib.h"
void HNMJ::MJTools::setCardImagic(cocos2d::Node * pNode, int kValue, std::string kImagicFront)
{
	if (kValue > 0)
	{
		BYTE cbValue = HNMJ::CGameLogic::Instance().GetCardValue(kValue);
		BYTE cbColor = HNMJ::CGameLogic::Instance().GetCardColor(kValue);
		WidgetFun::setImagic(pNode, utility::toString(kImagicFront, (int)cbColor, (int)cbValue, ".png"), false);
	}
	else
	{
		WidgetFun::setImagic(pNode, utility::toString(kImagicFront, 0, 0, ".png"), false);
	}
}

void HNMJ::MJTools::setMagicCardImagic(cocos2d::Node * cardSprite, int kValue)
{
	MJTools::setCardImagic(cardSprite, kValue, "GameHNMJ/2/handmah_");
}
