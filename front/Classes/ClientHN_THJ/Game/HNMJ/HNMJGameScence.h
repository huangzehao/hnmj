#pragma once
#include "cocos2d.h"
#include "ui/UIButton.h"
#include "Game/FV/FvSingleton.h"
#include "Game/Widget/WidgetDefine.h"
#include "Game/Game/GameBase.h"
#include "Game/Game/GameDefine.h"
#include "Game/Script/TimeNode.h"
#include "ClientHN_THJ/Game/HNMJ/CMD_HNMJ.h"
#include "Voice/YvVoiceManager.hpp"
#include "ui/UIText.h"


class HNMJPlayer;
struct CMD_S_GameEnd;
struct  CMD_WeaveItem;
struct  HNMJGameRecord;

class HNMJGameScence
	:public GameBase
	,public TimeNode
	,public FvSingleton<HNMJGameScence>
    //,public YVSDK::YVListern::YVUpLoadFileListern
{
public:
	const static int KIND_ID = 310;
	const static int VERSION_SERVER	= PROCESS_VERSION(6,0,3);				//≥Ã–Ú∞Ê±æ
	const static int VERSION_CLIENT	= PROCESS_VERSION(7,1,3);
	const static int MAX_PLAYER = 4;

	enum 
	{
		HNMJ_STATE_NULL,
		HNMJ_STATE_READY,
		HNMJ_STATE_XIAO_HU,
		HNMJ_STATE_PLAYING,
	};

	enum 
	{
		GAME_OPTION_TYPE_HN = 1,
		GAME_OPTION_TYPE_HZ,
		GAME_OPTION_TYPE_THJ,
	};
	enum 
	{
		GAME_OPTION_RULE_AUTO_CARD = 1,
		GAME_OPTION_RULE_SHOW_WIN_LOSE1,
		GAME_OPTION_RULE_AUTO_HU ,
		GAME_OPTION_RULE_SHOW_WIN_LOSE_ZERO,

		GAME_OPTION_RULE_HIDE_GAMETYPE,
		GAME_OPTION_RULE_HIDE_0,
		GAME_OPTION_RULE_HIDE_1,
		GAME_OPTION_RULE_HIDE_2,
		GAME_OPTION_RULE_HIDE_3,
		GAME_OPTION_RULE_HIDE_4,
		GAME_OPTION_RULE_HIDE_5,
		GAME_OPTION_RULE_HIDE_6,
		GAME_OPTION_RULE_HIDE_7,
		GAME_OPTION_RULE_HIDE_8,
		GAME_OPTION_RULE_HIDE_9,
		GAME_OPTION_RULE_HIDE_10,

		GAME_OPTION_RULE_HIDE_GUO,
		GAME_OPTION_RULE_PRIVATAEND_RETURN_HOME,

		GAME_OPTION_RULE_SHOW_TING_CARD,
		GAME_OPTION_RULE_USERID_ADD,
		GAME_OPTION_RULE_NIAO_DIRECT,
		GAME_OPTION_RULE_NIAO_ANIM_2,
	};
public:
	HNMJGameScence();
	HNMJGameScence(int iGameType,int iOption);
	~HNMJGameScence();
public:
	virtual bool init();

	void initPrivate();
	virtual bool IsInGame();
    //virtual void onUpLoadFileListern(YVSDK::UpLoadFileRespond*);
public:
	void EnterScence();
	void HideAll();
	void defaultState();
	bool isHZ();
	bool isTHJ();
	void defaultPlayerActionState();
	void showSaiZi(unsigned int iValue);
    void updatePlayerDistanceText();
    void setPlayerDistanceString(int id, double dis);
    double getTwoPlayerDistance(GamePlayer* player_0, GamePlayer* player_1);
	std::string getStringHuRight(dword kValue);
	std::string getStringGang(int nChairID,CMD_WeaveItem* pWeave,int iWeaveCout);
	void remmoveAutoAction();
	void AutoCard();
	void HuPaiAutoAction();
	void OnPlayWaring();
	void removeWaringSound();
	void upSelfFreeReadState();
    void setCurrentPlayer(int iCurrentPlayer, DWORD iUserAction, int nActionCard = 0);
    //void updatePiziMagicGangButton(int iCurrentPlayer, DWORD iUserAction);
    void updateOperateGangData(CMD_S_OperateNotify * pOperateNotify);
    void updateOperateGangData(CMD_S_SendCard * pOperateNotify);
    void setCurrentPlayerGang(int iCurrentPlayer,DWORD iUserAction,int nActionCard, int nGangCount, BYTE* cardDataGang);
	void checkActionBtns(DWORD iUserAction,int nActionCard);
	void checkActionGangBtns(DWORD nUserAction,int nActionCard, int nGangCount, BYTE* cardDataGang);
	//void recordGangAction(DWORD nUserAction);
	void checkActionBtn(bool icheck,const std::string&BtnName,int nCard,cocos2d::Vec2& kPos);
	void checkActionBtnGang(bool bcheck,const std::string& BtnName,int nCard, cocos2d::Vec2& kPos, int nGangCount, BYTE* cardDataGang);
	void checkActionBtnZFBGang(bool bcheck, const std::string& BtnName, int nCard, cocos2d::Vec2& kPos, int nGangCount, BYTE* cardDataGang);
	void setActionBtnCard(cocos2d::Node* pActionBtnCard,int nCard,std::string kImagicFront);
    void setCCBtnCardImagic(cocos2d::ui::Button* pActionBtnCard, int nCard, std::string kImagicFront);
	void showJieSuanInfo(CMD_S_GameEnd* pGameEnd);
	int getJieSuanHuScore(CMD_S_GameEnd* pGameEnd, int nChairID);
	void setJieSuanNiaoCard(cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum);
	void setGameEndNiaoCardAni1(cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum,float& delayTime);
	void setGameEndNiaoCardAni2(cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum,float& delayTime);

	void showChiCheckNode(cocos2d::Node* pCheckNode, BYTE cbAction,BYTE cbCard,int nChiCount);
    void showGangCheckNode(cocos2d::Node* pCheckNode, WidgetUserInfo* pUserInfo);
	void sendOperateCMD(int nCode,int nCard);
public:
	void initButton();
    void HNMJButton_Ready(cocos2d::Ref*, WidgetUserInfo*);
    bool canDoReady();
    void HNMJButton_UnReady(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_GuoAction(cocos2d::Ref*,WidgetUserInfo*);
    void HNMJButton_GangAction(cocos2d::Ref*, WidgetUserInfo*);
    void HNMJButton_LiangAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_PengAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_ChiAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_HuAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_XiaoHuAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_GangCS(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_BuAction(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_ChiCardList(cocos2d::Ref*,WidgetUserInfo*);
    void HNMJButton_GangCardList(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_QuXiaoTuoGuan(cocos2d::Ref*,WidgetUserInfo*);
	
	void HNMJButtonAction_ShowNext(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButtonAction_ShowCard(cocos2d::Ref*,WidgetUserInfo*);

	void HNMJButton_JieSuanShare(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_JieSuanStart(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_JieSuanClose(cocos2d::Ref*,WidgetUserInfo*);

	void HNMJButton_TalkBegin(cocos2d::Ref*,WidgetUserInfo*);
	void endButtonTalk();
	void HNMJButton_TalkEnd(cocos2d::Ref*,WidgetUserInfo*);
    void HNMJButton_Msg(cocos2d::Ref*,WidgetUserInfo*);
    void HNMJButton_ShowSheZhi(cocos2d::Ref*, WidgetUserInfo*);
private:
	virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
	void OnFreeScence(void* data, int dataSize);
	void OnPlayScence(void* data, int dataSize);

	void updateMagicCardView();
public:
	HNMJPlayer* getPlayerByChairID(int iChairID);
	virtual GamePlayer* CreatePlayer(IClientUserItem * pIClientUserItem);
	virtual void DeletePlayer(GamePlayer* pPlayer);
	virtual void upSelfPlayerInfo();
	virtual void OnGFGameClose(int iExitCode);
	virtual void OnEventUserStatus(GamePlayer * pPlayer);
	virtual bool BackKey();
	//ÀΩ»À≥°
public:
	void defaultPrivateState();
	virtual void OnSocketSubPrivateRoomInfo(CMD_GF_Private_Room_Info* pNetInfo);
	virtual void OnSocketSubPrivateEnd(CMD_GF_Private_End_Info* pNetInfo);
	virtual void OnSocketSubPrivateDismissInfo(CMD_GF_Private_Dismiss_Info* pNetInfo);
	void Button_WeiXinFriend(cocos2d::Ref*,WidgetUserInfo*);
	void Button_WeiXinImagic(cocos2d::Ref*,WidgetUserInfo*);
	void ButtonPlayerHeadClick(cocos2d::Ref*,WidgetUserInfo* pUserInfo);
public:
	void initNet();

	void OnSubGameStart(const void * pBuffer, word wDataSize);
	//”√ªß≥ˆ≈∆
	void OnSubOutCard(const void * pBuffer, WORD wDataSize);
	//”√ªß≥ˆ≈∆
	void OnSubOutCardCSGang(const void * pBuffer, WORD wDataSize);
	//∑¢≈∆œ˚œ¢
	void OnSubSendCard(const void * pBuffer, WORD wDataSize);
	//≥§…≥∏‹≤π≈∆
	void OnSubSendCard_CSGang(const void * pBuffer, WORD wDataSize);
	//≤Ÿ◊˜Ã· æ
	void OnSubOperateNotify(const void * pBuffer, WORD wDataSize);
	//≤Ÿ◊˜Ω·π˚
	void OnSubOperateResult(const void * pBuffer, WORD wDataSize);
	//”Œœ∑Ω· ¯
	void OnSubGameEnd(const void * pBuffer, WORD wDataSize);
	// £”‡≈∆
	void OnSubGameEndLastCard(const void * pBuffer, WORD wDataSize);
	//”√ªßÕ–π‹
	void OnSubTrustee(const void * pBuffer,WORD wDataSize);
	//≥‘∫˙œ˚œ¢
	void OnSubUserChiHu( const void *pBuffer,WORD wDataSize );
	//∏‹µ√∑÷
	void OnSubGangScore( const void *pBuffer, WORD wDataSize );
	void OnSubXiaoHu(const void *pBuffer, WORD wDataSize);
public:
	void Command_PlaceBet(int iArea,int iBetScore);
	void SendOutCard(cocos2d::Node* pCard);
public:
	void initTouch();
	bool ccTouchBegan(cocos2d::Vec2 kPos);
	void ccTouchMoved(cocos2d::Vec2 kPos);
	void ccTouchEnded(cocos2d::Vec2 kPos);

public:
	void setGameState(int nState);
	int getGameState();
	void setPlayCount(int nCount);
	int getPlayCount();
	void getGameRuleStr(std::vector<std::string>& kRuleList,dword	dwGameRuleIdex);
	void setPrivateInfo(CMD_GF_Private_Room_Info* pNetInfo);
	void setOutCardWaiteTime(int iOutCardWaiteTime);
public:
	void initRecord();
	void defaultRecordState();
	bool StartRecord(datastream kDataStream);
	void NextRecordAction();
	virtual void onGPAccountInfoHttpIP(dword dwUserID, std::string strIP,std::string strHttp);

	void Button_GameRecordPlay(cocos2d::Ref*,WidgetUserInfo*);
	void Button_GameRecordPase(cocos2d::Ref*,WidgetUserInfo*);
	void Button_GameRecordLeft(cocos2d::Ref*,WidgetUserInfo*);
	void Button_GameRecordRight(cocos2d::Ref*,WidgetUserInfo*);
	void Button_GameRecordExit(cocos2d::Ref*,WidgetUserInfo*);
public:
	void OnMasterHandCard( const void *pBuffer, WORD wDataSize );
	void OnMasterLeftCard( const void *pBuffer, WORD wDataSize );
	void updateUserRight();
	void showMater();
	void initMaster();
	void defaultMaster(bool bRestZhaNiao = false);
	void setMasterCheakCard(BYTE cbCard);
	void HNMJButton_Master(cocos2d::Ref*,WidgetUserInfo*);
	void Button_MasterClose(cocos2d::Ref*,WidgetUserInfo*);
	void NHMJ_MASTER_LEFTCARD(cocos2d::Ref*,WidgetUserInfo*);
	void MasterZhaNiao(cocos2d::Ref*,WidgetUserInfo*);
	void Button_TalkDefine(cocos2d::Ref*,WidgetUserInfo*);
	void Button_Send_TalkStr(cocos2d::Ref*,WidgetUserInfo*);
	void HNMJButton_BiaoQing(cocos2d::Ref*,WidgetUserInfo*);
public:
	void AutoOutCSGangCard();
	bool OnXiaoHuEnd(int nChairID);
public:
	void initSetButton();
	void HNMJ_Button_Hua(cocos2d::Ref*,WidgetUserInfo*);
	void SetBarSider1(cocos2d::Ref*,WidgetUserInfo*);
	void SetBarSider2(cocos2d::Ref*,WidgetUserInfo*);
	void Button_HideSet(cocos2d::Ref*,WidgetUserInfo*);
	void Button_GameSet(cocos2d::Ref* pNode,WidgetUserInfo* pUserInfo);
public:
	static void pushGameRule(std::vector<std::string>&kRuleList, dword dwGameRule,int nRuleTag,bool bShow);
protected:
	int							m_iBankerUser;						//◊Øº“”√ªß
	int							m_iCurrentUser;						//µ±«∞”√ªß
	int							m_iUserAction;						//ÕÊº“∂Ø◊˜
	BYTE						m_cbMagicCardData;					//癞子牌

	cocos2d::Sprite*			m_spMagicCard;

	HNMJPlayer*					m_pLocal;
	HNMJPlayer*					m_pPlayer[MAX_PLAYER];

	cocos2d::Node*				m_pTouchCardNode;
	cocos2d::Vec2				m_kTouchSrcPos;

	int                         m_nGameState;
	int                         m_nPlayCount;

	word						m_wRecordSelfChairID;
	int							m_iActRecordIdex;
	int							m_RecordTimeID;
	float						m_RecordTime;
	HNMJGameRecord*				m_pGameRecord;

	int							m_iOutCardWaiteTime;
	CMD_GF_Private_Room_Info    m_kRoomInfo;

	int							m_kCSGangSendCard1;
	int							m_kCSGangSendCard2;
	//DWORD                       m_gang_type;

    std::vector<cocos2d::ui::Text*> playerDistanceNode_;
    std::map<int, double> allPlayerDistance_;
    cocos2d::Node *distanceCsbNode_;
    cocos2d::Layer* pTouchScence_;
    //cocos2d::Node *gangPiziMagicNode_;
    //cocos2d::ui::Button* zhong_gang_btn_;
    //cocos2d::ui::Button* fa_gang_btn_;
    //cocos2d::ui::Button* laizi_gang_btn_;
    //std::vector<int> gang_data_;
};