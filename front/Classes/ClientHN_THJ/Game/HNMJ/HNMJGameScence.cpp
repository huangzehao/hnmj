#include "HNMJGameScence.h"
#include "HNMJPlayer.h"
#include "Game/GameLib.h"
#include "CMD_HNMJ.h"
#include "HNMJPlayer.h"
#include "HNMJGameLogic.h"
#include "HNMJSoundFun.h"
#include "Game/Script/ScriptXMLparse.h"
#include"cocostudio/CocoStudio.h"
#include "JniCross/JniFun.h"
#include "ClientHN_THJ/Scene/HNSetScence.h"

FV_SINGLETON_STORAGE(HNMJGameScence);

HNMJGameScence::HNMJGameScence()
	:m_iBankerUser(0)
	,m_iCurrentUser(0)
	,m_iUserAction(0)
	,m_pLocal(NULL)
	,m_pTouchCardNode(NULL)
	,m_nPlayCount(0)
	,m_pGameRecord(NULL)
	,m_wRecordSelfChairID(0)
	,m_RecordTimeID(0)
	,m_RecordTime(0)
	,GameBase(0,0)
	,m_iOutCardWaiteTime(30)
	, m_cbMagicCardData(0)
	, m_spMagicCard(nullptr)
{
	CCAssert(false,"");
}
HNMJGameScence::HNMJGameScence(int iGameType,int iOption)
	:m_iBankerUser(0)
	,m_iCurrentUser(0)
	,m_iUserAction(0)
	,m_pLocal(NULL)
	,m_pTouchCardNode(NULL)
	,m_nPlayCount(0)
	,m_pGameRecord(NULL)
	,m_wRecordSelfChairID(0)
	,m_RecordTimeID(0)
	,m_RecordTime(0)
	,GameBase(iGameType,iOption)
	,m_iOutCardWaiteTime(30)
	, m_cbMagicCardData(0)
	, m_spMagicCard(nullptr)
{
    for (int i = 0; i<MAX_PLAYER; i++)
    {
        m_pPlayer[i] = nullptr;
    }
	init();
}

HNMJGameScence::~HNMJGameScence()
{

}
bool HNMJGameScence::init()
{
	if (!GameBase::init())
	{
		return false;
	};
	WidgetScenceXMLparse kScence1("GameHNMJ/xml/GameScence.xml",this);
	cocos2d::ScriptXMLparse kScriptXml1("GameHNMJ/xml/ScriptValueStr.xml");

	cocos2d::Node* pNode = WidgetFun::getChildWidget(this,"GameTypeScenceNode");
	if (isHZ())
	{
		WidgetScenceXMLparse kScence2("GameHNMJ/xml/GameScence_HZ.xml",pNode);
		WidgetScenceXMLparse kScence3("GameHNMJ/xml/GameSetScence.xml",pNode);
	}
	else if(isTHJ())
	{
		WidgetScenceXMLparse kScence2("GameHNMJ/xml/GameScence_THJ.xml",pNode);
		WidgetScenceXMLparse kScence3("GameHNMJ/xml/GameSetScence.xml",pNode);
	}
	else
	{
		WidgetScenceXMLparse kScence2("GameHNMJ/xml/GameScence_HN.xml",pNode);
	}

    if (WidgetFun::getChildWidget(this, "GameTalkPlane"))
    {
        WidgetFun::getChildWidget(this, "GameTalkPlane")->setLocalZOrder(5);
    }

	if (WidgetFun::getChildWidget(this,"GameTalkList"))
	{
		 cocos2d::ListViewEx* pTalkList = WidgetFun::getListViewWidget(this,"GameTalkList");
		 pTalkList->removeAllChildren();
		 for (int i=0;i<10;i++)
		 {
		 	cocos2d::Node* pItem = WidgetManager::Instance().createWidget("HNMJGameTalkListItem",pTalkList);
		 	std::string kTxt = utility::getScriptString(utility::toString("GameTalkTxt",i));
		 	WidgetFun::setText(pItem,"TalkListItemTxt",kTxt);
		 	WidgetFun::setWidgetUserInfo(pItem,"Button_TalkDefine","Idex",utility::toString(i));
		 	WidgetFun::setWidgetUserInfo(pItem,"Button_TalkDefine","Txt",utility::toString(kTxt));
		 }
		 pTalkList->forceDoLayout();

		 for (int m = 0;m<6;m++)
		 {
		 	cocos2d::Node* pNode = WidgetFun::getChildWidget(this,utility::toString("BiaoQing",m));
		 	for (int n = 0;n<8;n++)
		 	{
		 		std::string kFileName = utility::toString("GameHNMJ/Talk/EE",m*8+n,".png");
		 		WidgetFun::setButtonImagic(pNode,utility::toString("HNMJButton_BiaoQing",n),kFileName,true);
		 		WidgetFun::setWidgetUserInfo(pNode,utility::toString("HNMJButton_BiaoQing",n),"File",kFileName);
		 	}
		 }
	}

	initPrivate();

    cocos2d::Node *csbNode = cocos2d::CSLoader::createNode("GameHNMJ/PlayerDistanceNode.csb");
    distanceCsbNode_ = csbNode;
    csbNode->setPosition(400, 500);
    csbNode->setScale(0.70f);
    pNode->addChild(csbNode);
    for (int i = 0; i < 6; i++)
    {
        auto* text = dynamic_cast<cocos2d::ui::Text*>(csbNode->getChildByName(cocos2d::StringUtils::format("Text_dis_%d", i)));
        text->setString("");
        playerDistanceNode_.push_back(text);
        allPlayerDistance_[i] = 0.000f;;
    }


	for (int i = 0;i<MAX_PLAYER;i++)
	{
		m_pPlayer[i] = new HNMJPlayer(i,WidgetFun::getChildWidget(this,utility::toString("Player",i)));
	}
	m_pLocal = m_pPlayer[0];

	WidgetManager::addButtonCB("HNMJButton_GameExit",this,button_selector(GameBase::Button_ExitGameBase));

    WidgetManager::addButtonCB("HNMJButton_SheZhi", this, button_selector(HNMJGameScence::HNMJButton_ShowSheZhi));


	m_spMagicCard = cocos2d::Sprite::create();
	m_spMagicCard->setScale(0.5f);
	WidgetFun::getChildWidget(this, "GameHNMJPlane")->addChild(m_spMagicCard);
	m_spMagicCard->setPosition(cocos2d::Vec2(190.0f, 660.0f));

    //gangPiziMagicNode_ = cocos2d::CSLoader::getInstance()->createNode("GameHNMJ/GangPiziMagicNode.csb");
    //WidgetFun::getChildWidget(this, "SelfActionNode")->addChild(gangPiziMagicNode_);
    //gangPiziMagicNode_->setVisible(false);
    //gangPiziMagicNode_->setPosition(0, 100);
    //gangPiziMagicNode_->setScale(0.5);

    //zhong_gang_btn_ = dynamic_cast<cocos2d::ui::Button*>(gangPiziMagicNode_->getChildByName("Button_zhong"));
    //zhong_gang_btn_->addClickEventListener([=](cocos2d::Ref*) {
    //    sendOperateCMD(WIK_GANG, 0x35);
    //    gangPiziMagicNode_->setVisible(false);
    //});
    //fa_gang_btn_ = dynamic_cast<cocos2d::ui::Button*>(gangPiziMagicNode_->getChildByName("Button_fa"));
    //fa_gang_btn_->addClickEventListener([=](cocos2d::Ref*) {
    //    sendOperateCMD(WIK_GANG, 0x36);
    //    gangPiziMagicNode_->setVisible(false);
    //});
    //laizi_gang_btn_ = dynamic_cast<cocos2d::ui::Button*>(gangPiziMagicNode_->getChildByName("Button_laizi"));
    //laizi_gang_btn_->addClickEventListener([=](cocos2d::Ref*) {
    //    sendOperateCMD(WIK_GANG, m_cbMagicCardData);
    //    gangPiziMagicNode_->setVisible(false);
    //});

	initButton();
	initNet();
	initTouch();
	initSetButton();

	return true;
}

void HNMJGameScence::HNMJButton_ShowSheZhi(cocos2d::Ref *, WidgetUserInfo *)
{
    HNSetScence::Instance().Button_SheZhi(nullptr, nullptr);
    HNSetScence::Instance().setButtonSetChangeAccountVisible(false);
}

void HNMJGameScence::EnterScence()
{
    this->sendSelfLocation();

	for (int i = 0;i<GAME_PLAYER;i++)
	{
		m_pPlayer[i]->PlayerLeave();
	}
	WidgetFun::setVisible(this,"PrivateInfo",false);
	WidgetFun::setVisible(this,"PrivateEndPlane",false);
	WidgetFun::setVisible(this,"GameHNMJPlane",true);
	WidgetFun::setVisible(this,"GoldRoomInfo",true);
	defaultState();
	HNMJSoundFun::playBackMusic("csmj.mp3");

}

bool HNMJGameScence::IsInGame()
{
	return WidgetFun::isWidgetVisble(this,"GameHNMJPlane");
}
void HNMJGameScence::HideAll()
{
	WidgetFun::setVisible(this,"GameHNMJPlane",false);
	defaultPrivateState();
	defaultRecordState();
	defaultMaster();
	removeWaringSound();
}
void HNMJGameScence::defaultState()
{
	m_pTouchCardNode = NULL;

	for (int i = 0;i<MAX_PLAYER;i++)
	{
		m_pPlayer[i]->defaultState();
		m_pPlayer[i]->EndGame();
	}

	if (WidgetFun::getChildWidget(this,"GameSetPlane"))
	{
		WidgetFun::setVisible(this,"GameSetPlane",false);
	}
	WidgetFun::setVisible(this,"TimeNode",false);
	WidgetFun::setVisible(this,"LastCardNode",false);
	WidgetFun::setVisible(this,"SaiZiNode",false);
	WidgetFun::setVisible(this,"SelfActionNode",false);
	WidgetFun::setVisible(this,"GameJieSuanNode",false);
	WidgetFun::setVisible(this,"FreeStateNode",false);
	WidgetFun::setVisible(this,"GameEndNiaoCardAni",false);
	WidgetFun::setVisible(this,"TuoGuanStateNode",false);

	defaultPrivateState();
	defaultPlayerActionState();
	defaultRecordState();
	defaultMaster(true);
	remmoveAutoAction();
}
bool HNMJGameScence::isHZ()
{
	return m_iGameType == GAME_OPTION_TYPE_HZ;
}
bool HNMJGameScence::isTHJ()
{
	return m_iGameType == GAME_OPTION_TYPE_THJ;
}
void HNMJGameScence::defaultPlayerActionState()
{
	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"TimeNode");
	for (int i = 0;i<MAX_PLAYER;i++)
	{
		WidgetFun::setVisible(pRootNode,utility::toString("TimePoint",i),false);
	}
	WidgetFun::getChildWidget(pRootNode,"ActPlayerLastTime")->stopAllActions();
	WidgetFun::setText(pRootNode,"ActPlayerLastTime","0");
    //gangPiziMagicNode_->setVisible(false);
}
std::string HNMJGameScence::getStringHuRight(dword kValue)
{
	dword dwRight[] = {
		CHR_PENGPENG_HU		,							//ÅöÅöºú
		CHR_JIANGJIANG_HU		,						//½«½«ºú
		CHR_QING_YI_SE		,							//ÇåÒ»É«
		CHR_HAI_DI_LAO		,							//º£µ×ÀÌ
		CHR_HAI_DI_PAO		,							//º£µ×ÀÌ
		CHR_QI_XIAO_DUI		,							//ÆßÐ¡¶Ô
		CHR_HAOHUA_QI_XIAO_DUI	,						//ºÀ»ªÆßÐ¡¶Ô
		CHR_GANG_KAI			,						//¸ÜÉÏ¿ª»¨
		CHR_QIANG_GANG_HU		,						//ÇÀ¸Üºú
		CHR_GANG_SHANG_PAO	,							//¸ÜÉÏÅÜ
		CHR_QUAN_QIU_REN		,						//È«ÇóÈË
		CHR_XIAO_DA_SI_XI	,							//´óËÄÏ²
		CHR_XIAO_BAN_BAN_HU		,						//°å°åºú
		CHR_XIAO_QUE_YI_SE		,						//È±Ò»É«
		CHR_XIAO_LIU_LIU_SHUN	,						//ÁùÁùË³
		CHR_SHUANG_HAOHUA_QI_XIAO_DUI	,				//Ë«ºÀ»ªÆßÐ¡¶Ô
		CHR_HONGZHONG_WU,								//ÎÞºìÖÐ¶à¼ÓÒ»Äñ
		CHR_TIAN_HU		,						    	//4ºìÖÐÌìºú
        CHR_SAN_HAOHUA_QI_XIAO_DUI,								//三豪华7对
	};
	std::string pszRight[] = {
		ScriptData<std::string>("HuRight0").Value(),
		ScriptData<std::string>("HuRight1").Value(),
		ScriptData<std::string>("HuRight2").Value(),
		ScriptData<std::string>("HuRight3").Value(),
		ScriptData<std::string>("HuRight4").Value(),
		ScriptData<std::string>("HuRight5").Value(),
		ScriptData<std::string>("HuRight6").Value(),
		ScriptData<std::string>("HuRight7").Value(),
		ScriptData<std::string>("HuRight8").Value(),
		ScriptData<std::string>("HuRight9").Value(),
		ScriptData<std::string>("HuRight10").Value(),
		ScriptData<std::string>("HuRight11").Value(),
		ScriptData<std::string>("HuRight12").Value(),
		ScriptData<std::string>("HuRight13").Value(),
		ScriptData<std::string>("HuRight14").Value(),
		ScriptData<std::string>("HuRight15").Value(),
		ScriptData<std::string>("HuRight16").Value(),
		ScriptData<std::string>("HuRight17").Value(),
		ScriptData<std::string>("HuRight18").Value(),
	};
	std::string kTxt;
	HNMJ::CChiHuRight	kChiHuRight;	
	kChiHuRight.SetRightData(&kValue,MAX_RIGHT_COUNT );
	for( BYTE j = 0; j < CountArray(pszRight); j++ )
	{
		if( !(kChiHuRight&dwRight[j]).IsEmpty() )
		{
			kTxt+=pszRight[j];
		}
	}
	return kTxt;
}

std::string HNMJGameScence::getStringGang(int nChairID,CMD_WeaveItem* pWeave,int iWeaveCout )
{
	std::string kStr;
	for (int i = 0;i<iWeaveCout;i++)
	{
		CMD_WeaveItem* pTemp = pWeave+i;
		if (pTemp->cbWeaveKind == WIK_GANG|| 
			pTemp->cbWeaveKind == WIK_PIZI_GANG ||
			pTemp->cbWeaveKind == WIK_PIZI_ZFB_GANG ||
			pTemp->cbWeaveKind == WIK_CS_GANG ||
			pTemp->cbWeaveKind == WIK_BU_ZHANG)
		{
			if ( pTemp->wProvideUser == nChairID && pTemp->cbPublicCard == 0)
			{
				kStr +=ScriptData<std::string>("AnGang").Value();
			}
			else if (pTemp->wProvideUser == nChairID && pTemp->cbPublicCard != 0)
			{
				kStr +=ScriptData<std::string>("ZiMoMingGang").Value();
			}
			else
			{
				kStr +=ScriptData<std::string>("MingGang").Value();
			}
		}
	
	}
	return kStr;
}


void HNMJGameScence::showSaiZi(unsigned int iValue)
{
	word wSice1 = word(iValue>>16);
	word wSice2 = word(iValue);
	if (wSice1 > 0)
	{
		BYTE SiceFirst = (wSice1 >> 8);
		BYTE SiceSecond = (wSice1);
		std::string kImagic = WidgetFun::getWidgetUserInfo(this,"SaiZiNode","Imagic");
		WidgetFun::setImagic(this,"SaiZi0",utility::toString(kImagic,(int)SiceFirst,".png"));
		WidgetFun::setImagic(this,"SaiZi1",utility::toString(kImagic,(int)SiceSecond,".png"));
		WidgetFun::setWidgetUserInfo(this,"SaiZiNode","NextValue",utility::toString((int)wSice2));
		WidgetFun::runWidgetAction(this,"SaiZiNode","ActionStart1");
	}
	else if (wSice2 > 0)
	{
		BYTE SiceFirst = (wSice2 >> 8);
		BYTE SiceSecond = (wSice2);
		std::string kImagic = WidgetFun::getWidgetUserInfo(this,"SaiZiNode","Imagic");
		WidgetFun::setImagic(this,"SaiZi0",utility::toString(kImagic,(int)SiceFirst,".png"));
		WidgetFun::setImagic(this,"SaiZi1",utility::toString(kImagic,(int)SiceSecond,".png"));
		WidgetFun::runWidgetAction(this,"SaiZiNode","ActionStart2");
		getPlayerByChairID(m_iBankerUser)->setZhuang();
	}
}
void HNMJGameScence::updatePlayerDistanceText()
{
    distanceCsbNode_->setVisible(true);

    //double dis_0 = this->getTwoPlayerDistance(this->getPlayerByChairID(1), this->getPlayerByChairID(2));
    //this->setPlayerDistanceString(0, dis_0);
    //double dis_1 = this->getTwoPlayerDistance(this->getPlayerByChairID(1), this->getPlayerByChairID(3));
    //this->setPlayerDistanceString(1, dis_1);
    //double dis_2 = this->getTwoPlayerDistance(this->getPlayerByChairID(0), this->getPlayerByChairID(2));
    //this->setPlayerDistanceString(2, dis_2);
    //double dis_3 = this->getTwoPlayerDistance(this->getPlayerByChairID(0), this->getPlayerByChairID(3));
    //this->setPlayerDistanceString(3, dis_3);
    //double dis_4 = this->getTwoPlayerDistance(this->getPlayerByChairID(0), this->getPlayerByChairID(1));
    //this->setPlayerDistanceString(4, dis_4);
    //double dis_5 = this->getTwoPlayerDistance(this->getPlayerByChairID(2), this->getPlayerByChairID(3));
    //this->setPlayerDistanceString(5, dis_5);

    double dis_0 = this->getTwoPlayerDistance(m_pPlayer[1], m_pPlayer[2]);
    this->setPlayerDistanceString(0, dis_0);

    double dis_1 = this->getTwoPlayerDistance(m_pPlayer[1], m_pPlayer[3]);
    this->setPlayerDistanceString(1, dis_1);

    double dis_2 = this->getTwoPlayerDistance(m_pPlayer[0], m_pPlayer[2]);
    this->setPlayerDistanceString(2, dis_2);

    double dis_3 = this->getTwoPlayerDistance(m_pPlayer[0], m_pPlayer[3]);
    this->setPlayerDistanceString(3, dis_3);

    double dis_4 = this->getTwoPlayerDistance(m_pPlayer[0], m_pPlayer[1]);
    this->setPlayerDistanceString(4, dis_4);

    double dis_5 = this->getTwoPlayerDistance(m_pPlayer[2], m_pPlayer[3]);
    this->setPlayerDistanceString(5, dis_5);

}

void HNMJGameScence::setPlayerDistanceString(int id, double dis)
{
    if (0 > id || id > 5)
    {
        return;
    }
    if (dis <= 0.0f)
    {
        playerDistanceNode_.at(id)->setString("");
        return;
    }
    allPlayerDistance_[id] = dis;
    playerDistanceNode_.at(id)->setString(cocos2d::StringUtils::format("%.3f km", dis / 1000.000f));
}

double HNMJGameScence::getTwoPlayerDistance(GamePlayer * player_0, GamePlayer * player_1)
{
    if (nullptr == player_0 || nullptr == player_1)
    {
        return -1;
    }

    if (nullptr == player_0->GetUserInfo() || nullptr == player_1->GetUserInfo())
    {
        return -1;
    }

    if (-1 == player_0->GetUserInfo()->lLatiude || -1 == player_1->GetUserInfo()->lLongitude)
    {
        return -1;
    }

    if (0.000000f == player_0->GetUserInfo()->lLatiude || 0.000000f == player_1->GetUserInfo()->lLongitude)
    {
        return 0.000f;
    }

    double dis = JniFun::getDistanceToSelf(
        player_0->GetUserInfo()->lLatiude / 10000.f, player_0->GetUserInfo()->lLongitude / 10000.f,
        player_1->GetUserInfo()->lLatiude / 10000.f, player_1->GetUserInfo()->lLongitude / 10000.f);
    return dis;
}
void HNMJGameScence::remmoveAutoAction()
{
	TimeManager::Instance().removeByFun(TIME_CALLBACK(HNMJGameScence::HuPaiAutoAction,this));
	TimeManager::Instance().removeByFun(TIME_CALLBACK(HNMJGameScence::AutoCard,this));
}
void HNMJGameScence::AutoCard()
{
	if (!HaveOptionRule(GAME_OPTION_RULE_AUTO_CARD))
	{
		return;
	}
	if (m_iCurrentUser != m_pLocal->GetChairID())
	{
		return;
	}
	if (WidgetFun::isWidgetVisble(this,"SelfActionNode"))
	{
		HNMJButton_GuoAction(NULL,NULL);
		return;
	}
	CMD_C_OutCard OutCard;
	OutCard.cbCardData = m_pLocal->getNewInCard();
	SendSocketData(SUB_C_OUT_CARD,&OutCard,sizeof(OutCard));
}
void HNMJGameScence::HuPaiAutoAction()
{
	HNMJButton_HuAction(NULL,NULL);
}
void HNMJGameScence::OnPlayWaring()
{
	HNMJSoundFun::playEffectEX("warning.mp3");
}
void HNMJGameScence::removeWaringSound()
{
	TimeManager::Instance().removeByFun(TIME_CALLBACK(HNMJGameScence::OnPlayWaring,this));
}
void HNMJGameScence::setCurrentPlayer(int iCurrentPlayer,DWORD iUserAction,int nActionCard/*=0*/)
{
	remmoveAutoAction();
	if (iCurrentPlayer < 0 || iCurrentPlayer > MAX_PLAYER)
	{
		CCASSERT(false,"");
		return;
	}
	defaultPlayerActionState();

	m_iCurrentUser = iCurrentPlayer;
    //updatePiziMagicGangButton(m_iCurrentUser, iUserAction);

	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"TimeNode");
	
	HNMJPlayer* pPlyer = getPlayerByChairID(m_iCurrentUser);
	if (!pPlyer)
	{
		return;
	}
	int iWaiteTime = TIME_OPERATE_CARD;
	if (iUserAction == WIK_NULL)
	{
		iWaiteTime = m_iOutCardWaiteTime;
		WidgetFun::setVisible(pRootNode,utility::toString("TimePoint",pPlyer->GetChairID()),true);
	}
	WidgetFun::getChildWidget(pRootNode,"ActPlayerLastTime")->runAction(
		cocos2d::MoveExTxtTime::create(iWaiteTime));
	removeWaringSound();
	TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::OnPlayWaring,this),iWaiteTime-3);
	if(pPlyer == m_pLocal && iUserAction != WIK_NULL)
	{
		WidgetFun::setWidgetUserInfo(this,"NotifyCard",utility::toString(nActionCard));
		checkActionBtns(iUserAction,nActionCard);
	}
	bool bHu = ((iUserAction)&WIK_CHI_HU || (iUserAction)&WIK_ZI_MO);
	if (pPlyer == m_pLocal && HaveOptionRule(GAME_OPTION_RULE_AUTO_HU) && bHu)
	{
		WidgetFun::setVisible(this,"SelfActionNode",false);
		TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::HuPaiAutoAction,this),2);
	}
	if(pPlyer == m_pLocal && HaveOptionRule(GAME_OPTION_RULE_AUTO_CARD) && !bHu)
	{
		TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::AutoCard,this),iWaiteTime);
	}
}
//void HNMJGameScence::updatePiziMagicGangButton(int iCurrentPlayer, DWORD iUserAction)
//{
//    if (m_iCurrentUser != m_pLocal->GetChairID())
//    {
//        gangPiziMagicNode_->setVisible(false);
//        return;
//    }
//    if (iUserAction != WIK_GANG)
//    {
//        gangPiziMagicNode_->setVisible(false);
//        return;
//    }
//
//    gangPiziMagicNode_->setVisible(m_pLocal->hasCardInHand(0x35) || m_pLocal->hasCardInHand(0x36) 
//        || m_pLocal->hasCardInHand(m_cbMagicCardData));
//
//    zhong_gang_btn_->setVisible(m_pLocal->hasCardInHand(0x35));
//    fa_gang_btn_->setVisible(m_pLocal->hasCardInHand(0x36));
//    laizi_gang_btn_->setVisible(m_pLocal->hasCardInHand(m_cbMagicCardData));
//}
void HNMJGameScence::updateOperateGangData(CMD_S_OperateNotify * pOperateNotify)
{
    if (m_iCurrentUser != m_pLocal->GetChairID())
    {
        return;
    }

    if (m_pLocal->hasCardInHand(0x35))
    {
        pOperateNotify->cbCardDataGang[pOperateNotify->cbGangCount++] = 0x35;
    }
    if (m_pLocal->hasCardInHand(0x36))
    {
        pOperateNotify->cbCardDataGang[pOperateNotify->cbGangCount++] = 0x36;
    }
    if (m_pLocal->hasCardInHand(m_cbMagicCardData))
    {
        pOperateNotify->cbCardDataGang[pOperateNotify->cbGangCount++] = m_cbMagicCardData;
    }
}
void HNMJGameScence::updateOperateGangData(CMD_S_SendCard * cmd_cata)
{
    if (cmd_cata->wCurrentUser != m_pLocal->GetChairID())
    {
        return;
    }

    if (m_pLocal->hasCardInHand(0x35))
    {
        cmd_cata->cbCardDataGang[cmd_cata->cbGangCount++] = 0x35;
    }
    if (m_pLocal->hasCardInHand(0x36))
    {
        cmd_cata->cbCardDataGang[cmd_cata->cbGangCount++] = 0x36;
    }
    if (m_pLocal->hasCardInHand(m_cbMagicCardData))
    {
        cmd_cata->cbCardDataGang[cmd_cata->cbGangCount++] = m_cbMagicCardData;
    }
}
void HNMJGameScence::setCurrentPlayerGang(int iCurrentPlayer,DWORD iUserAction,int nActionCard, int nGangCount, BYTE* cardDataGang)
{
	remmoveAutoAction();
	if (iCurrentPlayer < 0 || iCurrentPlayer > MAX_PLAYER)
	{
		CCASSERT(false,"");
		return;
	}
	defaultPlayerActionState();


	m_iCurrentUser = iCurrentPlayer;
    //updatePiziMagicGangButton(m_iCurrentUser, iUserAction);

	cocos2d::Node* pRootNode = WidgetFun::getChildWidget(this,"TimeNode");
	
	HNMJPlayer* pPlyer = getPlayerByChairID(m_iCurrentUser);
	if (!pPlyer)
	{
		return;
	}
	int iWaiteTime = TIME_OPERATE_CARD;
	if (iUserAction == WIK_NULL)
	{
		iWaiteTime = m_iOutCardWaiteTime;
		WidgetFun::setVisible(pRootNode,utility::toString("TimePoint",pPlyer->GetChairID()),true);
	}
	WidgetFun::getChildWidget(pRootNode,"ActPlayerLastTime")->runAction(cocos2d::MoveExTxtTime::create(iWaiteTime));
	removeWaringSound();
	TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::OnPlayWaring,this),iWaiteTime-3);
	if(pPlyer == m_pLocal && iUserAction != WIK_NULL)
	{
		bool bGang = (((iUserAction)&WIK_GANG) || ((iUserAction)==WIK_PIZI_ZFB_GANG));//||(iUserAction)&WIK_BU_ZHANG||(iUserAction)&WIK_CS_GANG
		if(bGang){
			//if ((iUserAction&WIK_PIZI_ZFB_GANG) != 0)
			//{
			//	m_gang_type = WIK_PIZI_ZFB_GANG;
			//}
			//else if ((iUserAction&WIK_PIZI_GANG) != 0)
			//{
			//	m_gang_type = WIK_PIZI_GANG;
			//}
			//else
			//{
			//	m_gang_type = WIK_GANG;
			//}
			cocos2d::log("杠 true");
			//显示提示
			WidgetFun::setWidgetUserInfo(this,"NotifyCard",utility::toString(nActionCard));
			checkActionGangBtns(iUserAction,nActionCard,nGangCount,cardDataGang);
		}else{
			//显示提示
			WidgetFun::setWidgetUserInfo(this,"NotifyCard",utility::toString(nActionCard));
			checkActionBtns(iUserAction,nActionCard);
		}
	}
	bool bHu = ((iUserAction)&WIK_CHI_HU || (iUserAction)&WIK_ZI_MO);
	if (pPlyer == m_pLocal && HaveOptionRule(GAME_OPTION_RULE_AUTO_HU) && bHu)
	{
		WidgetFun::setVisible(this,"SelfActionNode",false);
		TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::HuPaiAutoAction,this),2);
	}
	if(pPlyer == m_pLocal && HaveOptionRule(GAME_OPTION_RULE_AUTO_CARD) && !bHu)
	{
		TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::AutoCard,this),iWaiteTime);
	}
}
void HNMJGameScence::setActionBtnCard(cocos2d::Node* pActionBtnCard,int nCard,std::string kImagicFront)
{
	if (pActionBtnCard==NULL|| nCard == 0 )
	{
		return;
	}
	BYTE cbValue= HNMJ::CGameLogic::Instance().GetCardValue(nCard);
	BYTE cbColor=HNMJ::CGameLogic::Instance().GetCardColor(nCard);
	WidgetFun::setButtonImagic(pActionBtnCard,utility::toString(kImagicFront,(int)cbColor,(int)cbValue,".png"),true);

}

void HNMJGameScence::setCCBtnCardImagic(cocos2d::ui::Button * pActionBtnCard, int nCard, std::string kImagicFront)
{
    if (pActionBtnCard == NULL || nCard == 0)
    {
        return;
    }
    BYTE cbValue = HNMJ::CGameLogic::Instance().GetCardValue(nCard);
    BYTE cbColor = HNMJ::CGameLogic::Instance().GetCardColor(nCard);
    std::string path = utility::toString(kImagicFront, (int)cbColor, (int)cbValue, ".png");
    pActionBtnCard->loadTextureNormal(path);
    pActionBtnCard->loadTexturePressed(path);
}

void HNMJGameScence::checkActionBtns(DWORD nUserAction,int nActionCard)
{
	DWORD iUserAction = nUserAction;
	if (iUserAction == WIK_NULL)
	{
		return;
	}

	//this->recordGangAction(iUserAction);

	cocos2d::Node* pSelfActionNode = WidgetFun::getChildWidget(this,"SelfActionNode");
	cocos2d::Vec2 kStartPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pSelfActionNode,"StartPos"));
	checkActionBtn(true,"HNMJButton_GuoAction",0,kStartPos);
	checkActionBtn(iUserAction&WIK_LEFT || iUserAction&WIK_CENTER 
		|| iUserAction&WIK_RIGHT,"HNMJButton_ChiAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_PENG,"HNMJButton_PengAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_GANG,"HNMJButton_GangAction",nActionCard,kStartPos);
    checkActionBtn(iUserAction&WIK_PIZI_ZFB_GANG, "HNMJButton_LiangAction", nActionCard, kStartPos);
	checkActionBtn(iUserAction&WIK_CS_GANG&&m_pLocal->isGangTing(nActionCard),"HNMJButton_GangCS",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_CHI_HU || iUserAction&WIK_ZI_MO,"HNMJButton_HuAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_XIAO_HU,"HNMJButton_XiaoHuAction",0,kStartPos);
	checkActionBtn(iUserAction&WIK_BU_ZHANG,"HNMJButton_BuAction",0,kStartPos);

	WidgetFun::setVisible(this,"SelfActionNode",true);
	if (WidgetFun::isWidgetVisble(this, "HNMJButton_GangAction"))
	{
		cocos2d::log("%s", __FUNCTION__);

		cocos2d::log("杠 count>0");
		WidgetFun::setWidgetUserInfo(this, "HNMJButton_GangAction", "Gang_CardCount", utility::toString(1));


		cocos2d::log("Gang_Card : %d", nActionCard);
		WidgetFun::setWidgetUserInfo(this, "HNMJButton_GangAction", "Gang_Card" + utility::toString(1), utility::toString((int)(nActionCard)));

	}
	if (WidgetFun::isWidgetVisble(this,"HNMJButton_ChiAction"))
	{
		WidgetFun::setVisible(this,"ChiCardBg",false);
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction","Chi_Type",utility::toString(nUserAction));
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction","Chi_Card",utility::toString(nActionCard));

		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction_Card","Chi_Type",utility::toString(nUserAction));
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction_Card","Chi_Card",utility::toString(nActionCard));
	}
	if (WidgetFun::isWidgetVisble(this,"HNMJButton_XiaoHuAction"))
	{
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_GuoAction","IsXiaoHu","XiaoHu");
	}
	else
	{
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_GuoAction","IsXiaoHu","");
	}

	if (HaveOptionRule(GAME_OPTION_RULE_HIDE_GUO)&& (iUserAction&WIK_CHI_HU || iUserAction&WIK_ZI_MO)) 
	{
		WidgetFun::setVisible(this,"HNMJButton_GuoAction",false);
	}
}
void HNMJGameScence::checkActionGangBtns(DWORD nUserAction,int nActionCard, int nGangCount, BYTE* cardDataGang)
{
	DWORD iUserAction = nUserAction;
	if (iUserAction == WIK_NULL)
	{
		return;
	}


	//这里统计记录到底是哪一种杠
	//this->recordGangAction(iUserAction);
	//----------------------------

	cocos2d::Node* pSelfActionNode = WidgetFun::getChildWidget(this,"SelfActionNode");
	cocos2d::Vec2 kStartPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pSelfActionNode,"StartPos"));
	checkActionBtn(true,"HNMJButton_GuoAction",0,kStartPos);
	checkActionBtn(iUserAction&WIK_LEFT || iUserAction&WIK_CENTER || iUserAction&WIK_RIGHT,"HNMJButton_ChiAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_PENG,"HNMJButton_PengAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_CHI_HU || iUserAction&WIK_ZI_MO,"HNMJButton_HuAction",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_XIAO_HU,"HNMJButton_XiaoHuAction",0,kStartPos);
	//检查杠牌按钮
	checkActionBtnGang(iUserAction&WIK_GANG ,"HNMJButton_GangAction",nActionCard,kStartPos,nGangCount,cardDataGang);
    checkActionBtn(iUserAction&WIK_PIZI_ZFB_GANG, "HNMJButton_LiangAction", nActionCard, kStartPos);
	checkActionBtn(iUserAction&WIK_CS_GANG&&m_pLocal->isGangTing(nActionCard),"HNMJButton_GangCS",nActionCard,kStartPos);
	checkActionBtn(iUserAction&WIK_BU_ZHANG,"HNMJButton_BuAction",0,kStartPos);

	WidgetFun::setVisible(this,"SelfActionNode",true);

	if(WidgetFun::isWidgetVisble(this,"HNMJButton_GangAction"))
	{

		cocos2d::log("杠 按钮显示");
		//当杠牌按钮存在且杠牌数>0时
		if(nGangCount>0){

			cocos2d::log("杠 count>0");
			WidgetFun::setWidgetUserInfo(this,"HNMJButton_GangAction","Gang_CardCount",utility::toString(nGangCount));

			for(int k = 0 ; k < nGangCount; k++)
		    {
				cocos2d::log("杠 Gang_Card %x : %x",(k+1),cardDataGang[k]);
				WidgetFun::setWidgetUserInfo(this,"HNMJButton_GangAction","Gang_Card"+utility::toString(k+1),utility::toString((int)(cardDataGang[k])));
		    }
		}
        if (nGangCount>1)
        {
            cocos2d::Node* node = WidgetFun::getChildWidget(this, "HNMJButton_GangAction");
            showGangCheckNode(node, WidgetFun::getUserInfo(node));
        }
	}
	if (WidgetFun::isWidgetVisble(this,"HNMJButton_ChiAction"))
	{
		WidgetFun::setVisible(this,"ChiCardBg",false);
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction","Chi_Type",utility::toString(nUserAction));
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction","Chi_Card",utility::toString(nActionCard));

		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction_Card","Chi_Type",utility::toString(nUserAction));
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_ChiAction_Card","Chi_Card",utility::toString(nActionCard));
	}
	if (WidgetFun::isWidgetVisble(this,"HNMJButton_XiaoHuAction"))
	{
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_GuoAction","IsXiaoHu","XiaoHu");
	}
	else
	{
		WidgetFun::setWidgetUserInfo(this,"HNMJButton_GuoAction","IsXiaoHu","");
	}
	if (HaveOptionRule(GAME_OPTION_RULE_HIDE_GUO)&& (iUserAction&WIK_CHI_HU || iUserAction&WIK_ZI_MO)) 
	{
		WidgetFun::setVisible(this,"HNMJButton_GuoAction",false);
	}
}
//void HNMJGameScence::recordGangAction(DWORD nUserAction)
//{
//	//这里统计记录到底是哪一种杠
//	if ((nUserAction&WIK_PIZI_ZFB_GANG) != 0)
//	{
//		m_gang_type = WIK_PIZI_ZFB_GANG;
//	}
//	//else if ((nUserAction&WIK_PIZI_GANG) != 0)
//	//{
//	//	m_gang_type = WIK_PIZI_GANG;
//	//}
//	else if ((nUserAction&WIK_GANG) != 0)
//	{
//		m_gang_type = WIK_GANG;
//	}
//}
void HNMJGameScence::checkActionBtn(bool bcheck,const std::string& BtnName,int nCard, cocos2d::Vec2& kPos)
{
	cocos2d::Node* pSelfActionNode = WidgetFun::getChildWidget(this,"SelfActionNode");
	cocos2d::Vec2 kAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pSelfActionNode,"AddPos"));
	std::string kImagicFront = WidgetFun::getWidgetUserInfo(pSelfActionNode,"ImagicFront");

	WidgetFun::setVisible(this,BtnName,false);
	if (bcheck)
	{
		kPos +=kAddPos;
		WidgetFun::setVisible(this,BtnName,true);
		WidgetFun::setPos(this,BtnName,kPos);
		cocos2d::Node* pActionBtnCard = WidgetFun::getChildWidget(this,utility::toString(BtnName,"_Card"));
		if(nCard != 0 && pActionBtnCard)
		{
			pActionBtnCard->setVisible(true);
			setActionBtnCard(pActionBtnCard,nCard,kImagicFront);
		}
		else if(pActionBtnCard)
		{
			pActionBtnCard->setVisible(false);
		}
	}
}
void HNMJGameScence::checkActionBtnGang(bool bcheck,const std::string& BtnName,int nCard, cocos2d::Vec2& kPos, int nGangCount, BYTE* cardDataGang)
{
	//assert(nGangCount <=3); //只有3个显示杠牌的widget

	cocos2d::Node* pSelfActionNode = WidgetFun::getChildWidget(this,"SelfActionNode");
	cocos2d::Vec2 kAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pSelfActionNode,"AddPos"));
	std::string kImagicFront = WidgetFun::getWidgetUserInfo(pSelfActionNode,"ImagicFront");

	WidgetFun::setVisible(this,BtnName,false);
	if (bcheck)
	{
	
		kPos +=kAddPos;
		WidgetFun::setVisible(this,BtnName,true);
		WidgetFun::setPos(this,BtnName,kPos);
		cocos2d::Node* pActionBtnCard = WidgetFun::getChildWidget(this,utility::toString(BtnName,"_Card"));
		pActionBtnCard->setVisible(false);
		WidgetFun::setVisible(this,"GangCardBg",false);
		if(nGangCount>0){
			if(nGangCount>1){
				//WidgetFun::setVisible(this,"GangCardBg",true);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp1"))->setVisible(false);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp2"))->setVisible(false);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp3"))->setVisible(false);
                WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp4"))->setVisible(false);
                WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp5"))->setVisible(false);
                WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp6"))->setVisible(false);
			}
            else
            {
                pActionBtnCard->setVisible(true);
            }

			for(int k = 0 ; k < nGangCount; k++)
		    {
		        if(cardDataGang[k] != 0 && pActionBtnCard)
		        //if(cardDataGang[k]!=0)
				{
					cocos2d::Node* pGang_Card_Temp_i = WidgetFun::getChildWidget(this,utility::toString("Gang_Card_Temp",k+1));
					cocos2d::Node* pGangCardNode = WidgetFun::getChildWidget(pGang_Card_Temp_i,"HNMJButton_GangCard");
					//pGang_Card_Temp_i->setVisible(true);
                    setActionBtnCard(pGangCardNode,cardDataGang[k],kImagicFront);
                    cocos2d::log("杠 设置：%x",cardDataGang[k]);
					WidgetFun::setWidgetUserInfo(pGang_Card_Temp_i,"GangIndex",utility::toString((int)(cardDataGang[k])));
					setActionBtnCard(pActionBtnCard,cardDataGang[k],kImagicFront);
				}
				else if(pActionBtnCard)
				{
					pActionBtnCard->setVisible(false);
				}
		    }
		}
	}
}

void HNMJGameScence::checkActionBtnZFBGang(bool bcheck, const std::string & BtnName, int nCard, cocos2d::Vec2 & kPos, int nGangCount, BYTE * cardDataGang)
{

	cocos2d::Node* pSelfActionNode = WidgetFun::getChildWidget(this, "SelfActionNode");
	cocos2d::Vec2 kAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pSelfActionNode, "AddPos"));
	std::string kImagicFront = WidgetFun::getWidgetUserInfo(pSelfActionNode, "ImagicFront");

	WidgetFun::setVisible(this, BtnName, false);
	if (bcheck)
	{

		kPos += kAddPos;
		WidgetFun::setVisible(this, BtnName, true);
		WidgetFun::setPos(this, BtnName, kPos);
		cocos2d::Node* pActionBtnCard = WidgetFun::getChildWidget(this, utility::toString(BtnName, "_Card"));
		pActionBtnCard->setVisible(false);
		WidgetFun::setVisible(this, "GangCardBg", false);
		if (nGangCount>0) {
			if (nGangCount>1) {
				WidgetFun::setVisible(this, "GangCardBg", true);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp1"))->setVisible(false);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp2"))->setVisible(false);
				WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp3"))->setVisible(false);
			}
			else
				pActionBtnCard->setVisible(true);
			for (int k = 0; k < nGangCount; k++)
			{
				if (cardDataGang[k] != 0 && pActionBtnCard)
					//if(cardDataGang[k]!=0)
				{
					cocos2d::Node* pGang_Card_Temp_i = WidgetFun::getChildWidget(this, utility::toString("Gang_Card_Temp", k + 1));
					cocos2d::Node* pGangCardNode = WidgetFun::getChildWidget(pGang_Card_Temp_i, "HNMJButton_GangCard");
					pGang_Card_Temp_i->setVisible(true);
					setActionBtnCard(pGangCardNode, cardDataGang[k], kImagicFront);
					cocos2d::log("杠 设置：%x", cardDataGang[k]);
					WidgetFun::setWidgetUserInfo(pGang_Card_Temp_i, "GangIndex", utility::toString((int)(cardDataGang[k])));
					setActionBtnCard(pActionBtnCard, cardDataGang[k], kImagicFront);
				}
				else if (pActionBtnCard)
				{
					pActionBtnCard->setVisible(false);
				}
			}
		}
	}
}

void HNMJGameScence::showJieSuanInfo(CMD_S_GameEnd* pGameEnd)
{//显示结算界面
	float delayTime = 2.0f;
	cocos2d::Node* pNiaoCardAni = WidgetFun::getChildWidget(this,"GameEndNiaoCardAni");
	pNiaoCardAni->setVisible(false);
	if (pGameEnd->cbNiaoCount>0)
	{
		pNiaoCardAni->runAction(cocos2d::CCVisibleAction::create(delayTime,true));
		if (!HaveOptionRule(GAME_OPTION_RULE_NIAO_ANIM_2))
		{
			setGameEndNiaoCardAni1(pNiaoCardAni,pGameEnd->cbCardDataNiao,pGameEnd->cbNiaoCount,delayTime);
		}
		else
		{
			setGameEndNiaoCardAni2(pNiaoCardAni,pGameEnd->cbCardDataNiao,pGameEnd->cbNiaoCount,delayTime);
		}
	}
	delayTime += 2.0f;
	GameManagerBase::InstanceBase().OnGameEnd(this,delayTime+3.0f);

	cocos2d::Node* pJieSuanNode = WidgetFun::getChildWidget(this,"GameJieSuanNode");
	WidgetFun::setWidgetUserInfo(this,"GameJieSuanNode","Time",utility::toString((int)delayTime));
	cocos2d::CCVisibleAction* pAction = cocos2d::CCVisibleAction::create(delayTime,true);
	pAction->setTag(1);
	pJieSuanNode->runAction(pAction);
	pNiaoCardAni->runAction(cocos2d::CCVisibleAction::create(delayTime,false));

	TimeManager::Instance().addCerterTimeCB(TIME_CALLBACK(HNMJGameScence::upSelfFreeReadState,this),delayTime+1);

	 WidgetFun::setPlaceText(pJieSuanNode,"JieSuan_JuShu",utility::toString(getPlayCount()));
	 setJieSuanNiaoCard(pJieSuanNode,pGameEnd->cbCardDataNiao,pGameEnd->cbNiaoCount);
	 
	 bool isLiuJu = true;
	 bool isZimo = false;
     int huChairID = -1;

	 for (int i=0;i<MAX_PLAYER;i++)
	 {
		int iChirID = m_pPlayer[i]->GetChairID();
		//int nHuScore = pGameEnd->lGameScore[iChirID] - pGameEnd->lStartHuScore[iChirID]-pGameEnd->lGangScore[iChirID];
		int nHuScore = this->getJieSuanHuScore(pGameEnd, iChirID);

		cocos2d::Node* pJieSuanInfo = WidgetFun::getChildWidget(pJieSuanNode,utility::toString("JieSuanInfo",i));

		WidgetFun::setText(pJieSuanInfo,"Jiesuan_Nicke",m_pPlayer[i]->GetNickName());
		if (pGameEnd->lGameScore[iChirID]>=0)
		{
			WidgetFun::setText(pJieSuanInfo,"Jiesuan_GoldNum",utility::toString("+",pGameEnd->lGameScore[iChirID]));
		}
		else
		{
			WidgetFun::setText(pJieSuanInfo,"Jiesuan_GoldNum",pGameEnd->lGameScore[iChirID]);
		}

		WidgetFun::setVisible(pJieSuanInfo,"JieSuan_Zhuang",false);
		WidgetFun::setVisible(pJieSuanInfo,"JieSuan_Hu",false);
		WidgetFun::setVisible(pJieSuanInfo,"Jiesuan_HuType",false);
		if (nHuScore>0 )
		{
			WidgetFun::setVisible(pJieSuanInfo,"JieSuan_Hu",true);
            huChairID = iChirID;
			if (pGameEnd->wProvideUser[iChirID] == iChirID )
			{
				isZimo = true;
			}
		}
		if (iChirID == m_iBankerUser)
		{
			WidgetFun::setVisible(pJieSuanInfo,"JieSuan_Zhuang",true);
		}
		cocos2d::Node* pCardNode = WidgetFun::getChildWidget(pJieSuanInfo,"JieSuanCardNode");
		m_pPlayer[i]->showJieSuanCard(pCardNode,pGameEnd->WeaveItemArray[iChirID],pGameEnd->cbWeaveCount[iChirID],
			pGameEnd->cbCardData[iChirID],pGameEnd->cbCardCount[iChirID]);

        //显示每个item右上方痞子赖子的信息
        m_pPlayer[i]->showJieSuanPiZiCard(WidgetFun::getChildWidget(pJieSuanInfo, "JieSuanPiZiCardNode"), pGameEnd->PiZiWeaveItemArray[iChirID], pGameEnd->cbPiZiWeaveCount[iChirID]);
		
		if(pGameEnd->lGameScore[iChirID] != 0)
		{
			isLiuJu = false;
		}
	 }

	 for (int i=0;i<MAX_PLAYER;i++)
	 {
		 int iChirID = m_pPlayer[i]->GetChairID();
		 //int nHuScore = pGameEnd->lGameScore[iChirID] - pGameEnd->lStartHuScore[iChirID]-pGameEnd->lGangScore[iChirID];
		 int nHuScore = this->getJieSuanHuScore(pGameEnd, iChirID);

		 cocos2d::Node* pJieSuanInfo = WidgetFun::getChildWidget(pJieSuanNode,utility::toString("JieSuanInfo",i));
		 WidgetFun::setVisible(pJieSuanInfo,"Jiesuan_HuType",true);

		 WidgetFun::setNodeKeepAfter(pJieSuanInfo,WidgetFun::getChildWidget(pJieSuanInfo,"Jiesuan_Nicke"),"Jiesuan_HuType");


         std::string huTypeStr = WidgetFun::getText(pJieSuanInfo, "Jiesuan_HuType");

		 std::string kHuType;
		 if(nHuScore>0 && isZimo)
		 {
			kHuType = ScriptData<std::string>("HU_TYPE_ZIMO").Value();
			if (pGameEnd->cbNiaoCount>0)
			{
				kHuType += utility::toString(ScriptData<std::string>("ZHONG_NIAO").Value(),(int)pGameEnd->cbNiaoPick,ScriptData<std::string>("ZHONG_GE").Value());
			}
		 }
		 else if(nHuScore>0 && !isZimo)
		 {
			 kHuType = ScriptData<std::string>("HU_TYPE_JIEPAO").Value();
			 if (pGameEnd->cbNiaoCount>0)
			 {
				 kHuType += utility::toString(ScriptData<std::string>("ZHONG_NIAO").Value(),(int)pGameEnd->cbNiaoPick,ScriptData<std::string>("ZHONG_GE").Value());
			 }
		 }
		 else if (!isZimo && m_pPlayer[i]->GetChairID() == pGameEnd->wProvideUser[huChairID])
		 {
			kHuType = ScriptData<std::string>("HU_TYPE_FANGPAO").Value();
		 }

		 if (pGameEnd->lGangScore[iChirID]>0)
		 {
			 kHuType += getStringGang(iChirID,pGameEnd->WeaveItemArray[iChirID],pGameEnd->cbWeaveCount[iChirID]);
		 }
		 if (pGameEnd->lStartHuScore[iChirID] >0)
		 {
			 kHuType += getStringHuRight(pGameEnd->dwStartHuRight[iChirID]);
		 }
		 if (pGameEnd->lGameScore[iChirID] >0)
		 {
			 kHuType += getStringHuRight(pGameEnd->dwChiHuRight[iChirID]);
		 }

		 WidgetFun::setText(pJieSuanInfo,"Jiesuan_HuType",kHuType);
	 }

	 int nChairID = m_pLocal->GetChairID();
	 cocos2d::Node* pJieSuanTitle = WidgetFun::getChildWidget(pJieSuanNode,"JieSuan_Title");
	 cocos2d::Node* pJieSuanNPC = WidgetFun::getChildWidget(pJieSuanNode,"JieSuan_NPC");
	 if (pGameEnd->lGameScore[nChairID]>0)
	 {
		 std::string kTitleWin = WidgetFun::getWidgetUserInfo(pJieSuanTitle,"Title_Win");
		 std::string kNPCXiao = WidgetFun::getWidgetUserInfo(pJieSuanNPC,"NPC_Smile");

		 WidgetFun::setImagic(pJieSuanTitle,kTitleWin,false);
		 WidgetFun::setImagic(pJieSuanNPC,kNPCXiao,false);
	 }
	 else if (pGameEnd->lGameScore[nChairID]== 0)
	 {
		 std::string kTitlePing = WidgetFun::getWidgetUserInfo(pJieSuanTitle,"Title_Ping");
		 if (isLiuJu == true)
		 {
			 kTitlePing =  WidgetFun::getWidgetUserInfo(pJieSuanTitle,"Title_LiuJu");
		 }
		 std::string kNPCXiao = WidgetFun::getWidgetUserInfo(pJieSuanNPC,"NPC_Smile");

		 WidgetFun::setImagic(pJieSuanTitle,kTitlePing,false);
		 WidgetFun::setImagic(pJieSuanNPC,kNPCXiao,false);
	 }
	 else if (pGameEnd->lGameScore[nChairID] < 0)
	 {
		 std::string kTitleLose = WidgetFun::getWidgetUserInfo(pJieSuanTitle,"Title_Lose");
		 std::string kNPCKu = WidgetFun::getWidgetUserInfo(pJieSuanNPC,"NPC_Cry");

		 WidgetFun::setImagic(pJieSuanTitle,kTitleLose,false);
		 WidgetFun::setImagic(pJieSuanNPC,kNPCKu,false);
	 }

}

int HNMJGameScence::getJieSuanHuScore(CMD_S_GameEnd* pGameEnd, int nChairID)
{
	//int nHuScore = pGameEnd->lGameScore[iChirID] - pGameEnd->lStartHuScore[iChirID]-pGameEnd->lGangScore[iChirID];
	int nHuScore = pGameEnd->lGameScore[nChairID];
	return nHuScore;
}

void HNMJGameScence::setJieSuanNiaoCard( cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum )
{
	ASSERT(pNode);

	cocos2d::Node* pNiaoNode = WidgetFun::getChildWidget(pNode,"JieSuan_CardNiao");
 	pNiaoNode->removeAllChildren();

	int iIdex = 0;
	int iOder = 0;
	cocos2d::Vec2 kStartPos = cocos2d::Vec2(0,0);
	std::string kHandSkin = utility::toString("HNMJ_HAND_",iIdex);
	cocos2d::Vec2 kHandAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pNiaoNode,"JieSuanAddPos"));
	for (int i=0;i<cbCardNum;i++)
	{
		ASSERT(HNMJ::CGameLogic::Instance().IsValidCard(pNiaoCard[i]));

		cocos2d::Node* pCard = WidgetManager::Instance().createWidget(kHandSkin,pNiaoNode);
		pCard->setTag(1);
		pCard->setLocalZOrder(iOder);
		pCard->setPosition(kStartPos);
		{
			kStartPos += kHandAddPos;
		}
		if (iIdex == 0)
		{
			std::string kImagic = WidgetFun::getWidgetUserInfo(pCard,"MingImagic");
			m_pLocal->setCardImagic(pCard,pNiaoCard[i],kImagic);
		}
		iOder++;
	}

}


void HNMJGameScence::setGameEndNiaoCardAni1(cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum,float& delayTime)
{
	ASSERT(pNode);

	cocos2d::Node* pNiaoNode = WidgetFun::getChildWidget(pNode,"NiaoCardAniNode");
	pNiaoNode->removeAllChildren();

	cocos2d::Vec2 kHandAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pNiaoNode,"JieSuanAddPos"));
	int iOder = 0;

	float fWidth = (700 - kHandAddPos.x * (int)cbCardNum)/(int)(cbCardNum+1)+kHandAddPos.x;

	cocos2d::Vec2 kStartPos = cocos2d::Vec2(-350-kHandAddPos.x/2,0);
	std::string kHandSkin = "HNMJ_NIAO_CARD_NODE1";
	for (int i=0;i<cbCardNum;i++)
	{
		kStartPos.x += fWidth;
		ASSERT(HNMJ::CGameLogic::Instance().IsValidCard(pNiaoCard[i]));

		cocos2d::Node* pCardNode = WidgetManager::Instance().createWidget(kHandSkin,pNiaoNode);
		pCardNode->setTag(1);
		pCardNode->setLocalZOrder(iOder);
		pCardNode->setPosition(kStartPos);
		
		cocos2d::Node* pCard = WidgetFun::getChildWidget(pCardNode,"HNMJ_NIAO_CARD");
		std::string kImagic  = WidgetFun::getWidgetUserInfo(pCard,"MingImagic");
		m_pLocal->setCardImagic(pCard,pNiaoCard[i],kImagic);
		
		WidgetFun::setWidgetUserInfo(pCard,"ShowDelayTime",utility::toString(delayTime));

		float fBombDelay = utility::parseFloat(WidgetFun::getWidgetUserInfo(pCardNode,"BombDelay"));
		cocos2d::Node* pBomb = WidgetFun::getChildWidget(pCardNode,"Bomb");
		WidgetFun::setWidgetUserInfo(pBomb,"BombDelayTime",utility::toString(delayTime+fBombDelay));
		pCard->setVisible(false);
		pBomb->setVisible(false);
		WidgetFun::runWidgetAction(pCard,"ShowAni");
		WidgetFun::runWidgetAction(pBomb,"ShowAni");

		if (!HaveOptionRule(GAME_OPTION_RULE_NIAO_DIRECT))
		{
			delayTime += fBombDelay+0.7;
		}
		iOder++;
	}
}
void HNMJGameScence::setGameEndNiaoCardAni2(cocos2d::Node* pNode,BYTE* pNiaoCard,BYTE cbCardNum,float& delayTime)
{

	ASSERT(pNode);

	cocos2d::Node* pNiaoNode = WidgetFun::getChildWidget(pNode,"NiaoCardAniNode");
	pNiaoNode->removeAllChildren();

	cocos2d::Vec2 kHandAddPos = utility::parsePoint(WidgetFun::getWidgetUserInfo(pNiaoNode,"JieSuanAddPos"));
	int iOder = 0;

	float fWidth = (700 - kHandAddPos.x * (int)cbCardNum)/(int)(cbCardNum+1)+kHandAddPos.x;

	cocos2d::Vec2 kStartPos = cocos2d::Vec2(-350-kHandAddPos.x/2,0);
	std::string kHandSkin = "HNMJ_NIAO_CARD_NODE2";
	for (int i=0;i<cbCardNum;i++)
	{
		kStartPos.x += fWidth;
		ASSERT(HNMJ::CGameLogic::Instance().IsValidCard(pNiaoCard[i]));

		cocos2d::Node* pCardNode = WidgetManager::Instance().createWidget(kHandSkin,pNiaoNode);
		pCardNode->setTag(1);
		pCardNode->setLocalZOrder(iOder);
		pCardNode->setPosition(kStartPos);

		cocos2d::Node* pCard = WidgetFun::getChildWidget(pCardNode,"HNMJ_NIAO_CARD");
		WidgetFun::setWidgetUserInfo(pCard,"ShowDelayTime",utility::toString(delayTime));

		BYTE cbValue= HNMJ::CGameLogic::Instance().GetCardValue(pNiaoCard[i]);
		BYTE cbColor=HNMJ::CGameLogic::Instance().GetCardColor(pNiaoCard[i]);
		WidgetFun::setWidgetUserInfo(pCard,"Pic1",utility::toString("GameHNMJ/2/handmah_",(int)cbColor,(int)cbValue,".png"));
		WidgetFun::setWidgetUserInfo(pCard,"Pic2",utility::toString("GameHNMJ/2/mingmah_",(int)cbColor,(int)cbValue,".png"));

		WidgetFun::runWidgetAction(pCard,"ShowAni");

		if (!HaveOptionRule(GAME_OPTION_RULE_NIAO_DIRECT))
		{
			delayTime += 0.7f;
		}
		iOder++;
	}
}

bool HNMJGameScence::BackKey()
{
	if(!IsInGame())
	{
		return false;
	}
	if (WidgetFun::getChildWidget(this,"GameSetPlane"))
	{
		WidgetFun::setVisible(this,"GameSetPlane",true);
	}
	else
	{
		GameBase::BackKey();
	}
	return true;
}

void HNMJGameScence::setGameState(int nState)
{
	m_nGameState = nState;
}
int HNMJGameScence::getGameState()
{
	return m_nGameState;
}

