#ifndef CMD_SPARROW_HEAD_FILE
#define CMD_SPARROW_HEAD_FILE

#include "Game/Game/GameDefine.h"
#pragma pack(1)

//◊Èº˛ Ù–‘
#define GAME_PLAYER					4									//游戏人数

#define GAME_START_SCORE            0                                   //游戏的起始分数

#define GAME_GENRE					(GAME_GENRE_SCORE|GAME_GENRE_MATCH|GAME_GENRE_GOLD)	//”Œœ∑¿‡–Õ

//”Œœ∑◊¥Ã¨
#define GS_MJ_FREE					GAME_STATUS_FREE					//空闲状态
#define GS_MJ_PLAY					(GAME_STATUS_PLAY+1)				//游戏状态
#define GS_MJ_XIAOHU				(GAME_STATUS_PLAY+2)				//小胡状态

#define TIME_START_GAME				30									//ø™ º∂® ±∆˜
#define TIME_OPERATE_CARD			15									//≤Ÿ◊˜∂® ±∆˜


#define GAME_TYPE_ZZ				0
#define GAME_TYPE_CS				1
#define GAME_TYPE_HH				GAME_TYPE_ZZ									//黄石晃晃

#define GAME_TYPE_ZZ_ZIMOHU			1		//只能自模胡
#define GAME_TYPE_ZZ_QIDUI			2		//可胡七对
#define GAME_TYPE_ZZ_QIANGGANGHU	3		//可抢杠胡
#define GAME_TYPE_ZZ_ZHANIAO2		4		//扎鸟2个
#define GAME_TYPE_ZZ_ZHANIAO4		5		//扎鸟4个
#define GAME_TYPE_ZZ_ZHANIAO6		6		//扎鸟6个
#define GAME_TYPE_ZZ_HONGZHONG		7		//红中癞子
#define GAME_TYPE_CS_ZHUANGXIANFEN	8		//庄闲分
#define GAME_TYPE_ZZ_ZHANIAO3		9		//扎鸟3个
#define GAME_TYPE_ZZ_ZHANIAO5		10		//扎鸟5个
#define GAME_TYPE_ZZ_258			11		//258做将 长沙麻将
#define GAME_TYPE_ZZ_HONGZHONG_GZ	12		//红中玩法 癞子算红中2分 不可接炮

#define GAME_TYPE_ZZ_LIUJU_4CARD	13		//剩余4张黄庄
#define GAME_TYPE_ZZ_ZHANIAO1		14		//‘˙ƒÒ1∏ˆ
#define GAME_TYPE_ZZ_ZHANIAO_DOUBLE	15		//‘˙ƒÒ∑≠±∂


#define GAME_TYPE_HSHH_DIFEN16  	16		//胡牌底分16
#define GAME_TYPE_HSHH_DIFEN20  	17		//胡牌底分20


#define MAX_GAME_RULE				8		//◊Ó∂‡πÊ‘Ú

//////////////////////////////////////////////////////////////////////////

//◊È∫œ◊”œÓ
struct CMD_WeaveItem
{
	DWORD							cbWeaveKind;						//◊È∫œ¿‡–Õ
	BYTE							cbCenterCard;						//÷––ƒ∆ÀøÀ
	BYTE							cbPublicCard;						//π´ø™±Í÷æ
	WORD							wProvideUser;						//π©”¶”√ªß
};

//////////////////////////////////////////////////////////////////////////
//∑˛ŒÒ∆˜√¸¡ÓΩ·ππ

#define SUB_S_GAME_START			100									//”Œœ∑ø™ º
#define SUB_S_OUT_CARD				101									//≥ˆ≈∆√¸¡Ó
#define SUB_S_SEND_CARD				102									//∑¢ÀÕ∆ÀøÀ
#define SUB_S_SEND_CARD_CSGANG		103									//∑¢ÀÕ∆ÀøÀ
#define SUB_S_OPERATE_NOTIFY		104									//≤Ÿ◊˜Ã· æ
#define SUB_S_OPERATE_RESULT		105									//≤Ÿ◊˜√¸¡Ó
#define SUB_S_GAME_END				106									//”Œœ∑Ω· ¯
#define SUB_S_TRUSTEE				107									//”√ªßÕ–π‹
#define SUB_S_CHI_HU				108									//
#define SUB_S_GANG_SCORE			110									//
#define SUB_S_OUT_CARD_CSGANG		111									//
#define SUB_S_XIAO_HU				112									//–°∫˙
#define SUB_S_GAME_END_LAST_CARD	113									// £œ¬≈∆

#define SUB_S_MASTER_HANDCARD		120									//
#define SUB_S_MASTER_LEFTCARD		121									// £”‡≈∆∂—

#define ZI_PAI_COUNT	7												//∂—¡¢»´≈∆

#define MASK_CHI_HU_RIGHT			0x0fffffff							//◊Ó¥Û»®ŒªDWORD∏ˆ ˝			

//≥£¡ø∂®“Â
#define MAX_WEAVE					4									//◊Ó¥Û◊È∫œ
#define MAX_WEAVE_PIZI				12									//痞子最大组合
//#define MAX_WEAVE					10									//◊Ó¥Û◊È∫œ
#define MAX_INDEX					34									//◊Ó¥ÛÀ˜“˝
#define MAX_COUNT					14									//◊Ó¥Û ˝ƒø
#define MAX_REPERTORY				108									//◊Ó¥Ûø‚¥Ê

#define MAX_REPERTORY_HSHH			120									//黄石晃晃麻将最大库存
#define MAX_PIZI_INDEX			    2									//黄石晃晃麻将最大库存

#define MAX_NIAO_CARD				10									//◊Ó¥Û÷–ƒÒ ˝
//”Œœ∑◊¥Ã¨
struct CMD_S_StatusFree
{
	LONGLONG						lCellScore;							//ª˘¥°Ω±“
	WORD							wBankerUser;						//◊Øº“”√ªß
	bool							bTrustee[GAME_PLAYER];				// «∑ÒÕ–π‹
};

//游戏状态
struct CMD_S_StatusPlay
{
	//游戏变量
	LONGLONG						lCellScore;									//单元积分
	WORD							wBankerUser;								//庄家用户
	WORD							wCurrentUser;								//当前用户

																				//状态变量
	BYTE							cbActionCard;								//动作扑克
	DWORD							cbActionMask;								//动作掩码
	BYTE							cbLeftCardCount;							//剩余数目
	bool							bTrustee[GAME_PLAYER];						//是否托管
	WORD							wWinOrder[GAME_PLAYER];						//

																				//出牌信息
	WORD							wOutCardUser;								//出牌用户
	BYTE							cbOutCardData;								//出牌扑克
	BYTE							cbDiscardCount[GAME_PLAYER];				//丢弃数目
	BYTE							cbDiscardCard[GAME_PLAYER][60];				//丢弃记录

																				//扑克数据
	BYTE							cbCardCount;								//扑克数目
	BYTE							cbCardData[MAX_COUNT];						//扑克列表
	BYTE							cbSendCardData;								//发送扑克

																				//组合扑克
	BYTE							cbWeaveCount[GAME_PLAYER];					//组合数目
	CMD_WeaveItem					WeaveItemArray[GAME_PLAYER][MAX_WEAVE];		//组合扑克

	BYTE							cbPiZiCount[GAME_PLAYER];					//痞子数目
	CMD_WeaveItem					PiZiItemArray[GAME_PLAYER][MAX_WEAVE_PIZI];		//痞子扑克

	bool                           bHasCSGang[GAME_PLAYER];

	BYTE							cbMagicCardData;                           //癞子牌
};

//游戏开始
struct CMD_S_GameStart
{
    QYLONG							lSiceCount;									//骰子点数
	WORD							wBankerUser;								//庄家用户
	WORD							wCurrentUser;								//当前用户
	DWORD							cbUserAction;								//用户动作
	BYTE							cbCardData[MAX_COUNT*GAME_PLAYER];			//扑克列表
	BYTE							cbLeftCardCount;							//
	BYTE							cbXiaoHuTag;                           //小胡标记 0 没小胡 1 有小胡；
	BYTE							cbMagicCardData;                           //癞子牌
};

//≥ˆ≈∆√¸¡Ó
struct CMD_S_OutCard
{
	WORD							wOutCardUser;						//≥ˆ≈∆”√ªß
	BYTE							cbOutCardData;						//≥ˆ≈∆∆ÀøÀ
};

//≥ˆ≈∆√¸¡Ó
struct CMD_S_OutCard_CSGang
{
	WORD							wOutCardUser;						//≥ˆ≈∆”√ªß
	BYTE							cbOutCardData1;						//≥ˆ≈∆∆ÀøÀ
	BYTE							cbOutCardData2;						//≥ˆ≈∆∆ÀøÀ
};
//发送扑克
struct CMD_S_SendCard
{
	BYTE							cbCardData;							//扑克数据
	DWORD							cbActionMask;						//动作掩码
	WORD							wCurrentUser;						//当前用户
    BYTE                            cbCardDataGang[8];                  //哪些牌可以杠
    BYTE                            cbGangCount;                        //有多少张可以杠的牌
	bool							bTail;								//末尾发牌
};
//∑¢ÀÕ∆ÀøÀ
struct CMD_S_SendCard_CSGang
{
	BYTE							cbCardData1;						//∆ÀøÀ ˝æ›
	BYTE							cbCardData2;						//∆ÀøÀ ˝æ›
	DWORD							cbActionMask;						//∂Ø◊˜—⁄¬Î
	WORD							wCurrentUser;						//µ±«∞”√ªß
};

//≤Ÿ◊˜Ã· æ
struct CMD_S_OperateNotify
{
	WORD							wResumeUser;						//还原用户
	DWORD							cbActionMask;						//动作掩码
	BYTE							cbActionCard;						//动作扑克
    BYTE							cbCardDataGang[8];				    //当前可以杠的牌集合
    BYTE							cbGangCount;						//当前可以杠的拍的数量		
};

//≤Ÿ◊˜√¸¡Ó
struct CMD_S_OperateResult
{
	WORD							wOperateUser;						//≤Ÿ◊˜”√ªß
	WORD							wProvideUser;						//π©”¶”√ªß
	DWORD							cbOperateCode;						//≤Ÿ◊˜¥˙¬Î
	BYTE							cbOperateCard;						//≤Ÿ◊˜∆ÀøÀ
};

//”Œœ∑Ω· ¯
struct CMD_S_GameEnd
{
	BYTE							cbCardCount[GAME_PLAYER];			//
	BYTE							cbCardData[GAME_PLAYER][MAX_COUNT];	//
	//结束信息
	WORD							wProvideUser[GAME_PLAYER];			//供应用户
	dword							dwChiHuRight[GAME_PLAYER];			//胡牌类型
	dword							dwStartHuRight[GAME_PLAYER];			//起手胡牌类型
	LONGLONG						lStartHuScore[GAME_PLAYER];			//起手胡牌分数

	//积分信息
	LONGLONG						lGameScore[GAME_PLAYER];			//游戏积分
	int								lGameTax[GAME_PLAYER];				//

	WORD							wWinOrder[GAME_PLAYER];				//胡牌排名

	LONGLONG						lGangScore[GAME_PLAYER];			//详细得分
	BYTE							cbGenCount[GAME_PLAYER];			//
	WORD							wLostFanShu[GAME_PLAYER][GAME_PLAYER];
	WORD							wLeftUser;							//

	//组合扑克
	BYTE							cbWeaveCount[GAME_PLAYER];					//组合数目
	CMD_WeaveItem					WeaveItemArray[GAME_PLAYER][MAX_WEAVE];		//组合扑克

    BYTE							cbPiZiWeaveCount[GAME_PLAYER];					//痞子癞子杠数目
    CMD_WeaveItem					PiZiWeaveItemArray[GAME_PLAYER][MAX_WEAVE_PIZI];		//痞子癞子杠

	BYTE							cbCardDataNiao[MAX_NIAO_CARD];	//鸟牌
	BYTE							cbNiaoCount;	//鸟牌个数
	BYTE							cbNiaoPick;	//中鸟个数
};

struct CMD_S_GameEnd_LastCard
{
	BYTE							cbCardCout;// £”‡∏ˆ ˝
	BYTE							cbCardData[108];	// £”‡≈∆
};

//”√ªßÕ–π‹
struct CMD_S_Trustee
{
	bool							bTrustee;							// «∑ÒÕ–π‹
	WORD							wChairID;							//Õ–π‹”√ªß
};

//
struct CMD_S_ChiHu
{
	WORD							wChiHuUser;							//
	WORD							wProviderUser;						//
	BYTE							cbChiHuCard;						//
	BYTE							cbCardCount;						//
	LONGLONG						lGameScore;							//
	BYTE							cbWinOrder;							//
};
struct CMD_S_XiaoHu
{
	WORD							wXiaoHuUser;							//
	DWORD                        dwXiaoCode;

	BYTE							cbCardCount;
	BYTE							cbCardData[MAX_COUNT];
};

//
struct CMD_S_GangScore
{
	WORD							wChairId;							//
	BYTE							cbXiaYu;							//
	LONGLONG						lGangScore[GAME_PLAYER];			//
};

//////////////////////////////////////////////////////////////////////////
//øÕªß∂À√¸¡ÓΩ·ππ

#define SUB_C_OUT_CARD				1									//≥ˆ≈∆√¸¡Ó
#define SUB_C_OPERATE_CARD			3									//≤Ÿ◊˜∆ÀøÀ
#define SUB_C_TRUSTEE				4									//”√ªßÕ–π‹
#define SUB_C_XIAOHU				5									//–°∫˙
#define SUB_C_MASTER_LEFTCARD		6									// £”‡≈∆∂—
#define SUB_C_MASTER_CHEAKCARD		7									//—°‘Òµƒ≈∆
#define SUB_C_MASTER_ZHANIAO		8									//‘˙ƒÒ
#define SUB_C_OUT_CARD_CSGANG		9									//≥§…≥∏‹¥Ú≥ˆ

#define CARD_COLOR_NULL			0
#define CARD_COLOR_TONG			1
#define CARD_COLOR_WAN			2
#define CARD_COLOR_TIAO			3
//≥ˆ≈∆√¸¡Ó
struct CMD_C_OutCard
{
	BYTE							cbCardData;							//∆ÀøÀ ˝æ›
};
//≥ˆ≈∆√¸¡Ó
struct CMD_C_OutCard_CSGang
{
	BYTE							cbCardData1;							//∆ÀøÀ ˝æ›
	BYTE							cbCardData2;							//∆ÀøÀ ˝æ›
};
//≤Ÿ◊˜√¸¡Ó
struct CMD_C_OperateCard
{
	DWORD							cbOperateCode;						//≤Ÿ◊˜¥˙¬Î
	BYTE							cbOperateCard;						//≤Ÿ◊˜∆ÀøÀ
};

//”√ªßÕ–π‹
struct CMD_C_Trustee
{
	bool							bTrustee;							// «∑ÒÕ–π‹	
};

//∆ ÷–°∫˙
struct CMD_C_XiaoHu
{
	DWORD						cbOperateCode;						//≤Ÿ◊˜¥˙¬Î
	BYTE							cbOperateCard;						//≤Ÿ◊˜∆ÀøÀ
};


//////////////////////////////////////////////////////////////////////////

struct  HNMJGameRecordPlayer
{
	DWORD dwUserID;
	std::string kHead;
	std::string kNickName;
	std::vector<BYTE> cbCardData;
	void StreamValue(datastream& kData,bool bSend)
	{
		Stream_VALUE(dwUserID);
		Stream_VALUE(kHead);
		Stream_VALUE(kNickName);
		Stream_VALUE(cbCardData);
	}
};

struct  HNMJGameRecordOperateResult
{
	enum Type
	{
		TYPE_NULL,
		TYPE_OperateResult,
		TYPE_SendCard,
		TYPE_OutCard,
		TYPE_ChiHu,
	};
	HNMJGameRecordOperateResult()
	{
		cbActionType = 0;
		wOperateUser = 0;
		wProvideUser = 0;
		cbOperateCode = 0;
		cbOperateCard = 0;
	}
	BYTE							cbActionType;
	WORD							wOperateUser;						//≤Ÿ◊˜”√ªß
	WORD							wProvideUser;						//π©”¶”√ªß
	DWORD							cbOperateCode;						//≤Ÿ◊˜¥˙¬Î
	BYTE							cbOperateCard;						//≤Ÿ◊˜∆ÀøÀ
	void StreamValue(datastream& kData,bool bSend)
	{
		Stream_VALUE(cbActionType);
		Stream_VALUE(wOperateUser);
		Stream_VALUE(wProvideUser);
		Stream_VALUE(cbOperateCode);
		Stream_VALUE(cbOperateCard);
	}
};
struct  HNMJGameRecord
{
	DWORD dwKindID;
	DWORD dwVersion;
    BYTE cbMagicCardData;
	std::vector<HNMJGameRecordPlayer> kPlayers;
	std::vector<HNMJGameRecordOperateResult> kAction;
	void StreamValue(datastream& kData,bool bSend)
	{
		StructVecotrMember(HNMJGameRecordPlayer,kPlayers);
		StructVecotrMember(HNMJGameRecordOperateResult,kAction);
		Stream_VALUE(dwKindID);
		Stream_VALUE(dwVersion);
        Stream_VALUE(cbMagicCardData);
    }

};

struct MasterHandCardInfo
{
	int nChairId;
	std::vector<BYTE>    kMasterHandCard;

	void StreamValue(datastream& kData,bool bSend)
	{
		Stream_VALUE(nChairId);
		Stream_VALUE(kMasterHandCard);
	}
};

struct MasterHandCard
{
	std::vector<MasterHandCardInfo>    kMasterHandCardList;
	void StreamValue(datastream& kData,bool bSend)
	{
		StructVecotrMember(MasterHandCardInfo,kMasterHandCardList);
	}
};
struct MaterCheckCard
{
	BYTE							cbCheakCard;						//≤Ÿ◊˜¥˙¬Î
};

struct MaterNiaoCout
{
	BYTE							cbNiaoCout;							//≤Ÿ◊˜¥˙¬Î
};

struct MasterLeftCard
{
	BYTE      kMasterLeftIndex[MAX_INDEX];
	BYTE      kMasterCheakCard;
};

#pragma pack()
//////////////////////////////////////////////////////////////////////////

#endif